package fcycling.crskdev.com.data.connection.interceptors

import fcycling.crskdev.com.data.connection.InternetConnectivityCheck
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import java.util.concurrent.TimeUnit

class OfflineCacheInterceptor(private val internetConnectivityCheck: InternetConnectivityCheck,
                              private val maxOfflineAgeDays: Long = 30 ): Interceptor {

    init {
        assert(maxOfflineAgeDays > 0) {
            "Cache offline lifetime must be positive"
        }
        assert(maxOfflineAgeDays > 1) {
            "Cache offline lifetime must be at least of one day. Current $maxOfflineAgeDays"
        }
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        if (!internetConnectivityCheck.hasInternet()) {
            val cacheControl = CacheControl.Builder()
                    .maxStale(maxOfflineAgeDays.toInt(), TimeUnit.DAYS)
                    .onlyIfCached()
                    .build()
            val cachedRequest = request
                    .newBuilder()
                    .cacheControl(cacheControl)
                    .build()
            return chain.proceed(cachedRequest)
        } else {
            return chain.proceed(request)
        }
    }

}