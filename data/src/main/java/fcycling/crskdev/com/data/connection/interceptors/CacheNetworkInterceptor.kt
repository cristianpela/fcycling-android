package fcycling.crskdev.com.data.connection.interceptors

import fcycling.crskdev.com.data.connection.HEADER_CACHE_CONTROL
import fcycling.crskdev.com.data.connection.HEADER_CUSTOM_IS_CACHEABLE
import fcycling.crskdev.com.data.connection.HEADER_PRAGMA
import okhttp3.Interceptor
import okhttp3.Response
import java.util.concurrent.TimeUnit

/**
 * Created by Cristian Pela on 05.06.2018.
 */
class CacheNetworkInterceptor(private val maxAgeMinutes: Long = 5) : Interceptor {

    init {
        assert(maxAgeMinutes > 0) {
            "Cache lifetime must be positive"
        }
        assert(maxAgeMinutes > 1) {
            "Cache lifetime must be at least of one minute. Current $maxAgeMinutes"
        }
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        if (request.header(HEADER_CUSTOM_IS_CACHEABLE)?.toBoolean() == true) {
            val cacheEntry = HEADER_CACHE_CONTROL to
                    "public, max-age=${TimeUnit.MINUTES.toSeconds(maxAgeMinutes)}"
            val cachedRequest = request
                    .newBuilder()
                    .removeHeader(HEADER_CUSTOM_IS_CACHEABLE)
                    .removeHeader(HEADER_CACHE_CONTROL)
                    .build()
            val response = chain.proceed(cachedRequest)
            return if (response.isSuccessful) {
                response
                        .newBuilder()
                        .removeHeader(HEADER_CACHE_CONTROL)
                        .removeHeader(HEADER_PRAGMA)
                        .addHeader(cacheEntry.first, cacheEntry.second)
                        .build()
            } else
                response
        } else {
            return chain.proceed(request)
        }
    }
}


