package fcycling.crskdev.com.data.connection

import okhttp3.Headers

/**
 * Created by Cristian Pela on 05.06.2018.
 */
const val HEADER_CUSTOM_IS_CACHEABLE = "HEADER_CUSTOM_IS_CACHEABLE"

const val HEADER_CONTENT_ENCODING = "Content-Encoding"

const val HEADER_CONTENT_LENGTH = "Content-Length"

const val HEADER_CONTENT_TYPE = "Content-Type"

const val HEADER_CONTENT_TYPE_ISO_8859_VALUE = "text/html; charset=iso-8859-1"

const val HEADER_AUTH_USERNAME = "navn"

const val HEADER_AUTH_TOKEN = "lp"

const val HEADER_CACHE_CONTROL = "Cache-Control"

const val HEADER_PRAGMA = "Pragma"

const val HEADER_SET_COOKIE = "Set-Cookie"

const val HEADER_AGENT = "User-Agent"

const val HEADER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36"

val BASIC_REQUEST_HEADERS_BUILDER = Headers.Builder()
        .add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
        .add("Accept-Language", "en-US,en;q=0.9")
        .add("Upgrade-Insecure-Requests", "1")
        .add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36")!!