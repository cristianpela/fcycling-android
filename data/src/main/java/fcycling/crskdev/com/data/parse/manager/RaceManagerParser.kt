package fcycling.crskdev.com.data.parse.manager

import fcycling.crskdev.com.data.parse.*
import fcycling.crskdev.com.data.parse.profile.MinimalProfile
import fcycling.crskdev.com.data.parse.race.RaceAbout
import fcycling.crskdev.com.data.parse.race.RaceLink
import fcycling.crskdev.com.data.parse.rider.MinimalInfoRider
import fcycling.crskdev.com.domain.parse.CompositeModel
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.parse.ResponseParser
import fcycling.crskdev.com.domain.parse.toShard
import fcycling.crskdev.com.domain.screen.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

/**
 * Created by Cristian Pela on 08.06.2018.
 */
class RaceManagerParser : ResponseParser<CompositeModel> {

    override fun parse(rawData: String, otherModel: Model?): CompositeModel {

        val document = Jsoup.parse(rawData)

        val shards = shardsWithMenu(document)

        if (isRegistration(document)) {
            val managerRegistration = getManagerRegistration(document)
            shards.add(managerRegistration.toShard(linkRaceManagerScreen(managerRegistration.id)))
        } else {
            val managerResults = getManagerResults(document)
            shards.add(managerResults.toShard(linkRaceManagerScreen(managerResults.id)))
        }

        return CompositeModel(shards)
    }

    private fun isRegistration(document: Document): Boolean =
            document.select("#wrapper > div:nth-child(3) > div > form > button")
                    .firstOrNull()?.let { true } ?: false

    private fun getManagerRegistration(document: Document): ManagerRegistrationModel {
        val managerId = extractRaceManagerId(document, "#wrapper > p > a.valgt")
        val registeredTeams = document.select(
                "#wrapper > div:nth-child(5) > div > div")
                .takeIf { it.isNotEmpty() }
                ?.map {
                    it.select("p").map {
                        val (profileId, teamName) = it.select("span > a")
                                .firstOrNull()
                                ?.anchorHrefAndText()
                                ?.let {
                                    (it.first.convertHyperlinkToRequest().queryParams["ID"]
                                            ?: throw ModelParseException(this, "Team ID not not found in ${it.first}")) to it.second
                                }
                                ?: throw ModelParseException(this, "Profile Id not found")
                        val flagUrl = it.select("span > img").firstOrNull()?.imageExtractUrl()
                                ?: throw ModelParseException(this, "Flag Url not found")
                        MinimalProfile(profileId.toInt(), teamName, teamName, null, flagUrl)
                    }
                }
                ?.flatten()
                ?: emptyList()
        return ManagerRegistrationModel(managerId, registeredTeams)
    }

    private fun getManagerResults(document: Document): ManagerResultsModel {
        val managerId = extractRaceManagerId(document, "#wrapper > p:nth-child(3) > a.valgt")
        //classament
        //it might be a table duel to, so we start to search the 3rd table, to see if it's our table
        val table = document.select("#wrapper > div.cont720 > div > table:nth-child(3)")
                .firstOrNull()
                ?://else we go back to the first one
                document.select("#wrapper > div.cont720 > div > table")
                        .takeIf { it.isNotEmpty() }?.get(0)
                ?: throw ModelParseException(this, "Table not found")
        val managerScoreResults = table
                .child(1)
                .children()
                .fold(mutableListOf<ManagerResult>()) { list, tr ->
                    val hasParticipated = tr.child(0).text().trim() != "-"
                    if (!hasParticipated) {
                        list
                    } else {
                        val position = tr.child(0).text()
                        //profile
                        val profileTd = tr.child(1)
                        val profileNationFlag = profileTd.child(0).imageExtractUrl()
                        val (profileHref, profileManagerName) = profileTd.child(1).anchorHrefAndText()
                        val profileId = profileHref.split("lag=")[1].toInt()
                        val profile = MinimalProfile(profileId, profileManagerName, profileManagerName,
                                null, profileNationFlag)

                        //manager data
                        val managerRiderTeamLink = linkRaceManagerSelectionTeamScreen(profileHref.convertHyperlinkToRequest())
                        val racePoints = tr.child(2).text()
                        val seasonPoints = tr.child(3).text()
                        val fcTeamTd = tr.child(4)
                        val isInFcTeam = fcTeamTd.text().trim() != "-"
                        val fcTeamNationFlag = if (isInFcTeam) fcTeamTd.child(0).imageExtractUrl() else null
                        val fcTeamName = if (isInFcTeam) fcTeamTd.text().trim() else null

                        //captain data
                        val captainTd = tr.child(5)
                        val captainNationFlag = captainTd.child(0).imageExtractUrl()
                        val (captainRequest, captainName) = captainTd.child(1)
                                .anchorHrefAndText().let {
                                    it.first.convertHyperlinkToRequest() to it.second
                                }
                        val captainId = captainRequest.queryParams["r"]?.toInt()
                                ?: throw ModelParseException(this, "Captain id not found")

                        val captain = MinimalInfoRider(captainId, captainName, captainNationFlag, linkRiderScreen(captainRequest))
                        list.apply {
                            add(ManagerResult(position,
                                    profile,
                                    managerRiderTeamLink,
                                    racePoints,
                                    seasonPoints,
                                    fcTeamName,
                                    fcTeamNationFlag,
                                    captain))
                        }
                    }
                }
        return ManagerResultsModel(managerId, RaceInfoExtractor.raceInfo(document), managerScoreResults)
    }

    private fun extractRaceManagerId(document: Document, selector: String): Int {
        return document.select(selector)
                .firstOrNull()
                ?.let {
                    it.anchorHrefAndText().first.convertHyperlinkToRequest().queryParams["konk"]?.toInt()
                            ?: throw ModelParseException(this, "Manager Race ID  not extracted from $it")
                } ?: throw ModelParseException(this, "Manager Race ID  not found")
    }

}

object RaceInfoExtractor {

    fun raceInfo(document: Document): RaceLink {

        val raceLink = document.select("#wrapper > p:nth-child(2) > a.man")
                .attr("href")
                .convertHyperlinkToRequest()
                .let { linkRaceStartListScreen(it) }
        val raceElement = document.select("#wrapper > h1")[0]
                ?: throw ModelParseException(this, "Race Element not found")

        val raceFullTitle = raceElement.text().trim()

        val startIndexRaceName = 8 // MANAGER word len + 2
        val endIndexRaceName = raceFullTitle.length - 5 // subtract year format len dddd(4)   and -1 the space

        val raceName = raceFullTitle.substring(startIndexRaceName, endIndexRaceName)
        val year = raceFullTitle.substring(endIndexRaceName + 1)

        val flagUrl = raceElement.select("img")[0].imageExtractUrl()

        return RaceLink(raceLink, RaceAbout(raceName, year, flagUrl))
    }

}

data class ManagerRegistrationModel(val id: Int, val registeredManagerPlayers: List<MinimalProfile>) : Model

data class ManagerTeamSelectionModel(val id: Int, val selectedRiders: List<MinimalInfoRider>) : Model


data class ManagerResultsModel(val id: Int,
                               val raceLink: RaceLink,
                               val results: List<ManagerResult>) : Model


data class ManagerResult(val position: String,
                         val profile: MinimalProfile,
                         val teamLink: Link,
                         val racePoints: String,
                         val seasonCompetitionPoints: String,
                         val fcTeamName: String?,
                         val fcTeamNationFlag: String?,
                         val captain: MinimalInfoRider)
