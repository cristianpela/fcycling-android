package fcycling.crskdev.com.data.connection

/**
 * Created by Cristian Pela on 07.06.2018.
 */
interface InternetConnectivityCheck {

    fun hasInternet(): Boolean

}