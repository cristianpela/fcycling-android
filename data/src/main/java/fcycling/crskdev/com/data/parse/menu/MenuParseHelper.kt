package fcycling.crskdev.com.data.parse.menu

import fcycling.crskdev.com.data.parse.manager.MinimalManager
import fcycling.crskdev.com.data.parse.profile.getLoggedUserId
import fcycling.crskdev.com.domain.connection.Request
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.parse.ModelShard
import fcycling.crskdev.com.domain.screen.*
import fcycling.crskdev.com.domain.screen.Container.MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID
import fcycling.crskdev.com.domain.screen.Screen.MESSAGES_SCREEN_ID
import fcycling.crskdev.com.domain.screen.Screen.SEASON_STANDINGS_SCREEN_ID
import org.jsoup.nodes.Document
import java.util.*

/**
 * Created by Cristian Pela on 05.05.2018.
 */
class MenuParseHelper(private val currentYear: Int?) {

    @Suppress("unused")
    constructor() : this(Calendar.getInstance().get(Calendar.YEAR))

    internal fun parse(document: Document): MenuModelShard {

        //next Manager
        val nextManagerSelect = document
                .select("#toppmenyInner > div:nth-child(2) > div.small.right > em")

        val nextManager = takeIf { nextManagerSelect.isNotEmpty() }?.let {
            val nextManagerA = nextManagerSelect[0].child(0)
            val nextManagerHyperLinkStr = nextManagerA.attr("href")
            val nextManagerId = nextManagerHyperLinkStr.split("=")[1]
            val nextManagerLink = linkRaceManagerScreen(nextManagerId.toInt())
            val nextManagerFlagUrl = nextManagerA.child(0).attr("src").let {
                "$URL_DOMAIN/$it"
            }
            val nextManagerName = nextManagerA.text()
            val nextManagerDate = nextManagerSelect[0].child(1).text()
            MinimalManager(
                    nextManagerId.toInt(),
                    nextManagerFlagUrl,
                    nextManagerName,
                    nextManagerDate,
                    nextManagerLink)
        }

        //creating the menu model
        val loggedUserId = document.getLoggedUserId()
        val profileLink = profileLink(loggedUserId).skipRequest()

        val managersLink = linkRaceManagersOngoingUpcomingScreen()

        //TODO extract links in screens.kt
        val cupStandingsLink = Link(RouteBehavior(SEASON_STANDINGS_SCREEN_ID,
                MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID,
                showScreenAsChild = true, hasPop = true),
                Request("$URL_DOMAIN/maraton.php", queryParams = mapOf("aar" to currentYear.toString())),
                screenTag = null)

        val messagesEndPoint = "$URL_DOMAIN/melding.php"
        val messagesLink = Link(RouteBehavior(MESSAGES_SCREEN_ID,
                MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID,
                showScreenAsChild = true, hasPop = true),
                Request(messagesEndPoint),
                screenTag = null)

        val menuModel = MenuModel(
                profileLink,
                messagesLink,
                managersLink,
                cupStandingsLink,
                nextManager,
                LOGOUT_LINK_SCREEN)

        return MenuModelShard(MENU_LINK, menuModel)
    }

}

data class MenuModel(
        val profileLink: Link,
        val messagesLink: Link,
        val managersLink: Link,
        val cupStandings: Link,
        val nextManager: MinimalManager?,
        val logout: Link) : Model

class MenuModelShard(override val link: Link?, override val model: MenuModel?) : ModelShard<MenuModel>