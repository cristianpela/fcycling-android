package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.parse.ModelParseException
import fcycling.crskdev.com.data.parse.imageExtractUrl
import fcycling.crskdev.com.data.parse.profile.FullProfileModel.Info
import fcycling.crskdev.com.data.parse.race.MinimalResultRaceInfo
import fcycling.crskdev.com.data.parse.race.RaceLink
import fcycling.crskdev.com.data.parse.rider.MinimalInfoRider
import fcycling.crskdev.com.data.parse.rider.MostUsedCaptain
import fcycling.crskdev.com.data.parse.shardsWithMenu
import fcycling.crskdev.com.domain.parse.CompositeModel
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.parse.ModelShard
import fcycling.crskdev.com.domain.parse.ResponseParser
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.URL_DOMAIN
import fcycling.crskdev.com.domain.screen.profileLink
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

/**
 * Created by Cristian Pela on 01.05.2018.
 */
class FullProfileParser : ResponseParser<CompositeModel> {

    override fun parse(rawData: String, otherModel: Model?): CompositeModel {

        val document = Jsoup.parse(rawData)

        val shards = shardsWithMenu(document).apply {
            MinimalProfileParseHelper.addShardIfProfileIsPrincipal(document, this)
            add(FullProfileParseHelper().parse(document))
        }

        return CompositeModel(shards)
    }

}

class FullProfileParseHelper {
    fun parse(document: Document): FullProfileShard {
        val minimalProfileShard = MinimalProfileParseHelper().parse(document)

        val profileStatsLinks = ProfileStatsLinksParseHelper().parse(document).model.links

        val fullProfileModel = FullProfileModel(
                minimalProfileShard.model!!.minimalProfile,
                getInfoModel(document),
                profileStatsLinks,
                ProfileStatsVictoriesParseHelper().parse(document).model.victories
        )
        return FullProfileShard(profileLink(document.getProfileId()), fullProfileModel)
    }

    private fun getInfoModel(document: Document): FullProfileModel.Info =
    //This points to the INFO section on the profile page
            "#wrapper > div.cont240h"
                    .let { document.select(it) }
                    .takeIf { it.isNotEmpty() }
                    ?.get(0)
                    ?.let {
                        val builder = Info.Builder()
                        //info table
                        it.child(1)
                                .select("tr")
                                .foldIndexed(builder) { i, b, tr ->
                                    val select = tr.select("td")?.takeIf { it.size == 2 }
                                            ?: throw ModelParseException(this, "Profile info table must have to columns")
                                    val (columnName, td) = select[0].text().trim() to select[1]
                                    when (columnName) {
                                        "Location" -> b.apply { location = td.text() }
                                        "Nation" -> {
                                            b.apply {
                                                nation = td.text()
                                                nationFlagUrl = td.child(0).imageExtractUrl()
                                            }
                                        }
                                        "Mem.since" -> b.apply { since = td.text() }
                                        "Badges" -> b.apply {
                                            td.select("img").forEach {
                                                val badgeUrl = "$URL_DOMAIN/" + it.attr("src")
                                                val description = it.attr("title")
                                                b.badgesUrls.add(Info.Badge(badgeUrl, description))
                                            }
                                        }
                                        else -> b
                                    }
                                }
                        builder.create()
                    }
                    ?: throw ModelParseException(this, "Could not parse info model")
}

class FullProfileShard(override val link: Link?, override val model: FullProfileModel) : ModelShard<FullProfileModel>

data class FullProfileModel(
        val minimalProfile: MinimalProfile,
        val info: Info,
        val statsLinks: ProfileStatsLinks?,
        val victories: ProfileStatsVictories?) : Model {
    data class Info(val location: String?,
                    val nation: String?,
                    val nationFlagUrl: String?,
                    val since: String,
                    val badgesUrls: List<Badge>) {
        internal class Builder {
            var location: String? = null
            var nation: String? = null
            var nationFlagUrl: String? = null
            var since: String? = null
            val badgesUrls: MutableList<Badge> = mutableListOf()

            fun create(): Info = Info(location, nation, nationFlagUrl, since!!,
                    badgesUrls)
        }

        data class Badge(val url: String, val description: String)
    }
}


data class ProfileStatsVictories(val raceVictories: List<Victory>,
                                 val stageVictories: List<Victory>) {
    data class Victory(val raceLink: RaceLink, val times: String? = null)
}

data class ProfileStatsLinks(val raceGender: CompetitionGender,
                             val nextGenderLink: Link,
                             val availableYears: List<YearLink>) {
    data class YearLink(val year: Int, val link: Link, val selected: Boolean = false)
}

data class ProfileStatsLinksModel(val id: Int,
                                  val links: ProfileStatsLinks) : Model


data class ProfileStats(val profileSeasonStandings: List<SeasonStanding>,
                        val profileMonthlyStandings: List<MonthlyStanding>,
                        val mostUsedCaptains: List<MostUsedCaptain>,
                        val managerRaces: List<ManagerRace>)

data class ProfileStatsModel(val minimalProfile: MinimalProfile,
                             val year: Int,
                             val gender: CompetitionGender,
                             val stats: ProfileStats?) : Model

enum class CompetitionGender(val symbol: String) {
    MEN(String(Character.toChars(0x2642))),
    WOMEN(String(Character.toChars(0x2640)))
}

data class SeasonStanding(
        val no: Int,
        val type: String)

data class MonthlyStanding(
        val month: String,
        val position: Int,
        val points: Int,
        val entries: String,
        val percent: String)

data class ManagerRace(
        val resultRaceInfo: MinimalResultRaceInfo,
        val position: Int,
        val points: Int,
        val numberOfParticipants: Int,
        val performancePercent: Int,
        val managerPage: Link,
        val teamPage: Link,
        val captain: MinimalInfoRider?,
        val isOngoing: Boolean = false)



