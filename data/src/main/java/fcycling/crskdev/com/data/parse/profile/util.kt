package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.parse.ModelParseException
import fcycling.crskdev.com.data.parse.convertHyperlinkToRequest
import fcycling.crskdev.com.domain.screen.URL_DOMAIN
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

/**
 * Created by Cristian Pela on 11.05.2018.
 */

//#toppmenyInner > div:nth-child(2) > div:nth-child(1) > p > a:nth-child(1) > i
private const val LOGGED_USER_SELECTOR = "#toppmenyInner > div:nth-child(2) > div:nth-child(1) > p > a:nth-child(1) > i"

fun Document.loggedUserProfileHyperLink(): Element = this
        .select(LOGGED_USER_SELECTOR)
        ?.firstOrNull()
        ?.parent()
        ?: throw ModelParseException(this, "Util Parser: Could not find logged user hyperlink. Make sure DOM is not changed")

fun Document.loggedUserProfileHyperLinkStr(): String =
        loggedUserProfileHyperLink()
                .attr("href")
                ?.let { "$URL_DOMAIN/$it" }
                ?: throw ModelParseException(this, "Util Parser: Could not find logged user hyperlink string. Make sure DOM is not changed")


fun Document.isUserLogged() =
        this.select(LOGGED_USER_SELECTOR)
                .isNotEmpty()

fun Document.isCurrentProfileTheLoggedUser() = getLoggedUserId() == getProfileId()

fun Document.hasUserAvatar(): Boolean = getElementById("wrapper")
        .takeIf { it.children().isNotEmpty() && it.child(0).tag().name == "img" }
        ?.let { true } ?: false

fun Document.getLoggedUserId(): Int =
        loggedUserProfileHyperLink()
                .extractIDFromLink()
                ?: throw ModelParseException(this, "Util Parser: Could not find logged user ID. Make sure DOM is not changed or you are logged id")

//using the id from "general" button link to extract the visiting user profile
//#wrapper > p:nth-child(3) > a:nth-child(1) - no avatar
//#wrapper > p:nth-child(4) > a:nth-child(1) - with avatar
//#wrapper > p:nth-child(4) > a:nth-child(1)
fun Document.getProfileId(): Int =
        (if (hasUserAvatar()) 4 else 3).let {
            select("#wrapper > p:nth-child($it) >  a:nth-child(1)")
                    ?.takeIf { it.isNotEmpty() }
                    ?.get(0)
                    ?.extractIDFromLink()
                    ?: throw ModelParseException(this, "Util Parser: Could not find the user ID. Make sure DOM is not changed")

        }

private fun Element?.extractIDFromLink(): Int? = this
        ?.attr("href")
        ?.convertHyperlinkToRequest()
        ?.queryParams?.get("ID")?.toInt()

