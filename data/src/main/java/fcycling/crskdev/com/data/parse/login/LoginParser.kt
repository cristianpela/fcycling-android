package fcycling.crskdev.com.data.parse.login

import fcycling.crskdev.com.data.parse.profile.getLoggedUserId
import fcycling.crskdev.com.data.parse.profile.loggedUserProfileHyperLinkStr
import fcycling.crskdev.com.domain.parse.ErrorModel
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.parse.ResponseParser
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.URL_DOMAIN
import fcycling.crskdev.com.domain.screen.profileLink
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

/**
 * Created by Cristian Pela on 01.05.2018.
 */
class LoginParser : ResponseParser<Model> {

    override fun parse(rawData: String, otherModel: Model?): Model {
        val doc = Jsoup.parse(rawData)
        val errorElement = doc.select("#wrapper > p > b")
        return if (errorElement != null && errorElement.text() == "Errors:") {
            val errorMessageElement = doc.select("#wrapper > p")
            ErrorModel(Error(errorMessageElement.text()))
        } else {
            val loggedUserLink = doc.loggedUserProfileHyperLinkStr()
            val loggedUserId = doc.getLoggedUserId()
            //#wrapper > div.cont320h > div:nth-child(3) > p
            //#wrapper > div.cont320h > div:nth-child(4) > p // when there are duels
            val onlineUsers = (if (isOngoingDuels(doc)) 4 else 3).let { doc.select("#wrapper > div.cont320h > div:nth-child($it) > p") }
                    ?: throw Error("LoginParser: Could not find online users. Make sure DOM is not changed")
            val username = onlineUsers[0]
                    ?.children()
                    ?.firstOrNull {
                        val userLink = "$URL_DOMAIN/" + it.attr("href")
                        userLink == loggedUserLink
                    }?.child(0)?.text()?.trim()
            LoginModel(loggedUserId, username, profileLink(loggedUserId))
        }
    }

    private fun isOngoingDuels(doc: Document): Boolean =
            doc.select("#wrapper > div.cont320h > div:nth-child(1) > h2")?.takeIf {
                it.isNotEmpty() && it[0].text() == "Ongoing duels"
            }?.let { true } ?: false

}

data class LoginModel(val id: Int?, val userName: String? = null, val fullProfileLink: Link? = null) : Model