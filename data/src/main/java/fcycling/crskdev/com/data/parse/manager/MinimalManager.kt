package fcycling.crskdev.com.data.parse.manager

import fcycling.crskdev.com.domain.screen.Link

/**
 * Created by Cristian Pela on 06.05.2018.
 */
data class MinimalManager(val id: Int,
                          val flagUrl: String?,
                          val name: String,
                          val date: String,
                          val link: Link)