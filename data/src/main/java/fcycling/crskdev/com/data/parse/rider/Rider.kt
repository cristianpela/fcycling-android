package fcycling.crskdev.com.data.parse.rider

import fcycling.crskdev.com.data.parse.race.MinimalResultRaceInfo
import fcycling.crskdev.com.domain.screen.Link

/**
 * Created by Cristian Pela on 02.05.2018.
 */

data class MinimalInfoRider(
        val id: Int,
        val name: String,
        val nationFlagUrl: String?,
        val link: Link?,
        val pictureUrl: String? = null)

data class MostUsedCaptain(
        val minimalInfoRider: MinimalInfoRider,
        val times: Int)

data class FullInfoRider(
        val minimalInfoRider: MinimalInfoRider,
        val nationName: String,
        val age: Int,
        val birthDate: String,
        val place: String,
        val height: String,
        val nickName: String?,
        val currentTeam: String,
        val currentYear: Int,
        val currentYearResults: List<RiderRace>)

data class RiderRace(
        val resultRaceInfo: MinimalResultRaceInfo,
        val what: String,
        val position: String,
        val gc: String?,
        val profileImageUrl: String,
        val length: String)