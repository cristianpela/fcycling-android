package fcycling.crskdev.com.data.connection.interceptors

import fcycling.crskdev.com.data.connection.HEADER_CONTENT_ENCODING
import fcycling.crskdev.com.data.connection.HEADER_CONTENT_TYPE_ISO_8859_VALUE
import fcycling.crskdev.com.data.connection.HEADER_SET_COOKIE
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.Response
import okhttp3.ResponseBody

/**
 * Created by Cristian Pela on 10.06.2018.
 */
class LogoutNetworkInterceptor : Interceptor {

    companion object {
        const val LOGGED_OUT_MESSAGE = "You have been logged out!"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        return response.headers(HEADER_SET_COOKIE)
                .takeIf { it.isNotEmpty() && areLogoutHeaders(it)}
                ?.let { buildLogoutResponse(response) }
                ?: response
    }

    private fun areLogoutHeaders(headerValues: List<String>): Boolean {
        return headerValues.any { it.contains("deleted; expires=") }
    }


    private fun buildLogoutResponse(response: Response): Response {
        val mediaType = MediaType.parse(HEADER_CONTENT_TYPE_ISO_8859_VALUE)
        val body = ResponseBody
                .create(mediaType, LOGGED_OUT_MESSAGE)
        return response.newBuilder()
                .removeHeader(HEADER_CONTENT_ENCODING)
                .body(body)
                .code(401).build()
    }

}