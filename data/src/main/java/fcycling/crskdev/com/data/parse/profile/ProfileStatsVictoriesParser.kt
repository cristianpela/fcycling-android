package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.parse.convertHyperlinkToRequest
import fcycling.crskdev.com.data.parse.imageExtractUrl
import fcycling.crskdev.com.data.parse.race.RaceAbout
import fcycling.crskdev.com.data.parse.race.RaceLink
import fcycling.crskdev.com.data.utils.letTryOrNull
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.parse.ModelShard
import fcycling.crskdev.com.domain.parse.ResponseParser
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.URL_DOMAIN
import fcycling.crskdev.com.domain.screen.linkProfileInfoStatsVictories
import fcycling.crskdev.com.domain.screen.linkRaceManagerScreen
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

/**
 * Created by Cristian Pela on 01.06.2018.
 */
class ProfileStatsVictoriesParser : ResponseParser<ProfileStatsVictoriesModel> {
    override fun parse(rawData: String, otherModel: Model?): ProfileStatsVictoriesModel =
            ProfileStatsVictoriesParseHelper().parse(Jsoup.parse(rawData)).model
}

data class ProfileStatsVictoriesModel(val minimalProfile: MinimalProfile, val victories: ProfileStatsVictories) : Model

class ProfileStatsVictoriesShard(override val link: Link?, override val model: ProfileStatsVictoriesModel) : ModelShard<ProfileStatsVictoriesModel>

class ProfileStatsVictoriesParseHelper {
    fun parse(document: Document): ProfileStatsVictoriesShard {

        val minimalProfile = MinimalProfileParseHelper().parse(document).model!!.minimalProfile

        val (raceVictories, stageVictories) = "#wrapper > div.cont240h"
                .let { document.select(it) }
                .takeIf { it.isNotEmpty() }
                ?.get(0)
                ?.let {
                    //race victories
                    val rv = letTryOrNull { it.child(3) }
                            ?.select("tr")
                            ?.fold(mutableListOf()) { list, tr ->
                                val (link, name, date, flagUrl) = tr.select("td")
                                        .drop(1)
                                        .mapIndexed { index, td ->
                                            when (index) {
                                                0 -> {
                                                    val flagUrl =
                                                            "${URL_DOMAIN}/" + td.child(0).attr("src")
                                                    val a = td.child(1)
                                                    val name = a.text()
                                                    val link = linkRaceManagerScreen(a.attr("href")
                                                            .convertHyperlinkToRequest())
                                                    listOf(link, name, flagUrl)
                                                }
                                                1 -> listOf(td.text()) // date
                                                else -> emptyList()
                                            }
                                        }.flatten()

                                val raceLink = RaceLink(link as Link, RaceAbout( name as String,
                                        flagUrl as String, date as String))
                                list.apply { add(ProfileStatsVictories.Victory(raceLink)) }
                            } ?: emptyList<ProfileStatsVictories.Victory>()

                    //stage victories
                    val sv = letTryOrNull { it.child(5) }
                            ?.select("tr")
                            ?.fold(mutableListOf<List<ProfileStatsVictories.Victory>>()) { list, tr ->
                                //tr>td(0)>span
                                val times = tr.child(0).child(0).text()
                                //tr>td(1)>[img, a]*
                                val victories = mutableListOf<ProfileStatsVictories.Victory>()

                                val aHrefs = tr.child(1).select("a")
                                val imgs = tr.child(1).select("img")
                                assert(aHrefs.size == imgs.size)
                                for (i in aHrefs.indices) {
                                    val a = aHrefs[i]
                                    val name = a.text()
                                    val flagUrl = imgs[i].imageExtractUrl()
                                    victories.add(ProfileStatsVictories.Victory(RaceLink(null, RaceAbout(name, null, flagUrl)), times))
                                }
                                list.apply { add(victories) }
                            }?.flatten() ?: emptyList()

                    rv to sv

                } ?: emptyList<ProfileStatsVictories.Victory>() to emptyList()

        return ProfileStatsVictoriesShard(linkProfileInfoStatsVictories(minimalProfile.id),
                ProfileStatsVictoriesModel(minimalProfile, ProfileStatsVictories(raceVictories, stageVictories)))
    }


}