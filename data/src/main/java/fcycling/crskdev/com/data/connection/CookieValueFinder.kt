package fcycling.crskdev.com.data.connection

import java.net.CookieStore
import java.net.URI

/**
 * Created by Cristian Pela on 03.06.2018.
 */
interface CookieValueFinder {

    fun findValue(url: String, name: String): String?

    fun exists(url: String, name: String): Boolean

}

class CookieStoreValueFinder(private val cookieStore: CookieStore) : CookieValueFinder {

    override fun findValue(url: String, name: String): String? =
            cookieStore[URI(url)].firstOrNull { it.name == name }?.value

    override fun exists(url: String, name: String): Boolean =
            cookieStore[URI(url)].firstOrNull { it.name == name } != null

}