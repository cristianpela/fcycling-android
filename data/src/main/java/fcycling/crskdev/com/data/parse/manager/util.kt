package fcycling.crskdev.com.data.parse.manager

import fcycling.crskdev.com.data.parse.ModelParseException
import fcycling.crskdev.com.data.parse.anchorHrefAndText
import fcycling.crskdev.com.data.parse.convertHyperlinkToRequest
import org.jsoup.nodes.Document

/**
 * Created by Cristian Pela on 19.06.2018.
 */
fun isNewLayout(document: Document): Boolean =
        document.select("#wrapper > div.cont240 > div:nth-child(1) > table")
                .isNotEmpty()

fun getManagerId(source: Any, document: Document): Int {
    return (document.select("#wrapper > p > a:nth-child(2)").firstOrNull()
            ?.anchorHrefAndText()
            ?.first
            ?.convertHyperlinkToRequest()
            ?.queryParams
            ?.get("konk")
            ?.toInt()
            ?: throw ModelParseException(source, "Could not extract manager id"))
}