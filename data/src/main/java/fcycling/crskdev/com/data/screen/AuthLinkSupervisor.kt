package fcycling.crskdev.com.data.screen

import fcycling.crskdev.com.data.connection.CookieValueFinder
import fcycling.crskdev.com.data.connection.HEADER_AUTH_TOKEN
import fcycling.crskdev.com.data.connection.HEADER_AUTH_USERNAME
import fcycling.crskdev.com.domain.screen.*
import io.reactivex.ObservableTransformer

/**
 * Created by Cristian Pela on 30.04.2018.
 */
class AuthLinkSupervisor(private val cookieFinder: CookieValueFinder) : LinkSupervisor {

    override fun verify(link: Link): Link {
        val isLoginLink = link != LOGOUT_LINK_SCREEN
                && link.routeBehavior.screen == Screen.LOGIN_SCREEN_ID
        return if (isAuthenticated()) {
            if (isLoginLink)
                LOGGED_IN_LINK_SCREEN_REDIRECT
            else
                link
        } else if (isLoginLink) {
            link
        } else
            LOGIN_LINK_SCREEN_WITH_NO_REQUEST
    }


    private fun isAuthenticated(): Boolean {
        return cookieFinder.exists(URL_DOMAIN, HEADER_AUTH_TOKEN)
                && cookieFinder.exists(URL_DOMAIN, HEADER_AUTH_USERNAME)

    }

    override fun verifyLink(): ObservableTransformer<Link, Link> =
            ObservableTransformer { it.map { verify(it) } }

}