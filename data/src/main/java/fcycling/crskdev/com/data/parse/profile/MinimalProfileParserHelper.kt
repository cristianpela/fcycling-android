package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.parse.imageExtractUrl
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.parse.ModelShard
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.MENU_HEADER_LOGGED_PROFILE_LINK
import fcycling.crskdev.com.domain.screen.profileLink
import org.jsoup.nodes.Document

/**
 * Created by Cristian Pela on 30.04.2018.
 */
data class MinimalProfileModel(
        val minimalProfile: MinimalProfile,
        val seasonPosition: Int,
        val link: Link) : Model

data class MinimalProfile(val id: Int,
                          val userName: String,
                          val managerName: String,
                          val avatarUrl: String?,
                          val flagUrl: String?)

class MinimalProfileParseHelper {

    companion object {
        fun addShardIfProfileIsPrincipal(document: Document, shards: MutableList<ModelShard<*>>) {
            val link = MENU_HEADER_LOGGED_PROFILE_LINK
            if (document.isCurrentProfileTheLoggedUser()) {
                shards.add(MinimalProfileParseHelper().parse(document))
            } else {
                shards.add(MinimalProfileShard(link, null))
            }
        }
    }

    fun parse(document: Document): MinimalProfileShard {
        val userInfoDiv = document
                .getElementById("wrapper")

        val avatarUrl: String? = userInfoDiv.child(0)
                .takeIf { it.tag().name == "img" }?.imageExtractUrl()

        //extract id from top header link profile icon
        //http://firstcycling.com/profil.php?ID=2768 -> 2768
        val userId = document.getProfileId()

        val (flagUrl, userName, managerName) = userInfoDiv.select("h1")[0]
                .let {
                    val flagUrl = it.select("img").firstOrNull()?.imageExtractUrl()

                    val (userName, managerName) = it
                            .text().let {
                                val bracketL = it.indexOf("[")
                                val bracketR = it.indexOf("]")
                                it.substring(0, bracketL).trim() to
                                        it.substring(bracketL + 1, bracketR)
                            }
                    Triple(flagUrl, userName, managerName)
                }


        val seasonPosition = document
                .select("#wrapper > div.cont720 > div:nth-child(2) > table > tbody >" +
                        " tr:nth-child(1) > td:nth-child(1) > span")
                .firstOrNull()
                ?.text()
                ?.split("No.")?.get(1)
                ?.toInt()
                ?:-1

        val model = MinimalProfileModel(
                MinimalProfile(
                        userId,
                        userName,
                        managerName,
                        avatarUrl,
                        flagUrl),
                seasonPosition,
                profileLink(userId))

        return MinimalProfileShard(MENU_HEADER_LOGGED_PROFILE_LINK, model)
    }
}


class MinimalProfileShard(override val link: Link?, override val model: MinimalProfileModel?) : ModelShard<MinimalProfileModel>
