package fcycling.crskdev.com.data.connection

import fcycling.crskdev.com.domain.connection.*
import okhttp3.FormBody
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import java.io.IOException

/**
 * Created by Cristian Pela on 30.04.2018.
 */
class OkHttpConnection(private val client: OkHttpClient) : Connection {

    @Throws(IOException::class)
    override fun request(request: Request, tag: String?): Response {

        val url = HttpUrl.parse(request.endPoint)
                ?.newBuilder()
                ?.let { builder ->
                    request.queryParams.entries.fold(builder) { acc, entry ->
                        acc.addQueryParameter(entry.key, entry.value)
                    }
                }
                ?.build()
                ?.url()
                ?: return ErrorResponse(Error("Invalid URL ${request.endPoint}"))

        val httpRequest = RequestBuilder()
                .headers(BASIC_REQUEST_HEADERS_BUILDER.build())
                .let { if (request.cacheable) it.addHeader(HEADER_CUSTOM_IS_CACHEABLE, true.toString()) else it }
                .url(url)
                .tryPost(request.formParams)
                .let { if (tag == null) it else it.tag(tag) }
                .build()


        val (httpResponse, error) = try {
            client.newCall(httpRequest).execute() to Error()
        } catch (ex: IOException) {
            ex.printStackTrace()
            null to Error(ex)
        }

        return httpResponse?.let {
            if (it.cacheResponse() != null) {
                println("Response loaded from cache $it")
            }
            if (!it.isSuccessful) {
                val code = it.code()
                val msg = it.body()?.string()
                when (code) {
                    401 -> LoggedOutErrorResponse(Error(msg))
                    else -> ErrorResponse(Error("Error Code[$code]: $msg"))
                }
            } else {
                bodyToString(it)?.let {
                    RawResponse(emptyMap(), it)
                } ?: ErrorResponse(Error("Uh Oh! No response body has returned"))
            }
        } ?: ErrorResponse(error)
    }

    override fun cancel(tag: String?) {
        val dispatcher = client.dispatcher()
        if (tag == null) {
            dispatcher.cancelAll()
        } else {
            dispatcher.queuedCalls().forEach {
                if (it.request().tag() == tag)
                    it.cancel()
            }
        }
    }

    private fun bodyToString(httpResponse: HttpResponse): String? =
    //this is not needed anymore the gzip decoding is done by the BridgeInterceptor
//            httpResponse.header("Content-Encoding")
//                    ?.equals("gzip", true)
//                    ?.takeIf { true }
//                    ?.let {
//                        httpResponse.body()?.source()?.let {
//                            GzipSource(it).let { Okio.buffer(it).readUtf8() }
//                        }
//                    }
//                    ?:
            httpResponse.body()?.string()


    private fun RequestBuilder.tryPost(form: Map<String, String>): RequestBuilder =
            if (form.isEmpty()) {
                this
            } else {
                val formBody = form.entries
                        .fold(FormBody.Builder()) { acc, entry ->
                            acc.add(entry.key, entry.value)
                        }
                        .build()
                this.post(formBody)
            }
}

typealias RequestBuilder = okhttp3.Request.Builder
typealias HttpResponse = okhttp3.Response