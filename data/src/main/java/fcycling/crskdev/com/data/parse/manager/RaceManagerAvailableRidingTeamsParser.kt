package fcycling.crskdev.com.data.parse.manager

import fcycling.crskdev.com.data.parse.*
import fcycling.crskdev.com.data.parse.rider.MinimalInfoRider
import fcycling.crskdev.com.domain.parse.*
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.linkRaceManagersAvailableRidingTeamsScreen
import fcycling.crskdev.com.domain.screen.linkRiderScreen
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

/**
 * Created by Cristian Pela on 19.06.2018.
 */
class RaceManagerAvailableRidingTeamsParser : ResponseParser<CompositeModel> {

    override fun parse(rawData: String, otherModel: Model?): CompositeModel {
        val document = Jsoup.parse(rawData)
        return CompositeModel(shardsWithMenu(document).apply {
            add(RaceManagerAvailableRidingTeamsParseHelper().parse(document))
        })
    }

}

class RaceManagerAvailableRidingTeamsParseHelper {

    fun parse(document: Document): ModelShard<RaceManagerAvailableRidingTeamsModel> {
        val managerId = getManagerId(this, document)
        return getAvailableRidingTeams(managerId, document)
                .toShard(linkRaceManagersAvailableRidingTeamsScreen(managerId))
    }


    private fun getAvailableRidingTeams(managerId: Int, document: Document): RaceManagerAvailableRidingTeamsModel {
        val table = document.select("#wrapper > div.cont720").firstOrNull()
                ?: throw ModelParseException(this, "Table Riders: available team tables not found")

        val ridingTeams = table.select("div.back").map {
            val (teamId, teamName) = it.select("h2 > a:nth-child(1)").firstOrNull()
                    ?.anchorHrefAndText()
                    ?.let {
                        (it.first.convertHyperlinkToRequest().queryParams["l"]?.toInt()
                                ?: -1) to it.second
                    }
            //might be only title with no link - this happens if there are country-teams and not regular teams
                    ?: -1 to (it.select("h2").firstOrNull()?.text()
                            ?: throw ModelParseException(this, "Table Riders: team name and id not found!"))

            val riders = it.select("table > tbody > tr")
                    .takeIf { it.isNotEmpty() }
                    ?.map {
                        val (riderName, riderLink) = it.select("td:nth-child(2) > a").firstOrNull()
                                ?.anchorHrefAndText()
                                ?.let {
                                    it.second to linkRiderScreen(it.first.convertHyperlinkToRequest())
                                }
                                ?: throw ModelParseException(this, "Table Riders: rider name not found")
                        val flagUrl = it.select("td:nth-child(2) > img").firstOrNull()
                                ?.imageExtractUrl()
                                ?: throw ModelParseException(this, "Table Riders: flagUrl for $riderName not found")
                        val riderId = riderLink.request?.queryParams?.get("r")?.toInt() ?: -1
                        MinimalInfoRider(riderId, riderName, flagUrl, riderLink)
                    }
                    ?: throw ModelParseException(this, "Table Riders: riders for team $teamName not found")
            RidingTeam(teamId, teamName, null, riders)
        }

        return RaceManagerAvailableRidingTeamsModel(managerId, ridingTeams)
    }

}


data class RaceManagerAvailableRidingTeamsModel(val managerId: Int, val ridingTeams: List<RidingTeam>) : Model

data class RidingTeam(val teamId: Int, val name: String, val link: Link?, val riders: List<MinimalInfoRider>)