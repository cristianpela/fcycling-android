package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.parse.ModelParseException
import fcycling.crskdev.com.data.parse.imageExtractUrl
import fcycling.crskdev.com.data.parse.rider.MinimalInfoRider
import fcycling.crskdev.com.data.parse.rider.MostUsedCaptain
import fcycling.crskdev.com.data.parse.shardsWithMenu
import fcycling.crskdev.com.domain.parse.CompositeModel
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.parse.ModelShard
import fcycling.crskdev.com.domain.parse.ResponseParser
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.linkProfileInfoStatsScreen
import fcycling.crskdev.com.domain.screen.linkRiderScreen
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

/**
 * Created by Cristian Pela on 15.05.2018.
 */
class ProfileStatsParser : ResponseParser<CompositeModel> {

    override fun parse(rawData: String, otherModel: Model?): CompositeModel {
        val document = Jsoup.parse(rawData)
        val shards = shardsWithMenu(document).apply {
            add(FullProfileParseHelper().parse(document))
            add(ProfileStatsParseHelper().parse(document))
        }
        return CompositeModel(shards)
    }

}

class ProfileStatsShard(override val link: Link?, override val model: ProfileStatsModel)
    : ModelShard<ProfileStatsModel>

class ProfileStatsParseHelper {

    private fun hasFollowingRiders(document: Document) =
            document.select("#wrapper > div.cont720 > div.lesmer2.back")
                    .firstOrNull() != null

    fun parse(document: Document): ProfileStatsShard {

        //shift back div positions when no "following div" exists
        val shiftPosition = if (hasFollowingRiders(document)) 0 else 1

        val seasonStandings = document
                .select("#wrapper > div.cont720 > div:nth-child(${2-shiftPosition}) > table.tableSorter > tbody > tr")
                .takeIf { it.isNotEmpty() }
                ?.fold(mutableListOf<SeasonStanding>()) { list, tr ->
                    val no = tr.child(0).text().split(".")[1].toInt()
                    val type = tr.child(1).text()
                    list.apply { add(SeasonStanding(no, type)) }
                } ?: throw ModelParseException(this, "Season standings not found!")

        val monthlyStandings = document
                .select("#wrapper > div.cont720 > div:nth-child(${3-shiftPosition}) > table.tableSorter > tbody > tr")
                .takeIf { it.isNotEmpty() }
                ?.fold(mutableListOf<MonthlyStanding>()) { list, tr ->
                    val month = tr.child(0).child(0).text()
                    val position = tr.child(1).text().toInt()
                    val points = tr.child(2).text().toInt()
                    val entries = tr.child(3).text()
                    val percent = tr.child(4).text()
                    list.apply {
                        add(MonthlyStanding(
                                month,
                                position,
                                points,
                                entries,
                                percent
                        ))
                    }

                } ?: throw ModelParseException(this, "Monthly standings not found!")


        val mostUseCaptains =
                document.select("#wrapper > div.cont720 > div:nth-child(${2-shiftPosition}) > div > p > span")
                        .takeIf { it.isNotEmpty() }
                        ?.filter { it.attr("style") == "white-space:nowrap;" }
                        ?.fold(mutableListOf<MostUsedCaptain>()) { list, span ->
                            list.apply {
                                val a = span.select("a")
                                val href = a.attr("href")

                                val name = a.text()
                                val riderId = href.split("=")[1].toInt()
                                val flagUrl = span.select("img")[0].imageExtractUrl()

                                val rider = MinimalInfoRider(
                                        riderId,
                                        name,
                                        flagUrl,
                                        linkRiderScreen(riderId))

                                val times = span.select("span")[1].text().let {
                                    //#wrapper > div.cont720 > div:nth-child(2) > div > p > span:nth-child(3) > span
                                    val size = it.length
                                    assert(size >= 3)
                                    //(5) -> strip the brackets
                                    it.substring(1, size - 1)
                                }.toInt()

                                add(MostUsedCaptain(rider, times))
                            }
                        }
                        ?: throw ModelParseException(this, "Most used captains not found!")

        val managerRaces = ManagerRacesExtractor.managerRaces(document, ManagerRacesExtractor.Type.ONGOING)
        val linksModel = ProfileStatsLinksParseHelper().parse(document).model
        val (year, gender) = linksModel.links.let {
            it.availableYears.first { it.selected }.year to linksModel.links.raceGender
        }
        val minimalProfile = MinimalProfileParseHelper().parse(document).model!!.minimalProfile
        val id = minimalProfile.id
        val model = ProfileStatsModel(minimalProfile,
                year, gender,
                ProfileStats(seasonStandings, monthlyStandings, mostUseCaptains, managerRaces))
        return ProfileStatsShard(linkProfileInfoStatsScreen(id, year, gender == CompetitionGender.WOMEN),
                model)
    }
}


