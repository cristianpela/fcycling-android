package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.parse.convertHyperlinkToRequest
import fcycling.crskdev.com.data.parse.imageExtractUrl
import fcycling.crskdev.com.data.parse.menu.MenuParseHelper
import fcycling.crskdev.com.data.parse.race.MinimalResultRaceInfo
import fcycling.crskdev.com.data.parse.race.RaceAbout
import fcycling.crskdev.com.data.parse.race.RaceLink
import fcycling.crskdev.com.data.parse.rider.MinimalInfoRider
import fcycling.crskdev.com.data.utils.tryOrNull
import fcycling.crskdev.com.domain.parse.CompositeModel
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.parse.ModelShard
import fcycling.crskdev.com.domain.parse.ResponseParser
import fcycling.crskdev.com.domain.screen.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

/**
 * Created by Cristian Pela on 24.05.2018.
 */
class ProfileStatsManagerRacesParser : ResponseParser<CompositeModel> {
    override fun parse(rawData: String, otherModel: Model?): CompositeModel {
        val document = Jsoup.parse(rawData)
        val shards = mutableListOf<ModelShard<*>>()
        MenuParseHelper().parse(document)
        shards.add(object : ModelShard<ManagerRacesModel> {
            override val link: Link?
                get() = null
            override val model: ManagerRacesModel
                get() {
                    val linksModel = ProfileStatsLinksParseHelper().parse(document).model
                    val (year, gender) = linksModel.links.let {
                        it.availableYears.first { it.selected }.year to linksModel.links.raceGender
                    }
                    val minimalProfile = MinimalProfileParseHelper().parse(document).model!!.minimalProfile
                    return ManagerRacesModel(minimalProfile, year, gender,
                            ManagerRacesExtractor.managerRaces(document, ManagerRacesExtractor.Type.FINISHED))
                }
        })
        return CompositeModel(shards)
    }


}

data class ManagerRacesModel(val minimalProfile: MinimalProfile,
                             val year: Int,
                             val gender: CompetitionGender,
                             val races: List<ManagerRace>) : Model

object ManagerRacesExtractor {

    enum class Type {
        ONGOING, FINISHED
    }

    fun managerRaces(document: Document, type: Type): List<ManagerRace> = tryOrNull {
        document
                .select("#wrapper > div.cont720 > table")[0]
                .child(1)
                .children()
                .fold(mutableListOf<ManagerRace>()) { list, tr ->
                    val position = try {
                        tr.child(2).text().toInt()
                    } catch (e: Exception) {
                        -1
                    }
                    val participants = tr.child(3).text().toInt()
                    val points = try {
                        tr.child(7).text().toInt()
                    } catch (e: Exception) {
                        -1
                    }
                    val raceClassification = tr.child(0).text()

                    val captainTd = tr.child(6)
                    val captain = if (captainTd.text() == "-")
                        null
                    else {
                        val riderFlagUrl = captainTd.child(0).imageExtractUrl()
                        val riderHyperLink = captainTd.child(1)
                        val riderName = riderHyperLink.text()
                        val riderLink = linkRiderScreen(riderHyperLink.attr("href").convertHyperlinkToRequest())
                        val riderId = riderLink.request?.queryParams?.get("r")?.toInt()!!

                        MinimalInfoRider(riderId, riderName, riderFlagUrl, riderLink)
                    }

                    val raceHyperLink = tr.child(4).child(1)
                    val raceInsideLink = linkRaceResultsScreenLink(raceHyperLink
                            .attr("href")
                            .convertHyperlinkToRequest())
                    val raceNationFlagUrl = tr.child(4).child(0).imageExtractUrl()


                    val raceLink = RaceLink(raceInsideLink, RaceAbout(raceHyperLink.text(),
                            tr.child(1).text(), raceNationFlagUrl))

                    val raceInfo = MinimalResultRaceInfo(
                            //race link
                            raceLink,
                            raceClassification,
                            captain)

                    val managerHyperLink = tr.child(5).child(0)
                    val managerLink = linkRaceManagerScreen(managerHyperLink
                            .attr("href")
                            .convertHyperlinkToRequest())
                    val isOngoing = managerHyperLink.attr("class") == "ablue"

                    val teamLink = linkRaceManagerSelectionTeamScreen(tr.child(5)
                            .child(1)
                            .attr("href")
                            .convertHyperlinkToRequest())

                    val performancePercent =
                            tr.child(8).text().let {
                                val percentIndex = it.indexOf("%")
                                if (percentIndex != -1)
                                    it.substring(0, percentIndex)
                                else "-1"
                            }.toInt()

                    val managerRace = ManagerRace(
                            raceInfo,
                            position,
                            points,
                            participants,
                            performancePercent,
                            managerLink,
                            teamLink,
                            captain,
                            isOngoing)

                    list.apply {
                        if (type == Type.ONGOING) {
                            if (isOngoing) {
                                add(managerRace)
                            }
                        } else {
                            if (!isOngoing)
                                add(managerRace)
                        }

                    }

                }
    } ?: emptyList()
}