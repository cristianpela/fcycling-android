package fcycling.crskdev.com.data.parse

import fcycling.crskdev.com.data.parse.menu.MenuParseHelper
import fcycling.crskdev.com.domain.connection.Request
import fcycling.crskdev.com.domain.parse.ModelShard
import fcycling.crskdev.com.domain.screen.URL_DOMAIN
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.lang.RuntimeException
import java.net.URI

/**
 * Created by Cristian Pela on 14.05.2018.
 */
class ModelParseException(source: Any, message: String) :
        RuntimeException(source.javaClass.canonicalName + ":" + message)

/**
 * Decode a string url into a pair consisting of its path and a map of its query parameters
 */
fun String.convertHyperlinkToRequest(domain: String = URL_DOMAIN): Request =
        URI(this)
                .let {
                    Request("${domain.tryAppendPathDelim()}${it.path.let {
                        //strip the first "/" from path
                        if (it.indexOf("/") == 0 && it.length > 1) it.substring(1) else it
                    }}", it.query
                            ?.split("&")
                            ?.fold(mutableMapOf()) { acc, query ->
                                acc.apply {
                                    val keyVal = query.split("=")
                                    acc[keyVal[0]] = if (keyVal.size == 2) keyVal[1] else ""
                                }
                            } ?: emptyMap())
                }


fun Element.imageExtractUrl(domain: String = URL_DOMAIN): String {
    assertTag("img")
    return domain.tryAppendPathDelim() + this.attr("src")
}

/**
 * Extracts anchor's href and text
 *
 * return pair of href(first) and text(second)
 */
fun Element.anchorHrefAndText(domain: String = URL_DOMAIN): Pair<String, String> {
    assertTag("a")
    return domain.tryAppendPathDelim() + this.attr("href") to this.text()
}

/**
 * Extracts anchor's href and its child
 *
 * return pair of href(first) and child element(second)
 */
fun Element.anchorHrefAndChild(domain: String = URL_DOMAIN): Pair<String, Element> {
    assertTag("a")
    return domain.tryAppendPathDelim() + this.attr("href") to this
}

private fun Element.assertTag(tagName: String) {
    if (this.tag().name.toLowerCase() != tagName) {
        throw ModelParseException(this, "Element must be a/an $tagName tag. Provided: ${this.tag().name}")
    }
}

private fun String.tryAppendPathDelim(): String {
    val endsWithPath = indexOfLast { it == '/' } == length - 1
    return (if (endsWithPath) this else "$this/")
}

fun shardsWithMenu(document: Document): MutableList<ModelShard<*>> =
        mutableListOf(MenuParseHelper().parse(document))

fun <T> T.print() = apply { println(this) }

