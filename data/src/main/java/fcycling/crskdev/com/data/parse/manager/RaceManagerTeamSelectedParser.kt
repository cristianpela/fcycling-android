package fcycling.crskdev.com.data.parse.manager

import fcycling.crskdev.com.data.parse.*
import fcycling.crskdev.com.data.parse.rider.MinimalInfoRider
import fcycling.crskdev.com.domain.parse.CompositeModel
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.parse.ResponseParser
import fcycling.crskdev.com.domain.parse.toShard
import fcycling.crskdev.com.domain.screen.linkRaceManagersTeamSelectionScreen
import fcycling.crskdev.com.domain.screen.linkRiderScreen
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

/**
 * Created by Cristian Pela on 18.06.2018.
 */
class RaceManagerTeamSelectedParser : ResponseParser<CompositeModel> {

    override fun parse(rawData: String, otherModel: Model?): CompositeModel {
        val document = Jsoup.parse(rawData)
        val teamSelectionModel = if (isNewLayout(document))
            getRaceManagerTeamSelectionModelNew(document)
        else
            getRaceManagerTeamSelectionModelOld(document)
        return CompositeModel(
                shardsWithMenu(document).apply {
                    add(teamSelectionModel.toShard(linkRaceManagersTeamSelectionScreen(teamSelectionModel.managerId)))
                }
        )
    }

    private fun getRaceManagerTeamSelectionModelNew(document: Document): RaceManagerTeamSelectionModel {
        val managerId = getManagerId(this, document)
        return document.select("#wrapper > div.cont240 > div:nth-child(1) > table")
                .firstOrNull()
                ?.select("tbody > tr")
                ?.map {
                    val tds = it.select("td").takeIf { it.size == 4 }
                            ?: throw ModelParseException(this, "Selected Rider Row Must Have 4 Columns")
                    val coefficient = tds[0].select("span > span.n").firstOrNull()
                            ?.text()
                            //might be reserve, then don't extract the percent
                            ?.let { if (it == "Res.") "0" else it.substring(0, it.indexOf("%")) }
                            ?.toFloat()?.div(100)
                            ?: throw  ModelParseException(this, "Could not extract rider's position coefficient")
                    val riderPictureUrl = tds[1].select("img").firstOrNull()
                            ?.imageExtractUrl()
                            //don't need the placeholder 1.jpg
                            ?.let { if (it.endsWith("1.jpg")) null else it }
                    val riderFlagUrl = tds[2].select("img").firstOrNull()
                            ?.imageExtractUrl()
                    val isRemovedFromRoaster = tds[2].attr("style") == "background:orange;"
                    val (riderLink, riderName) = tds[2].select("a")
                            ?.firstOrNull()
                            ?.anchorHrefAndText()
                            ?.let { linkRiderScreen(it.first.convertHyperlinkToRequest()) to it.second }
                            ?: null to ""
                    val riderId = riderLink?.request?.queryParams?.get("r")?.toInt()
                            ?: -1
                    SelectedTeamRider(coefficient, MinimalInfoRider(riderId, riderName, riderFlagUrl, riderLink, riderPictureUrl), isRemovedFromRoaster)
                }
                ?.let { RaceManagerTeamSelectionModel(managerId, it) }
                ?: throw ModelParseException(this, "New Selected Riders Table Was Not Found")
    }


    private fun getRaceManagerTeamSelectionModelOld(document: Document): RaceManagerTeamSelectionModel {
        val managerId = getManagerId(this, document)
        val hasStagesShown = document.select("#wrapper > div.cont240h > div:nth-child(1) > h2")
                .takeIf { it.isNotEmpty() && it.first().text() == "Stages" }
                ?.let { true } ?: false
        return (if (hasStagesShown) 2 else 1).let {
            document.select("#wrapper > div.cont240h > div:nth-child($it) > div")
                    ?.takeIf { it.isNotEmpty() }
                    ?.map {
                        val coefficient = it.select("p > em")
                                ?.firstOrNull()
                                ?.text()
                                ?.let { if (it == "No rider selected") "0" else it.substring(0, it.indexOf("%")).trim() }
                                ?.toFloat()?.div(100)
                                ?: throw  ModelParseException(this, "Could not extract rider's position coefficient")
                        val (riderPictureUrl, riderFlagUrl) = it.select("p > img")
                                ?.takeIf { it.size == 2 }
                                ?.let {
                                    it.first().imageExtractUrl() to it.last().imageExtractUrl()
                                } ?: null to null
                        val isRemovedFromRoaster = it.attr("style").contains("background:orange")
                        val (riderLink, riderName) = it.select("a")
                                ?.firstOrNull()
                                ?.anchorHrefAndText()
                                ?.let { linkRiderScreen(it.first.convertHyperlinkToRequest()) to it.second }
                                ?: null to ""
                        val riderId = riderLink?.request?.queryParams?.get("r")?.toInt()
                                ?: -1
                        SelectedTeamRider(coefficient, MinimalInfoRider(riderId, riderName, riderFlagUrl, riderLink, riderPictureUrl), isRemovedFromRoaster)
                    }
                    ?.let { RaceManagerTeamSelectionModel(managerId, it) }
                    ?: throw ModelParseException(this, "Old Selected Riders Table Was Not Found")
        }
    }

}


data class SelectedTeamRider(val positionScoreCoefficient: Float, val info: MinimalInfoRider, val isRemovedFromRoaster: Boolean = false)

data class RaceManagerTeamSelectionModel(val managerId: Int, val selectedTeamRiders: List<SelectedTeamRider>) : Model