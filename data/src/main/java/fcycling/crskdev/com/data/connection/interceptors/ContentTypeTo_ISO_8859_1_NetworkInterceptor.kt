package fcycling.crskdev.com.data.connection.interceptors

import fcycling.crskdev.com.data.connection.HEADER_CONTENT_TYPE
import fcycling.crskdev.com.data.connection.HEADER_CONTENT_TYPE_ISO_8859_VALUE
import okhttp3.Interceptor
import okhttp3.Response

@Suppress("ClassName")
/**
 * Created by Cristian Pela on 10.06.2018.
 */
class ContentTypeTo_ISO_8859_1_NetworkInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        /**
         * Top of the chain interceptor that forces the "Content-Type" header to use
         * the iso-8859-1 charset, this way assuring that accents are properly decoded by
         * BrideInterceptor's gzip decoder and not replaced with question marks
         */
        return chain.proceed(chain.request()).newBuilder()
                .removeHeader(HEADER_CONTENT_TYPE)
                .header(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_ISO_8859_VALUE)
                .build()
    }
}