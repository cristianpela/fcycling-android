package fcycling.crskdev.com.data.parse.manager

import fcycling.crskdev.com.data.parse.anchorHrefAndText
import fcycling.crskdev.com.data.parse.convertHyperlinkToRequest
import fcycling.crskdev.com.data.parse.imageExtractUrl
import fcycling.crskdev.com.data.parse.shardsWithMenu
import fcycling.crskdev.com.data.utils.multiZipped
import fcycling.crskdev.com.domain.parse.CompositeModel
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.parse.ResponseParser
import fcycling.crskdev.com.domain.parse.toShard
import fcycling.crskdev.com.domain.screen.linkRaceManagerScreen
import fcycling.crskdev.com.domain.screen.linkRaceManagersOngoingUpcomingScreen
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

/**
 * Created by Cristian Pela on 17.06.2018.
 */
class RaceManagersOngoingUpcomingParser : ResponseParser<CompositeModel> {

    companion object {
        private const val UPCOMING_STYLE = "background:lightblue;"
        private const val ONGOING_STYLE = "background:lightgreen;"
    }

    override fun parse(rawData: String, otherModel: Model?): CompositeModel {
        val document = Jsoup.parse(rawData)
        val shards = shardsWithMenu(document).apply {
            add(RaceManagersOngoingUpcomingModel(
                    getManagerRaces(document, ONGOING_STYLE),
                    getManagerRaces(document, UPCOMING_STYLE)
            ).toShard(linkRaceManagersOngoingUpcomingScreen()))
        }
        return CompositeModel(shards)
    }

    private fun getManagerRaces(document: Document, typeStyle: String): List<MinimalManager> =
            document.select("#wrapper > div.cont240h > div.back").filter {
                it.attr("style") == typeStyle
            }.takeIf { it.isNotEmpty() }?.first()?.let {

                val imagesFlag = it.select(" p > img")
                val anchorsNames = it.select("p > a")
                val spansDates = it.select("p > span.dato")

                multiZipped<Element, MinimalManager>(imagesFlag, anchorsNames, spansDates) {
                    val flagUrl = it[0].imageExtractUrl()
                    val (request, name) = it[1].anchorHrefAndText().let {
                        it.first.convertHyperlinkToRequest() to it.second
                    }
                    val date = it[2].text()
                    val id = request.queryParams.get("konk")?.toInt() ?: -1
                    MinimalManager(id, flagUrl, name, date, linkRaceManagerScreen(request))

                }
            } ?: emptyList()
}


data class RaceManagersOngoingUpcomingModel(val ongoing: List<MinimalManager>, val upcoming: List<MinimalManager>) : Model