package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.parse.convertHyperlinkToRequest
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.parse.ModelShard
import fcycling.crskdev.com.domain.parse.ResponseParser
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.linkProfileInfoStatsScreen
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

/**
 * Created by Cristian Pela on 15.05.2018.
 */
class ProfileStatsLinksParser : ResponseParser<ProfileStatsLinksModel> {
    override fun parse(rawData: String, otherModel: Model?): ProfileStatsLinksModel {
        val document = Jsoup.parse(rawData)
        return ProfileStatsLinksParseHelper().parse(document).model
    }
}

class ProfileStatsLinksParseHelper {

    fun parse(document: Document): ProfileStatsLinksShard {
        //This points to the first year for men link
        val userId = document.getProfileId()

        val availableYears = (if (document.hasUserAvatar()) 3 else 2)
                .let {
                    document.select("#wrapper > p:nth-child($it) > a")
                            .filter { it.attr("href").startsWith("profil.php") }
                            .fold(mutableListOf<ProfileStatsLinks.YearLink>()) { list, a ->
                                list.apply {
                                    val req = a.attr("href").convertHyperlinkToRequest()
                                    val year = req.queryParams["aar"]!!.toInt()
                                    val isSelectedYear = a.attr("class").isNotEmpty()
                                    add(ProfileStatsLinks.YearLink(year, linkProfileInfoStatsScreen(req), isSelectedYear))
                                }
                            }
                }

        val genderSelector = if (document.hasUserAvatar()) "#wrapper > p:nth-child(4)" else "#wrapper > p:nth-child(3)"
        val (currentGenderLink, nextGenderLink, currentGender) = document.select(genderSelector)[0].let {
            val menHref = it.child(0)
            val womenHref = it.child(1)

            val menReq = menHref.attr("href").convertHyperlinkToRequest()
            val womenReq = womenHref.attr("href").convertHyperlinkToRequest()

            val isMenCurrent = menHref.attr("class") == "valgt"

            val nextReq = if (isMenCurrent) womenReq else menReq
            val currReq = if (isMenCurrent) menReq else womenReq
            val currGender = if (isMenCurrent) CompetitionGender.MEN else CompetitionGender.WOMEN
            Triple(linkProfileInfoStatsScreen(currReq), linkProfileInfoStatsScreen(nextReq), currGender)
        }

        val model = ProfileStatsLinksModel(userId, ProfileStatsLinks(currentGender, nextGenderLink, availableYears))

        return ProfileStatsLinksShard(currentGenderLink, model)
    }
}

class ProfileStatsLinksShard(override val link: Link?, override val model: ProfileStatsLinksModel) : ModelShard<ProfileStatsLinksModel>