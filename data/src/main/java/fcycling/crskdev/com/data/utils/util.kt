package fcycling.crskdev.com.data.utils

import timber.log.Timber

/**
 * Created by Cristian Pela on 14.05.2018.
 */
/**
 * Exception swallower. Use it with care!!!
 */
inline fun <T> tryOrNull(block: () -> T): T? =
        try {
            block()
        } catch (e: Exception) {
            Timber.e(e)
            null
        }

/**
 * Exception swallower. Use it with care!!!
 */
inline fun <T, R> T.letTryOrNull(block: T.() -> R): R? =
        try {
            block()
        } catch (e: Exception) {
            Timber.e(e)
            null
        }


fun <T, R> multiZipped(vararg lists: List<T>, combiner: (List<T>) -> R): List<R> {
    val size = lists.map { it.size }.max() ?: 0
    lists.forEach {
        assert(it.size == size) {
            "In order to zip, lists must be the same size"
        }
    }
    val output = mutableListOf<R>()
    (0 until size).forEach {
        //add each element at position $it to the combiner
        val column = mutableListOf<T>()
        lists.forEach { l ->
            column.add(l[it])
        }
        output.add(combiner(column))
    }

    return output
}

