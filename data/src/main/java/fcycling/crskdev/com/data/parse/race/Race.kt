package fcycling.crskdev.com.data.parse.race

import fcycling.crskdev.com.data.parse.rider.MinimalInfoRider
import fcycling.crskdev.com.domain.screen.Link

/**
 * Created by Cristian Pela on 02.05.2018.
 */
data class MinimalResultRaceInfo(
        val link: RaceLink,
        val classification: String,
        val contender: MinimalInfoRider?)

data class RaceAbout(val name: String, val date: String?, val flagUrl: String)

data class RaceLink(val link: Link?, val about: RaceAbout)

data class RaceInfo(
        private val about: RaceAbout,
        private val startListLink: Link,
        private val stagesLink: Link,
        private val stageProfilesLink: Link,
        private val results: Link,
        private val managerLink: Link)