package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.loadResource
import fcycling.crskdev.com.domain.screen.Container
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.RouteBehavior
import fcycling.crskdev.com.domain.screen.Screen.MENU_LOGGED_PROFILE_SCREEN_ID
import fcycling.crskdev.com.domain.screen.profileLink
import org.jsoup.Jsoup
import org.junit.Test

/**
 * Created by Cristian Pela on 30.04.2018.
 */
class MinimalProfileParserTest {

    @Test
    fun parseLoggedProfile() {
        loadResource("profil.html").readText().let {
            MinimalProfileParseHelper().parse(Jsoup.parse(it))
        }.apply {
            link.assertEq(Link(RouteBehavior(MENU_LOGGED_PROFILE_SCREEN_ID,
                    Container.MENU_LOGGED_PROFILE_CONTAINER_ID,
                    hasPop = false,
                    showScreenAsChild = true), screenTag = null))
            model.assertEq(MinimalProfileModel(
                    MinimalProfile(
                    2768,
                    "criskey",
                    "CRSK",
                    "http://firstcycling.com/img/users/t2768.jpg",
                    "http://firstcycling.com/img/nat/ROM.png"),
                    4,
                    profileLink(2768)))
        }
    }
}