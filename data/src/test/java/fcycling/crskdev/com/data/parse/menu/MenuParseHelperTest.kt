package fcycling.crskdev.com.data.parse.menu

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.loadResource
import fcycling.crskdev.com.data.parse.manager.MinimalManager
import fcycling.crskdev.com.domain.screen.Container.ROOT_CONTAINER_ID
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.Screen.MENU_SCREEN_ID
import fcycling.crskdev.com.domain.screen.linkRaceManagerScreen
import org.jsoup.Jsoup
import org.junit.Test
import java.util.*

/**
 * Created by Cristian Pela on 06.05.2018.
 */
class MenuParseHelperTest {

    @Test
    fun shouldParseTheMenu() {

        val document = loadResource("profil.html").readText().let {
            Jsoup.parse(it)
        }

        val year = Calendar.getInstance().get(Calendar.YEAR)

        MenuParseHelper(year).parse(document).apply {
            link.assertEq(Link.bare(MENU_SCREEN_ID, ROOT_CONTAINER_ID).makePop(false))
            with(model!!) {
                profileLink.request?.toString().assertEq(
                        "http://firstcycling.com/profil.php?ID=2768")
                managersLink.request?.toString().assertEq(
                        "http://firstcycling.com/manager.php?aar=2018&k=komplett")
                cupStandings.request?.toString().assertEq(
                        "http://firstcycling.com/maraton.php?aar=2018")
                logout.request?.toString().assertEq("http://firstcycling.com/index.php?logout=yes")
                messagesLink.request?.toString().assertEq(
                        "http://firstcycling.com/melding.php")
                nextManager.assertEq(
                        MinimalManager(
                                814,
                                "http://firstcycling.com/img/nat/FRA.png",
                                "Dunkerque",
                                "(08/05)",
                                linkRaceManagerScreen(814)
                        )
                )
            }
        }

    }
}