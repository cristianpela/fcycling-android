package fcycling.crskdev.com.data.screen

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.connection.CookieStoreValueFinder
import fcycling.crskdev.com.data.connection.HEADER_AUTH_TOKEN
import fcycling.crskdev.com.data.connection.HEADER_AUTH_USERNAME
import fcycling.crskdev.com.domain.screen.*
import org.junit.Test
import java.net.CookieManager
import java.net.CookiePolicy
import java.net.HttpCookie
import java.net.URI


/**
 * Created by Cristian Pela on 01.05.2018.
 */
class AuthLinkSupervisorTest {

    private val cookieManager = CookieManager().apply { setCookiePolicy(CookiePolicy.ACCEPT_ALL) }
    private val uri = URI(URL_DOMAIN)
    private val link = Link(RouteBehavior(Screen.FULL_PROFILE_SCREEN_ID, Container.ROOT_CONTAINER_ID), screenTag = null)

    @Test
    fun shouldPassLinkAsUserIsAuthenticated() {
        val cookieStore = cookieManager.cookieStore.apply {
            add(uri, HttpCookie(HEADER_AUTH_TOKEN, "lpkey"))
            add(uri, HttpCookie(HEADER_AUTH_USERNAME, "criskey"))
        }
        AuthLinkSupervisor(CookieStoreValueFinder(cookieStore)).verify(link).assertEq(link)
    }

    @Test
    fun shouldReturnLoginLinkWhenUserIsNotAuthenticated() {
        val cookieStore = cookieManager.cookieStore
        AuthLinkSupervisor(CookieStoreValueFinder(cookieStore)).verify(link).assertEq(LOGIN_LINK_SCREEN_WITH_NO_REQUEST)
    }
}