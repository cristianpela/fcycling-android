package fcycling.crskdev.com.data.parse.manager

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.loadResource
import org.junit.Test

/**
 * Created by Cristian Pela on 17.06.2018.
 */
class RaceManagersOngoingUpcomingParserTest {

    @Test
    fun parse() {
        loadResource("managers_ongoing_upcoming.html", Charsets.ISO_8859_1).readText().let {
            RaceManagersOngoingUpcomingParser().parse(it)
        }.shards[1].model.toString().assertEq(
               "RaceManagersOngoingUpcomingModel(ongoing=[MinimalManager(id=779, flagUrl=http://firstcycling.com/img/nat/SUI.png, name=Suisse, date=Sat 9.Jun, 15:20 CET, link=Link(routeBehavior=RouteBehavior(screen=RACE_MANAGER_RESULTS_SCREEN_ID, container=MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, showScreenAsChild=true, hasPop=true), request=http://firstcycling.com/manres.php?konk=779, isRefreshable=false, redirectLink=null, tag=null, containerTag=null, screenTag=null)), MinimalManager(id=886, flagUrl=http://firstcycling.com/img/nat/GBR.png, name=OVO Women's Tour, date=Wed 13.Jun, 11:30 CET, link=Link(routeBehavior=RouteBehavior(screen=RACE_MANAGER_RESULTS_SCREEN_ID, container=MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, showScreenAsChild=true, hasPop=true), request=http://firstcycling.com/manres.php?konk=886, isRefreshable=false, redirectLink=null, tag=null, containerTag=null, screenTag=null)), MinimalManager(id=899, flagUrl=http://firstcycling.com/img/nat/SLO.png, name=Slovénie, date=Wed 13.Jun, 13:25 CET, link=Link(routeBehavior=RouteBehavior(screen=RACE_MANAGER_RESULTS_SCREEN_ID, container=MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, showScreenAsChild=true, hasPop=true), request=http://firstcycling.com/manres.php?konk=899, isRefreshable=false, redirectLink=null, tag=null, containerTag=null, screenTag=null))], upcoming=[MinimalManager(id=815, flagUrl=http://firstcycling.com/img/nat/ITA.png, name=Adriatica Ionica Race, date=Wed 20.Jun, link=Link(routeBehavior=RouteBehavior(screen=RACE_MANAGER_RESULTS_SCREEN_ID, container=MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, showScreenAsChild=true, hasPop=true), request=http://firstcycling.com/manres.php?konk=815, isRefreshable=false, redirectLink=null, tag=null, containerTag=null, screenTag=null)), MinimalManager(id=816, flagUrl=http://firstcycling.com/img/nat/FRA.png, name=Tour Savoie, date=Thu 21.Jun, link=Link(routeBehavior=RouteBehavior(screen=RACE_MANAGER_RESULTS_SCREEN_ID, container=MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, showScreenAsChild=true, hasPop=true), request=http://firstcycling.com/manres.php?konk=816, isRefreshable=false, redirectLink=null, tag=null, containerTag=null, screenTag=null)), MinimalManager(id=848, flagUrl=http://firstcycling.com/img/nat/NOR.png, name=CN NO, date=Sun 24.Jun, link=Link(routeBehavior=RouteBehavior(screen=RACE_MANAGER_RESULTS_SCREEN_ID, container=MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, showScreenAsChild=true, hasPop=true), request=http://firstcycling.com/manres.php?konk=848, isRefreshable=false, redirectLink=null, tag=null, containerTag=null, screenTag=null)), MinimalManager(id=851, flagUrl=http://firstcycling.com/img/nat/BEL.png, name=CN BE, date=Sun 24.Jun, link=Link(routeBehavior=RouteBehavior(screen=RACE_MANAGER_RESULTS_SCREEN_ID, container=MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, showScreenAsChild=true, hasPop=true), request=http://firstcycling.com/manres.php?konk=851, isRefreshable=false, redirectLink=null, tag=null, containerTag=null, screenTag=null))])"
        )
    }
}