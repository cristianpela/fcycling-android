package fcycling.crskdev.com.data.parse.manager

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.loadResource
import fcycling.crskdev.com.data.parse.convertHyperlinkToRequest
import fcycling.crskdev.com.data.parse.profile.MinimalProfile
import fcycling.crskdev.com.data.parse.race.RaceAbout
import fcycling.crskdev.com.data.parse.race.RaceLink
import fcycling.crskdev.com.data.parse.rider.MinimalInfoRider
import fcycling.crskdev.com.domain.screen.URL_DOMAIN
import fcycling.crskdev.com.domain.screen.linkRaceManagerSelectionTeamScreen
import fcycling.crskdev.com.domain.screen.linkRaceStartListScreen
import fcycling.crskdev.com.domain.screen.linkRiderScreen
import org.junit.Test

/**
 * Created by Cristian Pela on 08.06.2018.
 */
class RaceManagerParserTest {

    @Test
    fun parseManagerResults() {
        val rawData = loadResource("managers_manres.html", Charsets.ISO_8859_1).readText()
        //first is menu model. second is our model
        val model = RaceManagerParser().parse(rawData).shards.get(1).model as ManagerResultsModel
        model.results.apply {
            size.assertEq(130)
            get(10).assertEq(
                    ManagerResult(
                            "11",
                            MinimalProfile(3,
                                    "Luxembourg Finest",
                                    "Luxembourg Finest",
                                    null,
                                    "http://firstcycling.com/img/nat/FC.png"),
                            linkRaceManagerSelectionTeamScreen("manres.php?konk=778&lag=3"
                                    .convertHyperlinkToRequest()),
                            2867.toString(),
                            261.toString(),
                            "The Legacy of Andy 4.0",
                            "http://firstcycling.com/img/nat/FC.png",
                            MinimalInfoRider(
                                    2961,
                                    "M.Kwiatkowski",
                                    "http://firstcycling.com/img/nat/POL.png",
                                    linkRiderScreen(2961)
                            )
                    )
            )
        }
        model.raceLink.assertEq(
                RaceLink(linkRaceStartListScreen(15, 2018),
                        RaceAbout("Critérium Dauphiné",
                                "2018",
                                "http://firstcycling.com/img/nat/FRA.png"))
        )

    }

    @Test
    fun parseManagerRegistration() {
        val rawData = loadResource("managers_registration.html", Charsets.ISO_8859_1).readText()
        //first is menu model. second is our model
        val model = RaceManagerParser().parse(rawData).shards.get(1).model as ManagerRegistrationModel
        model.id.assertEq(815)
        model.registeredManagerPlayers.run {
            size.assertEq(48)
            first().assertEq(MinimalProfile(1983, "Blancos", "Blancos", null,
                    "$URL_DOMAIN/img/nat/NOR.png"))
            last().assertEq(MinimalProfile(2949, "Vagabonds", "Vagabonds", null,
                    "$URL_DOMAIN/img/nat/THA.png"))
        }
    }
}