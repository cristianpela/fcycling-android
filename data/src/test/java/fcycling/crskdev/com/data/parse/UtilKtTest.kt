package fcycling.crskdev.com.data.parse

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.utils.multiZipped
import fcycling.crskdev.com.domain.connection.Request
import org.junit.Test

/**
 * Created by Cristian Pela on 14.05.2018.
 */
class UtilKtTest {

    @Test
    fun extractLinkQueryParams() {
        "http://firstcycling.com/manres.php?konk=527&lag=2768"
                .convertHyperlinkToRequest().assertEq(
                        Request("http://firstcycling.com/manres.php",
                                mapOf("konk" to "527", "lag" to "2768")))
        "http://firstcycling.com/manres.php"
                .convertHyperlinkToRequest().assertEq(
                        Request("http://firstcycling.com/manres.php"))
    }

    @Test
    fun multiZipTesting() {
        multiZipped(listOf(1, 2, 3),
                listOf(4, 5, 6),
                listOf(7, 8, 9)) {
            Triple(it[0], it[1], it[2])
        }.assertEq(
                listOf(Triple(1, 4, 7),
                        Triple(2, 5, 8),
                        Triple(3, 6, 9))
        )

    }
}