package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.loadResource
import fcycling.crskdev.com.domain.screen.linkProfileInfoStatsScreen
import org.jsoup.Jsoup
import org.junit.Test

/**
 * Created by Cristian Pela on 15.05.2018.
 */
class ProfileStatsLinksParserTest {

    @Test
    fun parse() {
        loadResource("profil.html").readText().let {
        ProfileStatsLinksParseHelper().parse(Jsoup.parse(it))
    }.model.assertEq(ProfileStatsLinksModel(2768,
                ProfileStatsLinks(
                        CompetitionGender.MEN,
                        linkProfileInfoStatsScreen(2768, 2018, true),
                        listOf(ProfileStatsLinks.YearLink(2018,
                                linkProfileInfoStatsScreen(2768, 2018, false),
                                true),
                                ProfileStatsLinks.YearLink(2017,
                                        linkProfileInfoStatsScreen(2768, 2017, false),
                                        false),
                                ProfileStatsLinks.YearLink(2016,
                                        linkProfileInfoStatsScreen(2768, 2016, false),
                                        false)))
        ))
    }

    @Test
    fun shouldParseProfileInfoModelAndy() {
        loadResource("profil_andy.html").readText().let {
            ProfileStatsLinksParseHelper().parse(Jsoup.parse(it)).apply {
                println(this.model)
            }
        }
    }

    @Test
    fun shouldParseProfileInfoModelJJJ() {
        loadResource("profil_jjj.html").readText().let {
            ProfileStatsLinksParseHelper().parse(Jsoup.parse(it)).apply {
                println(this.model)
            }
        }
    }

}