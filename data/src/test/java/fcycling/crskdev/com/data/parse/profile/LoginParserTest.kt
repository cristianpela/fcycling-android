package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.loadResource
import fcycling.crskdev.com.data.parse.login.LoginModel
import fcycling.crskdev.com.data.parse.login.LoginParser
import fcycling.crskdev.com.domain.parse.ErrorModel
import fcycling.crskdev.com.domain.screen.profileLink
import org.junit.Test

/**
 * Created by Cristian Pela on 01.05.2018.
 */
class LoginParserTest {

    @Test
    fun shouldReturnErrorModelWhenNotLoggedIntWithCredentials() {
        loadResource("login_error.html").readText().let {
            LoginParser().parse(it)
        }.apply {
            true.assertEq(this is ErrorModel)
            (this as ErrorModel).error.message.equals(
                    "Errors: - You didnt use a right combination of username (or e-mail) and password"
            )
        }
    }

    @Test
    fun shouldReturnLoginModelWhenLoggedInWhenAreDuels() {
        loadResource("login_success_fora_with_duels.html").readText().let {
            LoginParser().parse(it)
        }.assertEq(LoginModel(2768, "criskey", profileLink(2768)))
    }

    @Test
    fun shouldReturnLoginModelWhenLoggedInWhenAreNoDuels() {
        loadResource("login_success_fora_no_duels.html").readText().let {
            LoginParser().parse(it)
        }.assertEq(LoginModel(2768, "criskey", profileLink(2768)))
    }
}