package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.loadResource
import fcycling.crskdev.com.data.parse.convertHyperlinkToRequest
import fcycling.crskdev.com.data.parse.profile.FullProfileModel.Info
import fcycling.crskdev.com.data.parse.profile.FullProfileModel.Info.Badge
import fcycling.crskdev.com.domain.screen.URL_DOMAIN
import fcycling.crskdev.com.domain.screen.linkProfileInfoStatsScreen
import org.jsoup.Jsoup
import org.junit.Test

/**
 * Created by Cristian Pela on 14.05.2018.
 */
class FullProfileParserTest {

    @Test
    fun shouldParseProfileInfoModel() {
        loadResource("profil.html").readText().let {
            FullProfileParseHelper().parse(Jsoup.parse(it)).model.info
        }.assertEq(Info(
                "Timisoara",
                "Romania",
                "http://firstcycling.com/img/nat/ROM.png",
                "Apr 27th, 2016",
                listOf(
                        Badge("http://firstcycling.com/img/riders/87.jpg",
                                "Zubeldia: had Haimar Zubeldia on the team in his last race, San Sebastian 2017"),
                        Badge("http://firstcycling.com/img/riders/27.jpg",
                                "Scarponi: had Michele Scarponi on the team in his last race, Tour of the Alps 2017"),
                        Badge("http://firstcycling.com/img/riders/33.jpg",
                                "Boonen: had Tom Boonen as captain for his last race, Paris-Roubaix 2017")
                )
        ))
    }

    @Test
    fun shouldParseProfileInfoModelJJJ() {
        loadResource("profil_jjj.html").readText().let {
            FullProfileParseHelper().parse(Jsoup.parse(it)).apply {
                model.info.assertEq(
                        Info(
                                "Germany",
                                "Germany",
                                "http://firstcycling.com/img/nat/GER.png",
                                "Jun 4th, 2016",
                                listOf(
                                        Badge("http://firstcycling.com/img/riders/87.jpg",
                                                "Zubeldia: had Haimar Zubeldia on the team in his last race, San Sebastian 2017"),
                                        Badge("http://firstcycling.com/img/riders/27.jpg",
                                                "Scarponi: had Michele Scarponi on the team in his last race, Tour of the Alps 2017")
                                )
                        ))
                model.minimalProfile.assertEq(
                        MinimalProfile(
                                2863,
                                "johnjackjoe",
                                "JohnJackJoe Racing",
                                null,
                                "http://firstcycling.com/img/nat/GER.png"
                        )
                )
                model.statsLinks.assertEq(
                        ProfileStatsLinks(
                                CompetitionGender.MEN,
                                "http://firstcycling.com/profil.php?ID=2863&aar=2018&kj=F"
                                        .convertHyperlinkToRequest()
                                        .let { linkProfileInfoStatsScreen(it) },
                                listOf(
                                        ProfileStatsLinks.YearLink(2018,
                                                "$URL_DOMAIN/profil.php?ID=2863&k=&aar=2018".convertHyperlinkToRequest().let {
                                                    linkProfileInfoStatsScreen(it)
                                                }, true),
                                        ProfileStatsLinks.YearLink(2017,
                                                "$URL_DOMAIN/profil.php?ID=2863&k=&aar=2017".convertHyperlinkToRequest().let {
                                                    linkProfileInfoStatsScreen(it)
                                                }),
                                        ProfileStatsLinks.YearLink(2016,
                                                "$URL_DOMAIN/profil.php?ID=2863&k=&aar=2016".convertHyperlinkToRequest().let {
                                                    linkProfileInfoStatsScreen(it)
                                                }
                                        )
                                )
                        ))
            }
        }
    }


    @Test
    fun shouldParseProfileInfoModelRuvu() {
        loadResource("profil_ruvu.html").readText().let {
            FullProfileParseHelper().parse(Jsoup.parse(it)).model.info
        }.assertEq(Info(
                null,
                "Netherlands",
                "http://firstcycling.com/img/nat/NED.png",
                "Aug 9th, 2014",
                listOf(
                        Badge("http://firstcycling.com/img/riders/33.jpg",
                                "Boonen: had Tom Boonen as captain for his last race, Paris-Roubaix 2017"),
                        Badge("http://firstcycling.com/img/riders/118.jpg",
                                "Thor: had Thor Hushovd on his team at his last race, Impanis-Van Petegem 2014")
                )

        ))
    }
}