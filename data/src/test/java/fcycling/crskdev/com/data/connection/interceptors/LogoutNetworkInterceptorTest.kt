package fcycling.crskdev.com.data.connection.interceptors

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.connection.HEADER_CUSTOM_IS_CACHEABLE
import fcycling.crskdev.com.data.connection.HEADER_SET_COOKIE
import fcycling.crskdev.com.data.connection.interceptors.LogoutNetworkInterceptor.Companion.LOGGED_OUT_MESSAGE
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Test


/**
 * Created by Cristian Pela on 10.06.2018.
 */
class LogoutNetworkInterceptorTest {

    @Test
    fun intercept() {

        val path = "/test"

        val server = MockWebServer().apply {
            enqueue(MockResponse()
                    .setBody("")
                    .addHeader(HEADER_SET_COOKIE, "lp=deleted; expires=Sat, 10-Jun-2017 09:34:51 GMT")
                    .addHeader(HEADER_SET_COOKIE, "navn=deleted; expires=Sat, 10-Jun-2017 09:34:51 GMT")
            )
        }
        server.start()
        val url = server.url(path)

        val client = OkHttpClient.Builder()
                .addNetworkInterceptor(CacheNetworkInterceptor())
                .addNetworkInterceptor(LogoutNetworkInterceptor())
                .addNetworkInterceptor(ContentTypeTo_ISO_8859_1_NetworkInterceptor())
                .build()

        val response = client
                .newCall(Request.Builder().url(url).addHeader(HEADER_CUSTOM_IS_CACHEABLE, "true").build())
                .execute()

        with(response){
            isSuccessful.assertEq(false)
            code().assertEq(401)
            body()?.string().assertEq(LOGGED_OUT_MESSAGE)
        }

        server.shutdown()

    }
}