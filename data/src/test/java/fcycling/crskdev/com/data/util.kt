package fcycling.crskdev.com.data

import java.io.InputStreamReader
import java.nio.charset.Charset
import kotlin.test.assertEquals

/**
 * Created by Cristian Pela on 30.04.2018.
 */
inline fun <reified T> T.loadResource(file: String, charset: Charset = Charset.defaultCharset()) = InputStreamReader(T::class.java.classLoader
        .getResourceAsStream(file), charset)

fun <T> T.assertEq(expected: T, msg: String? = null) =
        assertEquals(expected, this, msg)
