package fcycling.crskdev.com.data.parse.profile

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.loadResource
import fcycling.crskdev.com.data.parse.rider.MinimalInfoRider
import fcycling.crskdev.com.data.parse.rider.MostUsedCaptain
import fcycling.crskdev.com.domain.screen.URL_DOMAIN
import fcycling.crskdev.com.domain.screen.linkRiderScreen
import org.jsoup.Jsoup
import org.junit.Test

/**
 * Created by Cristian Pela on 15.05.2018.
 */
class ProfileStatsParseHelperTest {


    @Test
    fun profileStatsParserRUVU() {

        loadResource("profil_ruvu.html").readText().let {
            ProfileStatsParseHelper().parse(Jsoup.parse(it))
        }.model.let {
            /**
            No.1	Season competition
            No.2	One-day races
            No.1	Stage races
            No.2	Spring Classics
            No.11	Grand Tour competition
            No.1	Form (Last 6 Weeks)

             */
            it.stats?.profileSeasonStandings.assertEq(
                    listOf(
                            SeasonStanding(1, "Season competition"),
                            SeasonStanding(2, "One-day races"),
                            SeasonStanding(1, "Stage races"),
                            SeasonStanding(2, "Spring Classics"),
                            SeasonStanding(11, "Grand Tour competition"),
                            SeasonStanding(1, "Form (Last 6 Weeks)")
                    ))
            /**
            June	1	1026	5 / 5	100 %
            Mai	    3	1674	10 / 10	100 %
            April	14	1771	14 / 14	100 %
            March	7	1629	14 / 14	100 %
            February3	2537	15 / 15	100 %
            January	39	511	    4 / 4	100 %
             */
            it.stats?.profileMonthlyStandings.assertEq(listOf(
                    MonthlyStanding("June", 1, 1026, "5 / 5", "100 %"),
                    MonthlyStanding("Mai", 3, 1674, "10 / 10", "100 %"),
                    MonthlyStanding("April", 14, 1771, "14 / 14", "100 %"),
                    MonthlyStanding("March", 7, 1629, "14 / 14", "100 %"),
                    MonthlyStanding("February", 3, 2537, "15 / 15", "100 %"),
                    MonthlyStanding("January", 39, 511, "4 / 4", "100 %")
            ))
            it.stats?.mostUsedCaptains.assertEq(listOf(
                    MostUsedCaptain(MinimalInfoRider(38, "A.Valverde", "$URL_DOMAIN/img/nat/ESP.png", linkRiderScreen(38)), 6),
                    MostUsedCaptain(MinimalInfoRider(784, "P.Sagan", "$URL_DOMAIN/img/nat/SVK.png", linkRiderScreen(784)), 4),
                    MostUsedCaptain(MinimalInfoRider(716, "G.Thomas", "$URL_DOMAIN/img/nat/GBR.png", linkRiderScreen(716)), 4),
                    MostUsedCaptain(MinimalInfoRider(18575, "F.Gaviria", "$URL_DOMAIN/img/nat/COL.png", linkRiderScreen(18575)), 3))
            )

            it.stats?.managerRaces?.forEach {
                println(it)
            }
        }

    }


    @Test
    fun parseCRSK() {
        loadResource("profil.html").readText().let {
            ProfileStatsParseHelper().parse(Jsoup.parse(it))
        }.model.let {
            it.stats?.profileSeasonStandings.assertEq(
                    /*
No.4	Season competition
No.23	One-day races
No.4	Stage races
No.9	Spring Classics
No.1	Form (Last 6 Weeks)

                    * */
                    listOf(
                            SeasonStanding(4, "Season competition"),
                            SeasonStanding(23, "One-day races"),
                            SeasonStanding(4, "Stage races"),
                            SeasonStanding(9, "Spring Classics"),
                            SeasonStanding(1, "Form (Last 6 Weeks)")
                    ))
            /*
Mai	    10	    166	    1 / 1	100 %
April	3	    2017	14 / 14	100 %
March	16	    1502	14 / 14	100 %
February13	    2194	15 / 15	100 %
January	53	    487	    4 / 4	100 %
            */
            it.stats?.profileMonthlyStandings.assertEq(listOf(
                    MonthlyStanding("Mai", 10, 166, "1 / 1", "100 %"),
                    MonthlyStanding("April", 3, 2017, "14 / 14", "100 %"),
                    MonthlyStanding("March", 16, 1502, "14 / 14", "100 %"),
                    MonthlyStanding("February", 13, 2194, "15 / 15", "100 %"),
                    MonthlyStanding("January", 53, 487, "4 / 4", "100 %")
            ))
            /*
img/nat/ESP.png 38      A.Valverde (5),
img/nat/FRA.png 22268   L.Calmejane (3),
img/nat/SVK.png 784     P.Sagan (3).
            */
            it.stats?.mostUsedCaptains.assertEq(listOf(
                    MostUsedCaptain(MinimalInfoRider(38, "A.Valverde", "$URL_DOMAIN/img/nat/ESP.png", linkRiderScreen(38)), 5),
                    MostUsedCaptain(MinimalInfoRider(22268, "L.Calmejane", "$URL_DOMAIN/img/nat/FRA.png", linkRiderScreen(22268)), 3),
                    MostUsedCaptain(MinimalInfoRider(784, "P.Sagan", "$URL_DOMAIN/img/nat/SVK.png", linkRiderScreen(784)), 3)
            ))

            it.stats?.managerRaces?.forEach {
                println(it)
            }
        }

    }
}