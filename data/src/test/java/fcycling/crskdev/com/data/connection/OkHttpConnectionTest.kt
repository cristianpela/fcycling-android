package fcycling.crskdev.com.data.connection


import fcycling.crskdev.com.domain.connection.ErrorResponse
import fcycling.crskdev.com.domain.connection.RawResponse
import fcycling.crskdev.com.domain.connection.Request
import fcycling.crskdev.com.domain.screen.LOGIN_URL
import fcycling.crskdev.com.domain.screen.LOGOUT_LINK_SCREEN
import fcycling.crskdev.com.domain.screen.URL_DOMAIN
import fcycling.crskdev.com.domain.screen.loginLinkSubmit
import okhttp3.HttpUrl
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.internal.http.BridgeInterceptor
import org.junit.Test

import org.junit.Assert.*
import java.lang.Thread.sleep
import java.net.CookieManager
import java.net.CookiePolicy
import java.net.URI
import java.net.URL

/**
 * Created by Cristian Pela on 30.04.2018.
 */
class OkHttpConnectionTest {

    @Test
    fun request() {
//        val conn = OkHttpConnection(OkHttpClient())
//        val response = conn.request(Request("http://firstcycling.com/profil.php",queryParams = mapOf("ID" to "2768")))
//        when(response){
//            is RawResponse -> LoggedProfileParser().parse(response.rawData).apply { println(this) }
//            is ErrorResponse -> println(response.error)
//        }
    }

    @Test
    fun shouldLogin() {

//        val cookieManager = CookieManager().apply { setCookiePolicy(CookiePolicy.ACCEPT_ALL) }
//        val cookieJar =JavaNetCookieJar(cookieManager)
//        val client = OkHttpClient.Builder()
//                .cookieJar(cookieJar).build()
//
//        val connection = OkHttpConnection(client)
//
//        connection.request(Request(LOGIN_URL)).apply {
//            when (this) {
//                is ErrorResponse -> println(error)
//                else -> println("Session established")
//            }
//        }
//
//        cookieManager.cookieStore[URI(URL_DOMAIN)].let {
//            println("Cookies before login : $it")
//        }
//
//        sleep(2000)
//
//        val requestLogInSubmit = loginLinkSubmit("criskey", "26041985").request
//                ?: throw Error("Ups no request submitted")
//        connection.request(requestLogInSubmit).apply {
//            when (this) {
//                is ErrorResponse -> println(error)
//                else -> Unit
//            }
//        }
//
//        cookieManager.cookieStore[URI(URL_DOMAIN)].let {
//            println("Cookies after login: $it")
//        }
//
//        sleep(3000)
//
//        val requestLogout = LOGOUT_LINK_SCREEN.request
//                ?: throw Error("Ups no request submitted")
//        connection.request(requestLogout)
//
//        cookieManager.cookieStore[URI(URL_DOMAIN)].let {
//            println("Cookies after logout: $it")
//        }
    }
}