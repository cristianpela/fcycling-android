package fcycling.crskdev.com.data.parse.manager

import fcycling.crskdev.com.data.loadResource
import org.junit.Test

/**
 * Created by Cristian Pela on 18.06.2018.
 */
class RaceManagerTeamSelectedParserTest {

    //todo do real tests not just prints
    @Test
    fun parseRaceManagerTeamSelectedNew() {
        val rawData = loadResource("managers_picks_new2.html", Charsets.ISO_8859_1).readText()
        val model = RaceManagerTeamSelectedParser().parse(rawData).shards[1].model as RaceManagerTeamSelectionModel
        println(model)
    }

    @Test
    fun parseRaceManagerTeamSelectedOld() {
        val rawData = loadResource("managers_picks_old.html", Charsets.ISO_8859_1).readText()
        val model = RaceManagerTeamSelectedParser().parse(rawData).shards[1].model as RaceManagerTeamSelectionModel
        println(model)
    }

    @Test
    fun parseRaceManagerTeamSelectedOldNoStages() {
        val rawData = loadResource("managers_picks_old_no_stages.html", Charsets.ISO_8859_1).readText()
        val model = RaceManagerTeamSelectedParser().parse(rawData).shards[1].model as RaceManagerTeamSelectionModel
        println(model)
    }
}