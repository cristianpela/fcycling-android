package fcycling.crskdev.com.data.parse.manager

import fcycling.crskdev.com.data.assertEq
import fcycling.crskdev.com.data.loadResource
import fcycling.crskdev.com.domain.screen.linkRaceManagersAvailableRidingTeamsScreen
import org.jsoup.Jsoup
import org.junit.Test

/**
 * Created by Cristian Pela on 19.06.2018.
 */
class RaceManagerAvailableRidingTeamsParseHelperTest {

    @Test
    fun parseRaceManagerAvailableRidingTeams() {
        val rawDataNew = loadResource("managers_picks_new2.html", Charsets.ISO_8859_1).readText()
        val shardNew = RaceManagerAvailableRidingTeamsParseHelper()
                .parse(Jsoup.parse(rawDataNew))
        shardNew.link.assertEq(linkRaceManagersAvailableRidingTeamsScreen(816))

        val rawDataOld = loadResource("managers_picks_old.html", Charsets.ISO_8859_1).readText()
        val shardOld = RaceManagerAvailableRidingTeamsParseHelper()
                .parse(Jsoup.parse(rawDataOld))
        shardOld.link.assertEq(linkRaceManagersAvailableRidingTeamsScreen(816))
        val teamsOld = shardOld.model?.ridingTeams ?: emptyList()
        val teamsNew = shardNew.model?.ridingTeams ?: emptyList()
        teamsNew.size.assertEq(teamsNew.size)

        (0 until teamsNew.size).forEach {
            teamsNew[it].assertEq(teamsOld[it])
        }
    }


}