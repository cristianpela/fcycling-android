package fcycling.crskdev.com.domain.parse

import fcycling.crskdev.com.domain.assertEq
import fcycling.crskdev.com.domain.connection.*
import fcycling.crskdev.com.domain.schedulers.SchedulersContract
import fcycling.crskdev.com.domain.screen.*
import fcycling.crskdev.com.domain.screen.Container.ROOT_CONTAINER_ID
import fcycling.crskdev.com.domain.screen.Screen.*
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import org.junit.Test

/**
 * Created by Cristian Pela on 29.04.2018.
 */
class RequestProcessorTest {

    @Test
    fun shouldEmitCorrectScreenModels() {

        val parsers = mapOf(MENU_SCREEN_ID to HelloParser(),
                FULL_PROFILE_SCREEN_ID to WorldParser())

        val requestProcessor = RequestProcessor(MockConnection(),
                ParserFactory(parsers),
                DummyLinkRefreshHandler,
                DummyLinkSupervisor(),
                MockSchedulersContract())

        val linkMenu = Link(RouteBehavior(MENU_SCREEN_ID, ROOT_CONTAINER_ID), Request("/menu"), screenTag = null)
        val linkProfile = Link(RouteBehavior(FULL_PROFILE_SCREEN_ID, ROOT_CONTAINER_ID), Request("/profile"), screenTag = null)

        Observable.fromArray(linkMenu, linkProfile)
                .compose(requestProcessor.process())
                .test()
                .values()
                .assertEq(listOf(
                        ScreenModel.Wait,
                        ScreenModel.DataModel(linkMenu, HelloModel("Hello")),
                        ScreenModel.Wait,
                        ScreenModel.DataModel(linkProfile, WorldModel("World"))))
    }


    @Test
    fun shouldEmitLoginScreenWhenGettingLoggedOutError() {
        val parsers = mapOf(MENU_SCREEN_ID to DummyParser())
        val connection = object : Connection {
            override fun request(request: Request, tag: String?): Response =
                    LoggedOutErrorResponse(Error(""))
            override fun cancel(tag: String?) = Unit
        }
        val requestProcessor = RequestProcessor(connection, ParserFactory(parsers),
                DummyLinkRefreshHandler,
                DummyLinkSupervisor(),
                MockSchedulersContract())
        val link = Link(RouteBehavior(MENU_SCREEN_ID, ROOT_CONTAINER_ID), Request("/foo"), screenTag = null)
        Observable.just(link)
                .compose(requestProcessor.process())
                .test()
                .assertValueAt(0) {
                    it === ScreenModel.Wait
                }
                .assertValueAt(1) {
                    it == ScreenModel.DataModel(LOGIN_LINK_SCREEN_WITH_NO_REQUEST)
                }
    }

    @Test
    fun shouldEmitErrorScreenModelWhenGettingErrorRequest() {
        val parsers = mapOf(MENU_SCREEN_ID to DummyParser())
        val requestProcessor = RequestProcessor(ErrorMockConnection(), ParserFactory(parsers),
                DummyLinkRefreshHandler,
                DummyLinkSupervisor(),
                MockSchedulersContract())
        val link = Link(RouteBehavior(MENU_SCREEN_ID, ROOT_CONTAINER_ID), Request("/foo"), screenTag = null)
        Observable.just(link)
                .compose(requestProcessor.process())
                .test()
                .assertValueAt(0) {
                    it === ScreenModel.Wait
                }
                .assertValueAt(1) {
                    it is ScreenModel.ErrorModel
                }
    }

    @Test
    fun shouldEmitAnEmptyModelWhenIsNoRequest() {
        val requestProcessor = RequestProcessor(DummyConnection(), ParserFactory(emptyMap()),
                DummyLinkRefreshHandler,
                DummyLinkSupervisor(),
                MockSchedulersContract())
        val link = Link(RouteBehavior(FULL_PROFILE_SCREEN_ID, ROOT_CONTAINER_ID), null, screenTag = null)
        Observable.just(link)
                .compose(requestProcessor.process())
                .test()
                .assertValueAt(0) {
                    it === ScreenModel.Wait
                }
                .assertValueAt(1) {
                    it is ScreenModel.DataModel && it.model == null
                }
    }

    @Test
    fun shouldRedirectToLogInScreenWhenNotAuthorized() {
        val loginScreenId = LOGIN_SCREEN_ID
        val requestProcessor = RequestProcessor(DummyConnection(),
                ParserFactory(mapOf(loginScreenId to DummyParser())),
                DummyLinkRefreshHandler,
                MockAuthLinkSupervisor(false),
                MockSchedulersContract())
        val link = Link(RouteBehavior(FULL_PROFILE_SCREEN_ID, ROOT_CONTAINER_ID), Request("/profile"), screenTag = null)
        Observable.just(link)
                .compose(requestProcessor.process())
                .test()
                .assertValueAt(0) {
                    it === ScreenModel.Wait
                }
                .assertValueAt(1) {
                    it is ScreenModel.DataModel &&
                            it.originalLink.routeBehavior.screen == loginScreenId
                }
    }

    @Test
    fun shouldRedirectToWorldScreenViaHelloScreen() {
        val parsers = mapOf(MENU_SCREEN_ID to HelloParser(),
                FULL_PROFILE_SCREEN_ID to WorldParser())


        val requestProcessor = RequestProcessor(MockConnection(), ParserFactory(parsers),
                DummyLinkRefreshHandler,
                DummyLinkSupervisor(),
                MockSchedulersContract())

        val worldLink = Link(RouteBehavior(FULL_PROFILE_SCREEN_ID, ROOT_CONTAINER_ID), Request("/world"), screenTag = null)
        val helloLink = Link(RouteBehavior(MENU_SCREEN_ID, ROOT_CONTAINER_ID), Request("/hello"), redirectLink = worldLink, screenTag = null)

        Observable.just(helloLink)
                .compose(requestProcessor.process())
                .test()
                .apply {
                    println(values())
                }
                .assertValueAt(0) {
                    it === ScreenModel.Wait
                }
                .assertValueAt(1) {
                    it == ScreenModel.DataModel(worldLink, WorldModel("World"))
                }
    }

    @Test
    fun shouldRedirectToWorldScreenViaHelloScreenUsingPlaceHolders() {
        val parsers = mapOf(MENU_SCREEN_ID to HelloParser(),
                FULL_PROFILE_SCREEN_ID to WorldParser())


        val requestProcessor = RequestProcessor(MockConnection(), ParserFactory(parsers),
                DummyLinkRefreshHandler,
                DummyLinkSupervisor(),
                MockSchedulersContract())

        val worldLink = Link(RouteBehavior(FULL_PROFILE_SCREEN_ID, ROOT_CONTAINER_ID), Request("/world", queryParams =
        mapOf("hello" to QP_PLACEHOLDER)), screenTag = null)
        val helloLink = Link(RouteBehavior(MENU_SCREEN_ID, ROOT_CONTAINER_ID), Request("/hello"), redirectLink = worldLink, screenTag = null)

        Observable.just(helloLink)
                .compose(requestProcessor.process())
                .test()
                .assertValueAt(0) {
                    it === ScreenModel.Wait
                }
                .assertValueAt(1) {
                    it == ScreenModel.DataModel(worldLink.copy(request = worldLink.request?.copy(queryParams = mapOf("hello" to "Hello"))),
                            WorldModel("Hello"))
                }
    }

    @Test
    fun shouldCancelRequest() {
        val contract = MockSchedulersContract()
        val requestProcessor = RequestProcessor(MockConnection(), ParserFactory(emptyMap()),
                DummyLinkRefreshHandler,
                DummyLinkSupervisor(),
                contract)
        Observable.just(Link.CANCEL).compose(requestProcessor.process())
                .test().assertValue(ScreenModel.Cancel)

    }

}

class ErrorMockConnection : Connection {

    companion object {
        const val MESSAGE = "Could not connect to server"
    }

    override fun request(request: Request, tag: String?): Response =
            ErrorResponse(Error(MESSAGE))


    override fun cancel(tag: String?) = Unit

}

object DummyLinkRefreshHandler : LinkRefreshHandler

object DummyModel : Model
class DummyParser : ResponseParser<DummyModel> {
    override fun parse(rawData: String, otherModel: Model?): DummyModel = DummyModel
}

class DummyConnection : Connection {
    override fun request(request: Request, tag: String?): Response = RawResponse(emptyMap(), "")
    override fun cancel(tag: String?) = Unit
}

class DummyLinkSupervisor : LinkSupervisor {
    override fun verify(link: Link): Link = link
}

class MockAuthLinkSupervisor(private val isAuthenticated: Boolean) : LinkSupervisor {

    companion object {
        val LOG_IN_LINK = Link(RouteBehavior(LOGIN_SCREEN_ID, ROOT_CONTAINER_ID), Request(
                "/login.php"), screenTag = null)
    }

    override fun verify(link: Link): Link =
            if (isAuthenticated) link else LOG_IN_LINK


}

class MockConnection : Connection {

    override fun request(request: Request, tag: String?): Response =
            RawResponse(emptyMap(), if (request.queryParams.isEmpty()) "Hello World" else
                request.queryParams.values.joinToString(","))

    override fun cancel(tag: String?) = Unit

}

data class HelloModel(val hello: String?) : Model
class HelloParser : ResponseParser<HelloModel> {

    override fun parse(rawData: String, otherModel: Model?): HelloModel {
        val toFind = "Hello"
        val hello = rawData.indexOf(toFind).takeIf { it != -1 }?.let {
            rawData.substring(it, it + toFind.length)
        }
        return HelloModel(hello ?: rawData)
    }
}

data class WorldModel(@property:RedirectQueryParam("hello") val world: String?) : Model
class WorldParser : ResponseParser<WorldModel> {
    override fun parse(rawData: String, otherModel: Model?): WorldModel {
        val toFind = "World"
        val world = rawData.indexOf(toFind).takeIf { it != -1 }?.let {
            rawData.substring(it, it + toFind.length)
        }
        return WorldModel(world ?: rawData)
    }
}


class MockSchedulersContract : SchedulersContract {
    var trampoline = Schedulers.trampoline()

    var io = Schedulers.trampoline()
    var ui = Schedulers.trampoline()
    var computation = Schedulers.trampoline()
    var single = Schedulers.trampoline()
    override fun trampoline(): Scheduler = trampoline

    override fun io(): Scheduler = io

    override fun ui(): Scheduler = ui

    override fun computation(): Scheduler = computation

    override fun single(): Scheduler = single

}