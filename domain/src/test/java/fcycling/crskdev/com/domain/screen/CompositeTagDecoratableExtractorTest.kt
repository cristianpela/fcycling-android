package fcycling.crskdev.com.domain.screen

import fcycling.crskdev.com.domain.assertEq
import org.junit.Test

/**
 * Created by Cristian Pela on 13.06.2018.
 */
class CompositeTagDecoratableExtractorTest {

    @Test
    fun extract() {
         val screenTag = Screen.FULL_PROFILE_SCREEN_ID.decorateSingle(2768).apply {
            assertEq("${Screen.FULL_PROFILE_SCREEN_ID}#2768")
        }
        val containerTag = Container.PROFILE_INFO_STATS_CONTAINER_ID.decorate(2768, 2018).apply {
            assertEq("${Container.PROFILE_INFO_STATS_CONTAINER_ID}#2768#2018")
        }

        val composite = TagDecoratable.compose(screenTag, containerTag).apply {
            assertEq(
                    "${Screen.FULL_PROFILE_SCREEN_ID}#2768:${Container.PROFILE_INFO_STATS_CONTAINER_ID}#2768#2018"
            )
        }

        TagDecoratable.extract(composite).assertEq(listOf(screenTag, containerTag))
    }
}