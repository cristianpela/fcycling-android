package fcycling.crskdev.com.domain

import kotlin.test.assertEquals
/**
 * Created by Cristian Pela on 29.04.2018.
 */
fun <T> T.assertEq(expected: T, msg: String? = null) =
        assertEquals(expected, this, msg)