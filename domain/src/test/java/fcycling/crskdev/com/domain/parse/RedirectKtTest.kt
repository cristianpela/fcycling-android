package fcycling.crskdev.com.domain.parse

import fcycling.crskdev.com.domain.assertEq
import fcycling.crskdev.com.domain.connection.Request
import org.junit.Test

/**
 * Created by Cristian Pela on 12.05.2018.
 */
class RedirectKtTest {

    private val expected = Request("/foo", mapOf(
            "id" to "Hello World"
    ))

    @Test
    fun interpretRedirectParamsPlaceholdersThroughPropertyAnnotation() {
        Request("/foo", mapOf(
                "id" to QP_PLACEHOLDER
        )).interpretRedirectParamsPlaceholders(FooModelAnnotation())
                .assertEq(expected)
    }

    @Test
    fun interpretRedirectParamsPlaceholdersThroughPropertyName() {
        Request("/foo", mapOf(
                "id" to QP_PLACEHOLDER
        )).interpretRedirectParamsPlaceholders(FooModelProperty())
                .assertEq(expected)
    }

    @Test
    fun interpretRedirectParamsPlaceholdersMixed() {
        Request("/foo", mapOf(
                "id" to QP_PLACEHOLDER,
                "bye" to QP_PLACEHOLDER
        )).interpretRedirectParamsPlaceholders(FooModelMix())
                .assertEq(Request("/foo", mapOf(
                        "id" to "Hello World",
                        "bye" to "Bye World"
                )))
    }

    @Test
    fun shouldFallThroughWhenNoPlaceholderPresent() {
        Request("/foo", mapOf(
                "id" to "Hello World"
        )).interpretRedirectParamsPlaceholders(FooModelProperty())
                .assertEq(expected)
    }
}

private data class FooModelAnnotation(@property:RedirectQueryParam("id") val someProperty: String = "Hello World") : Model
private data class FooModelMix(@property:RedirectQueryParam("id") val someProperty: String = "Hello World"
                               , val bye: String = "Bye World") : Model
private data class FooModelProperty(val id: String = "Hello World") : Model