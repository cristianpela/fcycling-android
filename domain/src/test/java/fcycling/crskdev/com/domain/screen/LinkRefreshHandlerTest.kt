package fcycling.crskdev.com.domain.screen

import fcycling.crskdev.com.domain.assertEq
import fcycling.crskdev.com.domain.screen.Container.ROOT_CONTAINER_ID
import fcycling.crskdev.com.domain.screen.Screen.FULL_PROFILE_SCREEN_ID
import fcycling.crskdev.com.domain.screen.Screen.MENU_LOGGED_PROFILE_SCREEN_ID
import io.reactivex.Observable
import org.junit.Test

/**
 * Created by Cristian Pela on 02.05.2018.
 */
class LinkRefreshHandlerTest {

    @Test
    fun shouldRefreshPreviousLink() {
        val linkRefreshHandler = LinkRefreshHandlerImpl()
        val link1 = Link(RouteBehavior(MENU_LOGGED_PROFILE_SCREEN_ID, ROOT_CONTAINER_ID), isRefreshable = true, screenTag = null)
        val link2 = Link(RouteBehavior(FULL_PROFILE_SCREEN_ID, ROOT_CONTAINER_ID), isRefreshable = true, screenTag = null)
        val link3 = Link(RouteBehavior(MENU_LOGGED_PROFILE_SCREEN_ID, ROOT_CONTAINER_ID), screenTag = null)


        Observable
                .fromArray(
                        Link.REFRESH,
                        Link.REFRESH,
                        link1,
                        Link.REFRESH,
                        link2,
                        Link.REFRESH,
                        link3,
                        Link.REFRESH,
                        link1,
                        link1,
                        Link.REFRESH,
                        Link.REFRESH,
                        Link.REFRESH,
                        link3,
                        Link.REFRESH,
                        Link.REFRESH
                )
                .compose(linkRefreshHandler.handleRefresh())
                .test()
                .values().assertEq(
                        listOf(link1, link1, link2, link2, link3,
                                link1, link1, link1, link1, link1, link3))

    }
}