package fcycling.crskdev.com.domain.screen

import io.reactivex.ObservableTransformer

/**
 * Created by Cristian Pela on 30.04.2018.
 */
interface LinkSupervisor {

    /**
     * Checks if the link passes a criteria. If verification passes,
     * it will forward the link unaltered, if is not, it will return an other predefined link by the supervisor
     *
     * For example, for the auth criteria, if the user is not authenticated it will return the link to log in screen
     */
    fun verify(link: Link): Link = link

    fun verifyLink(): ObservableTransformer<Link, Link> = ObservableTransformer { it.map { verify(it) } }

}