package fcycling.crskdev.com.domain.screen

import io.reactivex.ObservableTransformer

interface LinkRefreshHandler {

    fun handleRefresh(): ObservableTransformer<Link, Link> = ObservableTransformer { it.map { it } }

}

/**
 * Handles the following cases of refreshing a link:
 *
 * + if current link is the [Link.REFRESH] flag and the last link before calling the flag
 * is refreshable, then will emit the last link
 * + if current link is the [Link.REFRESH] flag and the last link before calling the flag
 * is  not refreshable, then will emit nothing
 *
 * Note: no matter how many consecutively times [Link.REFRESH] flag is called, it will
 * emit the same previous non-flag refreshable link befor calling [Link.REFRESH].
 *
 * Created by Cristian Pela on 02.05.2018
 */
class LinkRefreshHandlerImpl : LinkRefreshHandler {

    override fun handleRefresh(): ObservableTransformer<Link, Link> =
            ObservableTransformer {
                it.scan(Pair<Link?, Link?>(null, null)) { acc, curr ->
                    val lastLink = if (curr != Link.REFRESH) curr else acc.first
                    val emitLink =
                            if (curr != Link.REFRESH) {
                                curr
                            } else if (curr == Link.REFRESH && acc.first?.isRefreshable == true) {
                                acc.first
                            } else {
                                null
                            }
                    lastLink to emitLink
                }.skip(1).filter { it.second != null }.map { it.second }
            }
}