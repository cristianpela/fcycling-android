package fcycling.crskdev.com.domain.parse

/**
 * Created by Cristian Pela on 29.04.2018.
 */
interface ResponseParser<out M : Model> {

    fun parse(rawData: String, otherModel: Model? = null): M

}