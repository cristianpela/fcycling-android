package fcycling.crskdev.com.domain.parse

import fcycling.crskdev.com.domain.connection.Request
import java.util.*
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible


/**
 * Created by Cristian Pela on 12.05.2018.
 */
val QP_PLACEHOLDER = UUID.randomUUID().toString()

fun <M : Model> Request.interpretRedirectParamsPlaceholders(model: M): Request {
    val queryParams = queryParams
            .map {
                if (it.value == QP_PLACEHOLDER) {
                    it.key to interpretParamsPlaceholders(it.key, model)
                } else
                    it.key to it.value
            }
            .toTypedArray()

    return copy(queryParams = mapOf(*queryParams))
}

private fun interpretParamsPlaceholders(key: String, model: Model): String {
    for (member in model.javaClass.kotlin.memberProperties) {
        member.isAccessible = true
        val redirectAnnotation = member.findAnnotation<RedirectQueryParam>()
        if (redirectAnnotation != null) {
            if (redirectAnnotation.param.equals(key, true)) {
                return member.get(model).toString()
            }
        } else {
            val name = member.name
            if (name.equals(key, true)) {
                return member.get(model).toString()
            }
        }
        member.isAccessible = false
    }
    throw Error("Request has query params with \"placeholders\" but the model doesn't have " +
            " properties with RedirectQueryParam annotation which has param name matching \"$key\" " +
            "or having property names matching \"$key\"")
}

@Target(AnnotationTarget.PROPERTY)
annotation class RedirectQueryParam(val param: String)