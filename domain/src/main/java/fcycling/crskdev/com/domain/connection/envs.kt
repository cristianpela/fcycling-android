package fcycling.crskdev.com.domain.connection

/**
 * Created by Cristian Pela on 28.04.2018.
 */
data class Request(
        val endPoint: String,
        val queryParams: Map<String, String> = emptyMap(),
        val formParams: Map<String, String> = emptyMap(),
        val skipForNow: Boolean = false,
        val cacheable: Boolean = true) {

    init {
        assert(!endPoint.contains("?")) {
            "Adding query parameters to endpoint ${endPoint.split('?')[0]} is forbidden. Use queryParams property!"
        }
    }

    override fun toString(): String = endPoint + (queryParams
            .takeIf { it.isNotEmpty() }
            ?.map { "${it.key}=${it.value}" }
            ?.joinToString("&")
            ?.let { "?$it" }
            ?: "")
}

interface Response

data class RawResponse(val mapHeader: Map<String, String>, val rawData: String) : Response
data class ErrorResponse(val error: Throwable) : Response
data class LoggedOutErrorResponse(val error: Throwable): Response