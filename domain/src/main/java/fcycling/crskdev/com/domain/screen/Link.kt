package fcycling.crskdev.com.domain.screen

import fcycling.crskdev.com.domain.connection.Request

/**
 * Created by Cristian Pela on 29.04.2018.
 */
data class Link(val routeBehavior: RouteBehavior,
                val request: Request? = null,
                val isRefreshable: Boolean = false,
                val redirectLink: Link? = null,
                val tag: String? = null,
                val containerTag: String? = null,
                val screenTag: String? = null) {
    companion object {
        val REFRESH = Link(RouteBehavior(Screen.NONE, Container.NONE), isRefreshable = true, screenTag = null)
        val CANCEL = Link(RouteBehavior(Screen.NONE, Container.NONE), screenTag = null)
        val INIT = Link(RouteBehavior(Screen.NONE, Container.NONE), screenTag = "INIT")
        fun bare(screen: Screen, container: Container) = Link(RouteBehavior(screen, container), screenTag = null)
    }

    fun tag(): String = tag ?: toString()

    fun containerTag(): String = containerTag ?: routeBehavior.container.toString()

    fun screenTag(): String = screenTag ?: routeBehavior.screen.toString()

    fun skipRequest(skip: Boolean = true): Link = this.copy(request = this.request?.copy(skipForNow = skip))

    fun makePop(has: Boolean): Link = copy(routeBehavior = routeBehavior.copy(hasPop = has))
}