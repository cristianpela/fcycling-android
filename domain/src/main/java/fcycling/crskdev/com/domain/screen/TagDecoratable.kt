package fcycling.crskdev.com.domain.screen

/**
 * Created by Cristian Pela on 13.06.2018.
 */
interface TagDecoratable {
    companion object {
        internal const val EXTRAS_DELIM = "#"
        internal const val COMPOSITE_DELIM = ":"

        fun compose(vararg tagDecors: TagDecor): TagDecor =
            tagDecors.joinToString(COMPOSITE_DELIM)

        fun extract(tagDecor: TagDecor): List<TagDecor> = tagDecor.split(COMPOSITE_DELIM)
    }

    fun decorate(vararg extras: Any): TagDecor =
            (listOf(toString()) + extras).joinToString(EXTRAS_DELIM)
}

fun TagDecoratable.decorateSingle(extra: Any): TagDecor =
        this.decorate(extra)

typealias TagDecor = String
