package fcycling.crskdev.com.domain.schedulers

import io.reactivex.Scheduler

/**
 * Created by Cristian Pela on 29.04.2018.
 */
interface SchedulersContract {

    fun trampoline(): Scheduler

    fun io(): Scheduler

    fun ui(): Scheduler

    fun computation(): Scheduler

    fun single(): Scheduler

}