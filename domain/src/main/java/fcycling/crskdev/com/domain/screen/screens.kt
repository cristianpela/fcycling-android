package fcycling.crskdev.com.domain.screen

import fcycling.crskdev.com.domain.connection.Request
import fcycling.crskdev.com.domain.parse.QP_PLACEHOLDER
import fcycling.crskdev.com.domain.screen.Container.*
import fcycling.crskdev.com.domain.screen.Screen.*
import java.util.*

/**
 * Created by Cristian Pela on 30.04.2018.
 */
enum class Screen : TagDecoratable {
    NONE,
    LOGIN_SCREEN_ID,

    MENU_SCREEN_ID,
    MENU_LOGGED_PROFILE_SCREEN_ID,

    SEASON_STANDINGS_SCREEN_ID,

    MESSAGES_SCREEN_ID,

    FULL_PROFILE_SCREEN_ID,
    PROFILE_INFO_STATS_SCREEN_ID,
    PROFILE_INFO_STATS_PRIZES_SCREEN_ID,
    PROFILE_INFO_STATS_MANAGER_RACES_SCREEN_ID,
    PROFILE_INFO_STATS_VICTORIES,
    PROFILE_INFO_LINKS_SCREEN_ID,

    RIDER_SCREEN_ID,

    RACE_MANAGERS_REGISTRATION_SCREEN_ID,
    RACE_MANAGER_RESULTS_SCREEN_ID,
    RACE_MANAGER_SELECTION_TEAM_SCREEN_ID,
    RACE_MANAGER_SELECTION_TEAM_AVAILABLE_RIDER_TEAMS_SCREEN_ID,
    RACE_MANAGERS_ONGOING_UPCOMING_SCREEN_ID,

    RACE_MAIN_SCREEN_ID,
    RACE_START_LIST_SCREEN_ID,
    RACE_RESULTS_SCREEN_ID,
    RACE_STAGES_SCREEN_ID,
    RACE_STAGE_PROFILES_SCREEN_ID,

}

enum class Container : TagDecoratable {
    NONE,
    ROOT_CONTAINER_ID,
    MENU_LOGGED_PROFILE_CONTAINER_ID,
    MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID,
    PROFILE_INFO_LINKS_CONTAINER_ID,
    PROFILE_INFO_STATS_CONTAINER_ID,
    RACE_MANAGERS_SELECTION_TEAM_CONTAINER_ID
}


//NETWORK
const val URL_DOMAIN = "http://firstcycling.com"

//const val ROOT_CONTAINER_ID = 100
//const val MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID = 200
//const val MENU_LOGGED_PROFILE_CONTAINER_ID = 300

val MENU_HEADER_LOGGED_PROFILE_LINK = Link(RouteBehavior(MENU_LOGGED_PROFILE_SCREEN_ID,
        MENU_LOGGED_PROFILE_CONTAINER_ID, true, hasPop = false))

val MENU_LINK = Link(RouteBehavior(MENU_SCREEN_ID, ROOT_CONTAINER_ID, hasPop = false))

//const val FULL_PROFILE_SCREEN_ID: ScreenId = 2
val PROFILE_LINK = Link(
        RouteBehavior(FULL_PROFILE_SCREEN_ID,
                MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID,
                showScreenAsChild = true),
        Request("$URL_DOMAIN/profil.php", queryParams = mapOf("ID" to QP_PLACEHOLDER)))


//LOGIN-LOGOUT
//const val LOGIN_SCREEN_ID: ScreenId = 1

const val LOGIN_FORM_USER = "bnamn"
const val LOGIN_FORM_PASSWORD = "passw"

const val LOGIN_URL = "$URL_DOMAIN/loggpaa.php"

val LOGGED_IN_LINK_SCREEN_REDIRECT = Link(RouteBehavior(LOGIN_SCREEN_ID, ROOT_CONTAINER_ID,
        hasPop = false),
        Request("$URL_DOMAIN/fora.php")
        , redirectLink = PROFILE_LINK)

val LOGIN_LINK_SCREEN_WITH_NO_REQUEST = Link(RouteBehavior(LOGIN_SCREEN_ID, ROOT_CONTAINER_ID,
        hasPop = false), null)

val LOGOUT_LINK_SCREEN = Link(RouteBehavior(LOGIN_SCREEN_ID, ROOT_CONTAINER_ID),
        Request("$URL_DOMAIN/index.php", mapOf("logout" to "yes"), cacheable = false))


fun loginLinkSubmit(user: String, password: String): Link =
        Link(RouteBehavior(LOGIN_SCREEN_ID, ROOT_CONTAINER_ID, hasPop = false),
                Request(LOGIN_URL, formParams = mapOf(
                        LOGIN_FORM_USER to user,
                        LOGIN_FORM_PASSWORD to password,
                        "loggpaa" to ""
                ), cacheable = false))

fun profileLink(userId: Int) = Link(
        RouteBehavior(FULL_PROFILE_SCREEN_ID,
                MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID,
                showScreenAsChild = true,
                hasPop = true),
        Request("$URL_DOMAIN/profil.php", queryParams = mapOf("ID" to userId.toString())),
        screenTag = FULL_PROFILE_SCREEN_ID.decorateSingle(userId))

/**
 * this is the main screen behind auth wall
 *
 * this screen is basically parsing the authenticated user's profile page
 * From here we're getting the profile picture and username of the authenticated user,
 * then model will add links to
 *
 * user
 * profile,
 * managers,
 * logout,
 */
fun linkRiderScreen(riderId: Int, vararg otherArgs: Pair<String, String>): Link {
    val map: Map<String, String> = mutableMapOf("r" to riderId.toString()).apply {
        putAll(otherArgs)
    }
    return Link(RouteBehavior(RIDER_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, true, true),
            Request("$URL_DOMAIN/rider.php", queryParams = map))
}

fun linkRiderScreen(request: Request): Link {
    return Link(RouteBehavior(RIDER_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, true, true),
            request)
}


//#########################################RACE#####################################################
private fun linkRaceBaseScreenLink(screen: Screen, container: Container, raceId: Int, year: Int,
                                   vararg otherArgs: Pair<String, String>): Link =
        Link(RouteBehavior(screen, container, true, true),
                Request("$URL_DOMAIN/race.php",
                        mutableMapOf(
                                "r" to raceId.toString(),
                                "y" to year.toString()
                        ).apply {
                            putAll(otherArgs)
                        }))


fun linkRaceResultsScreenLink(raceId: Int, year: Int): Link =
        linkRaceBaseScreenLink(RACE_RESULTS_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, raceId, year)

fun linkRaceResultsScreenLink(request: Request) =
        Link(RouteBehavior(RACE_RESULTS_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, true, true),
                request)


fun linkRaceStartListScreen(raceId: Int, year: Int): Link =
        linkRaceBaseScreenLink(RACE_START_LIST_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, raceId, year,
                "k" to "start")


fun linkRaceStartListScreen(request: Request): Link =
        Link(RouteBehavior(RACE_START_LIST_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, true, true),
                request)

fun linkRaceStagesScreenLink(raceId: Int, year: Int): Link =
        linkRaceBaseScreenLink(RACE_STAGES_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, raceId, year,
                "k" to "etapper")

fun linkRaceStagesScreenLink(request: Request): Link =
        Link(RouteBehavior(RACE_STAGES_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, true, true),
                request)

fun linkRaceStageProfilesScreenLink(raceId: Int, year: Int): Link =
        linkRaceBaseScreenLink(RACE_STAGE_PROFILES_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, raceId, year,
                "k" to "etapper", "e" to "all")

fun linkRaceStageProfilesScreenLink(request: Request): Link =
        Link(RouteBehavior(RACE_STAGE_PROFILES_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, true, true),
                request)

//######################################RACE MANAGERS SCREENS#######################################
fun linkRaceManagerSelectionTeamScreen(managerCompId: Int, managerId: Int): Link =
        Link(RouteBehavior(RACE_MANAGER_SELECTION_TEAM_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, true, true),
                Request("$URL_DOMAIN/manres.php",
                        mapOf(
                                "konk" to managerCompId.toString(),
                                "lag" to managerId.toString()
                        )))

fun linkRaceManagerSelectionTeamScreen(request: Request): Link =
        Link(RouteBehavior(RACE_MANAGER_SELECTION_TEAM_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, true, true),
                request)

fun linkRaceManagerScreen(managerCompId: Int): Link =
        linkRaceManagerScreen(
                Request("$URL_DOMAIN/manres.php", mapOf("konk" to managerCompId.toString())))

fun linkRaceManagerScreen(request: Request): Link =
        Link(RouteBehavior(RACE_MANAGER_RESULTS_SCREEN_ID,
                MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, true, true),
                request)

fun linkRaceManagersOngoingUpcomingScreen(year: Int = Calendar.getInstance().get(Calendar.YEAR)) =
        Link(RouteBehavior(RACE_MANAGERS_ONGOING_UPCOMING_SCREEN_ID,
                MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, showScreenAsChild = true,
                hasPop = false),
                Request("$URL_DOMAIN/manager.php", queryParams = mapOf(
                        "aar" to year.toString(),
                        "k" to "komplett")))

private fun baseLinkRaceManagersTeamSelection(managerCompId: Int, formParams: Map<String, String> = emptyMap(), endPoint: String = "mandadm.php"): Link =
        Link(RouteBehavior(RACE_MANAGER_SELECTION_TEAM_SCREEN_ID,
                MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, hasPop = false, showScreenAsChild = true),
                screenTag = RACE_MANAGER_SELECTION_TEAM_SCREEN_ID.decorate(managerCompId),
                request = Request("$URL_DOMAIN/$endPoint",
                        queryParams = mapOf("konk" to managerCompId.toString()),
                        formParams = formParams))

//when registration is success will redirect to manager selection team screen
fun linkRaceManagersRegistrationPostFormToTeamSelectionScreen(managerCompId: Int): Link =
        baseLinkRaceManagersTeamSelection(managerCompId, mapOf("meldpaa" to ""))

fun linkRaceManagersTeamSelectionScreen(managerCompId: Int): Link =
        baseLinkRaceManagersTeamSelection(managerCompId)

//use the old team selection page arbitrarily pick a rider
fun linkRaceManagersTeamSelectionPickPostForm(managerCompId: Int, riderId: Int, position: Int): Link =
        baseLinkRaceManagersTeamSelection(managerCompId,
                mapOf("rytter" to riderId.toString(), "rPos" to position.toString()),
                "manadmOld.php")

fun linkRaceManagersTeamSelectionRemovePostForm(managerCompId: Int, riderId: Int): Link =
        baseLinkRaceManagersTeamSelection(managerCompId,
                mapOf("id" to riderId.toString(), "rem" to ""))

fun linkRaceManagersAvailableRidingTeamsScreen(managerCompId: Int): Link {
    return Link(RouteBehavior(RACE_MANAGER_SELECTION_TEAM_AVAILABLE_RIDER_TEAMS_SCREEN_ID,
            RACE_MANAGERS_SELECTION_TEAM_CONTAINER_ID, hasPop = false, showScreenAsChild = true),
            screenTag = RACE_MANAGER_SELECTION_TEAM_AVAILABLE_RIDER_TEAMS_SCREEN_ID.decorate(managerCompId),
            containerTag = TagDecoratable.compose(RACE_MANAGER_SELECTION_TEAM_AVAILABLE_RIDER_TEAMS_SCREEN_ID.decorate(managerCompId),
                    RACE_MANAGERS_SELECTION_TEAM_CONTAINER_ID.decorate()),
            request = Request("$URL_DOMAIN/manadmOld",
                    queryParams = mapOf("konk" to managerCompId.toString())))
}

//######################################PROFILE SCREENS#############################################
fun linkProfileInfoLinksScreen(userId: Int, year: Int, isWomenLink: Boolean): Link {
    val queryParams = linkProfileInfoStatsScreen(userId, year, isWomenLink)
            .request
            ?.queryParams
            ?.toMutableMap()
            ?.apply { remove("k") }
            ?: emptyMap<String, String>()
    return Link(RouteBehavior(PROFILE_INFO_LINKS_SCREEN_ID, PROFILE_INFO_LINKS_CONTAINER_ID, true),
            Request("$URL_DOMAIN/profil.php", queryParams), tag = PROFILE_INFO_LINKS_SCREEN_ID.toString())
}

fun linkProfileInfoLinksScreen(request: Request): Link {
    return Link(RouteBehavior(PROFILE_INFO_LINKS_SCREEN_ID, PROFILE_INFO_LINKS_CONTAINER_ID, true),
            request, tag = PROFILE_INFO_LINKS_SCREEN_ID.toString())
}

internal fun baseLinkProfileInfoStatsScreen(screen: Screen, containerId: Container, hasPop: Boolean,
                                            request: Request,
                                            containerTag: String? = null,
                                            screenTag: String? = null): Link {
    return Link(RouteBehavior(screen, containerId, true, hasPop),
            request, isRefreshable = true, containerTag = containerTag, screenTag = screenTag)
}

internal fun baseLinkProfileInfoStatsScreen(screen: Screen, containerId: Container, userId: Int,
                                            year: Int, isWomenLink: Boolean, hasPop: Boolean,
                                            screenTag: String? = null,
                                            containerTag: String? = null): Link {
    val queryParams = mutableMapOf("ID" to userId.toString())
            .apply {
                if (!isWomenLink) put("k", "")
                put("aar", year.toString())
                if (isWomenLink) put("kj", "F")
            }
    return Link(RouteBehavior(screen, containerId, true, hasPop),
            Request("$URL_DOMAIN/profil.php", queryParams),
            isRefreshable = true,
            screenTag = screenTag,
            containerTag = containerTag)
}

fun linkProfileInfoStatsScreen(request: Request): Link {
    val id = request.queryParams["ID"]!!.toInt()
    val year = request.queryParams["aar"].toString()
    val fullProfileScreenTag = FULL_PROFILE_SCREEN_ID.decorateSingle(id)
    val containerTag = PROFILE_INFO_STATS_CONTAINER_ID.decorate(id, year)
    return baseLinkProfileInfoStatsScreen(PROFILE_INFO_STATS_SCREEN_ID, Container.PROFILE_INFO_STATS_CONTAINER_ID, false, request,
            containerTag = TagDecoratable.compose(fullProfileScreenTag, containerTag))
}

fun linkProfileInfoStatsScreen(userId: Int, year: Int, isWomenLink: Boolean): Link {
    val fullProfileScreenTag = FULL_PROFILE_SCREEN_ID.decorateSingle(userId)
    val containerTag = PROFILE_INFO_STATS_CONTAINER_ID.decorate(userId, year)
    return baseLinkProfileInfoStatsScreen(PROFILE_INFO_STATS_SCREEN_ID, Container.PROFILE_INFO_STATS_CONTAINER_ID, userId, year, isWomenLink,
            false, containerTag = TagDecoratable.compose(fullProfileScreenTag, containerTag))
}

fun linkProfileInfoStatsManagerRacesScreen(request: Request): Link {
    val id = request.queryParams["ID"]!!.toInt()
    val year = request.queryParams["aar"].toString()
    val isWomenLink = request.queryParams["kj"] != null
    val screenTag = PROFILE_INFO_STATS_MANAGER_RACES_SCREEN_ID.decorate(id, year, isWomenLink)
    return baseLinkProfileInfoStatsScreen(PROFILE_INFO_STATS_MANAGER_RACES_SCREEN_ID, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, true, request,
            screenTag = screenTag)
}

fun linkProfileInfoStatsManagerRacesScreen(userId: Int, year: Int, isWomenLink: Boolean): Link {
    val screenTag = PROFILE_INFO_STATS_MANAGER_RACES_SCREEN_ID.decorate(userId, year, isWomenLink)
    return baseLinkProfileInfoStatsScreen(PROFILE_INFO_STATS_MANAGER_RACES_SCREEN_ID,
            MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID, userId, year, isWomenLink, true, screenTag = screenTag)
}

fun linkProfileInfoStatsVictories(userId: Int): Link {
    return Link(RouteBehavior(PROFILE_INFO_STATS_VICTORIES, MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID,
            true,
            true),
            profileLink(userId).request,
            screenTag = PROFILE_INFO_STATS_VICTORIES.decorateSingle(userId))
}
