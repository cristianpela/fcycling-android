package fcycling.crskdev.com.domain.screen

import fcycling.crskdev.com.domain.parse.Model

/**
 * Created by Cristian Pela on 29.04.2018.
 */
sealed class ScreenModel {
    data class DataModel(val originalLink: Link, val model: Model? = null) : ScreenModel()
    data class CompositeDataModel(val models: List<DataModel>): ScreenModel()
    data class ErrorModel(val error: Throwable) : ScreenModel()
    object Wait : ScreenModel()
    object Cancel: ScreenModel()
    object Init: ScreenModel()
    object Idle: ScreenModel()
}