package fcycling.crskdev.com.domain.parse

import fcycling.crskdev.com.domain.screen.Link

/**
 * Created by Cristian Pela on 29.04.2018.
 */
interface Model

object EmptyModel : Model

interface ModelShard<out M : Model> {
    val link: Link?
    val model: M?
}

data class ErrorModel(val error: Throwable) : Model

data class CompositeModel(val shards: List<ModelShard<*>>) : Model


fun <M : Model> M.toShard(link: Link?): ModelShard<M> = object : ModelShard<M> {

    override val link: Link? = link

    @Suppress("UNCHECKED_CAST")
    override val model: M? = this@toShard

}

