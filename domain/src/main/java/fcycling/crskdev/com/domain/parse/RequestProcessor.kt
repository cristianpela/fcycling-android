package fcycling.crskdev.com.domain.parse

import fcycling.crskdev.com.domain.connection.Connection
import fcycling.crskdev.com.domain.connection.ErrorResponse
import fcycling.crskdev.com.domain.connection.LoggedOutErrorResponse
import fcycling.crskdev.com.domain.connection.RawResponse
import fcycling.crskdev.com.domain.schedulers.SchedulersContract
import fcycling.crskdev.com.domain.screen.*
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Single
import io.reactivex.SingleEmitter
import timber.log.Timber

interface IRequestProcessor {
    fun process(): ObservableTransformer<Link, ScreenModel>
}

/**
 * Created by Cristian Pela on 29.04.2018.
 */
class RequestProcessor(
        private val connection: Connection,
        private val parsers: ParserFactory,
        private val refreshHandler: LinkRefreshHandler,
        private val linkSupervisor: LinkSupervisor,
        private val contract: SchedulersContract) : IRequestProcessor {

    init {
        Timber.tag("Request Processor")
    }

    override fun process(): ObservableTransformer<Link, ScreenModel> =
            ObservableTransformer {
                it.compose(refreshHandler.handleRefresh())
                        .compose(linkSupervisor.verifyLink())
                        .flatMap {
                            if (it == Link.CANCEL || it == Link.INIT) {
                                Observable.just(if (it == Link.CANCEL) ScreenModel.Cancel else
                                    ScreenModel.Init).doOnNext {
                                    connection.cancel()
                                }
                            } else if (it.request?.skipForNow == true) {
                                Observable.just(ScreenModel.DataModel(it.skipRequest(false), null))
                            } else {
                                processAsSingle(it)
                                        .doOnError { Timber.e(it) }
                                        .onErrorReturn { ScreenModel.ErrorModel(it) }
                                        .toObservable()
                                        .subscribeOn(contract.io())
                                        .observeOn(contract.ui())
                                        .startWith(ScreenModel.Wait)
                            }
                        }
            }


    private fun processAsSingle(link: Link): Single<ScreenModel> =
            Single.create<ScreenModel> {
                it.setCancellable { connection.cancel() }
                processRequest(link, it)
            }

    private fun processRequest(link: Link, e: SingleEmitter<ScreenModel>) {
        link.request?.takeIf { !it.skipForNow }
                ?.let {
                    val screenId = link.routeBehavior.screen
                    val requestTag = screenId.toString()
                    val response = try {
                        connection.request(it, requestTag)
                    } catch (ex: Error) {
                        ex.printStackTrace()
                        e.onError(ex)
                    }
                    when (response) {
                        is RawResponse -> {
                            val parser = lookUpParser(screenId)
                            val model = parser.parse(response.rawData)
                            if (model is ErrorModel) {
                                e.onSuccess(ScreenModel.ErrorModel(model.error))
                            } else {
                                //deal with redirect
                                if (link.redirectLink != null) {
                                    val interpretedRequest = link.redirectLink.request?.interpretRedirectParamsPlaceholders(model)
                                    val interpretedRedirectLink = link.redirectLink.copy(request = interpretedRequest)
                                    processRequest(interpretedRedirectLink, e)
                                } else {
                                    e.onSuccess(createScreenDataModel(link, model))
                                }
                            }
                        }
                        is LoggedOutErrorResponse -> e.onSuccess(ScreenModel.DataModel(LOGIN_LINK_SCREEN_WITH_NO_REQUEST))
                        is ErrorResponse -> e.onSuccess(ScreenModel.ErrorModel(response.error))
                        else -> throw UnsupportedOperationException("response type ${response::class} not supported")
                    }

                }
                ?: e.onSuccess(createScreenDataModel(link, null))
    }

    private fun lookUpParser(screen: Screen) = parsers.get(screen)


    private fun createScreenDataModel(link: Link, model: Model?): ScreenModel {
        return when (model) {
            is CompositeModel -> {
                val screens = mutableListOf<ScreenModel.DataModel>()
                model.shards.forEach {
                    screens.add(ScreenModel.DataModel(it.link ?: link, it.model))
                }
                //make sure to put the principal screens first, if the're any
                ScreenModel.CompositeDataModel(screens.apply { sortBy { it.originalLink.routeBehavior.showScreenAsChild } })
            }
            else -> ScreenModel.DataModel(link, model)
        }
    }

}