package fcycling.crskdev.com.domain.parse

import fcycling.crskdev.com.domain.screen.Screen

/**
 * Created by Cristian Pela on 08.05.2018.
 */
class ParserFactory(private val parsers: Map<Screen, ResponseParser<*>>) {

    fun get(screen: Screen): ResponseParser<*> = parsers[screen]
            ?: throw Error("Could not find parser for screen id: $screen")
}