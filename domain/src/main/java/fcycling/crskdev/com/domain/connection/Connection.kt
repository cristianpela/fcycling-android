package fcycling.crskdev.com.domain.connection

import java.io.IOException

/**
 * Created by Cristian Pela on 28.04.2018.
 */
interface Connection {

    @Throws(IOException::class)
    fun request(request: Request, tag: String? = null): Response

    fun cancel(tag: String? = null)

}