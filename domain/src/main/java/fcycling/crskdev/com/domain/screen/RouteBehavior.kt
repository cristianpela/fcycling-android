package fcycling.crskdev.com.domain.screen

/**
 * Created by Cristian Pela on 29.04.2018.
 */
data class RouteBehavior(val screen: Screen,
                         val container: Container,
                         val showScreenAsChild: Boolean = false,
                         val hasPop: Boolean = true)