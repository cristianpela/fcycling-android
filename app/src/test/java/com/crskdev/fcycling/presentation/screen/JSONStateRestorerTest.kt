package com.crskdev.fcycling.presentation.screen

import fcycling.crskdev.com.data.parse.login.LoginModel
import fcycling.crskdev.com.data.parse.profile.CompetitionGender
import fcycling.crskdev.com.data.parse.profile.ManagerRace
import fcycling.crskdev.com.data.parse.profile.ManagerRacesModel
import fcycling.crskdev.com.data.parse.profile.MinimalProfile
import fcycling.crskdev.com.data.parse.race.MinimalResultRaceInfo
import fcycling.crskdev.com.data.parse.race.RaceAbout
import fcycling.crskdev.com.data.parse.race.RaceLink
import fcycling.crskdev.com.data.parse.rider.MinimalInfoRider
import fcycling.crskdev.com.domain.screen.*
import org.junit.Test
import kotlin.test.assertEquals

/**
 * Created by Cristian Pela on 28.05.2018.
 */
class JSONStateRestorerTest {

    private val jsonStateRestorer = JSONStateRestorer(MockStateIO())

    @Test
    fun saveState() {
        val model = LoginModel(2768, "criskey", profileLink(2768))
        jsonStateRestorer.saveState(LOGIN_LINK_SCREEN_WITH_NO_REQUEST, model)
        assertEquals(model, jsonStateRestorer.loadState(LOGIN_LINK_SCREEN_WITH_NO_REQUEST, LoginModel::class))
    }

    @Test
    fun saveStateForModelWithList() {
        val noLink = Link.bare(Screen.NONE, Container.NONE)
        val model = ManagerRacesModel(
                MinimalProfile(
                        2768,
                        "criskey",
                        "CRSK",
                        null,
                        null),
                2018, CompetitionGender.MEN,
                listOf(ManagerRace(
                        MinimalResultRaceInfo(
                                RaceLink(null, RaceAbout("Giro d'Italia", "23.05-27.05", "")),
                                "GT.2",
                                null)
                        , 21, 730, 150, 40, noLink, noLink,
                        MinimalInfoRider(123, "T.Dumolin", "", noLink), true),
                        ManagerRace(
                                MinimalResultRaceInfo(
                                        RaceLink(null, RaceAbout("Ronde de l'Isard (U23)", "23.05-27.05", "")),
                                        "2.HC",
                                        null),
                                10, 198, 101, -1, noLink, noLink,
                                MinimalInfoRider(123, "J.Maas", "", null), true)))
        jsonStateRestorer.saveState(LOGIN_LINK_SCREEN_WITH_NO_REQUEST, model)
        assertEquals(model, jsonStateRestorer.loadState(LOGIN_LINK_SCREEN_WITH_NO_REQUEST, ManagerRacesModel::class))
    }
}

class MockStateIO : StateIO {

    private val fileSystem = mutableMapOf<String, String>()

    override fun delete(link: Link): Boolean = true

    override fun write(link: Link, stringState: String) {
        val fileName = fileNameGenerator(link)
        fileSystem[fileName] = stringState
    }

    override fun read(link: Link): String =
            fileSystem[fileNameGenerator(link)] ?: throw Error("File not found for link $link")

}