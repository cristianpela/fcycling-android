package com.crskdev.fcycling.presentation.screen.profile.victories

import fcycling.crskdev.com.data.parse.profile.ProfileStatsVictories
import fcycling.crskdev.com.data.parse.race.RaceAbout
import fcycling.crskdev.com.data.parse.race.RaceLink
import fcycling.crskdev.com.domain.screen.URL_DOMAIN
import fcycling.crskdev.com.domain.screen.linkRaceManagerScreen
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Cristian Pela on 01.06.2018.
 */
class VictoriesTransformerTest {

    @Test
    fun transform() {
        val items = VictoriesTransformer.transform(
                listOf(
                        ProfileStatsVictories.Victory(RaceLink(linkRaceManagerScreen(527),
                                RaceAbout("Cyclassics Hamburg",
                                "2016",
                                "$URL_DOMAIN/img/nat/GER.png"))),
                        ProfileStatsVictories.Victory(RaceLink(linkRaceManagerScreen(669),
                                RaceAbout("GP Kanton Aargau",
                                "2017",
                                "$URL_DOMAIN/img/nat/SUI.png"))),
                        ProfileStatsVictories.Victory(RaceLink(linkRaceManagerScreen(638),
                                RaceAbout("World Championship",
                                "2017",
                                "$URL_DOMAIN/img/nat/UCI.png")))),
                        listOf(
                                ProfileStatsVictories.Victory(
                                        RaceLink(null, RaceAbout("Ronde de l'Isard (U23)", null, "$URL_DOMAIN/img/nat/FRA.png")),
                                        "1x"),
                                ProfileStatsVictories.Victory(
                                        RaceLink(null, RaceAbout("Ronde de l'Isard (U23)", null, "$URL_DOMAIN/img/nat/FRA.png")),
                                        "2x"),
                                ProfileStatsVictories.Victory(
                                        RaceLink(null, RaceAbout("Ronde de l'Isard (U23)", null, "$URL_DOMAIN/img/nat/FRA.png")),
                                        "2x")
                        ),
                "Races", "Stages"
        )
        assertEquals(
                listOf(
                        Item.Header("Races"),
                        Item.Race("Cyclassics Hamburg", "$URL_DOMAIN/img/nat/GER.png", "2016"),
                        Item.Race("GP Kanton Aargau", "$URL_DOMAIN/img/nat/SUI.png", "2017"),
                        Item.Race("World Championship", "$URL_DOMAIN/img/nat/UCI.png", "2017"),
                        Item.Header("Stages"),
                        Item.StageTimes("1x", 1),
                        Item.Stage("Ronde de l'Isard (U23)", "$URL_DOMAIN/img/nat/FRA.png"),
                        Item.StageTimes("2x", 2),
                        Item.Stage("Ronde de l'Isard (U23)", "$URL_DOMAIN/img/nat/FRA.png"),
                        Item.Stage("Ronde de l'Isard (U23)", "$URL_DOMAIN/img/nat/FRA.png")
                ), items)
    }
}