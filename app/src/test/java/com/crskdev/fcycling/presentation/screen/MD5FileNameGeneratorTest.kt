package com.crskdev.fcycling.presentation.screen

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Cristian Pela on 28.05.2018.
 */
class MD5FileNameGeneratorTest {

    @Test
    fun generate() {
        assertEquals(MD5FileNameGenerator.generate("Hello"),"8B1A9953C4611296A827ABF8C47804D7")
    }
}