package com.crskdev.fcycling.presentation.screen

import android.support.test.InstrumentationRegistry
import android.support.test.filters.SmallTest
import fcycling.crskdev.com.domain.screen.LOGIN_LINK_SCREEN_WITH_NO_REQUEST
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Created by Cristian Pela on 28.05.2018.
 */
@SmallTest
class AndroidStateIOTest {

    private val context = InstrumentationRegistry.getTargetContext()

    @Test
    fun write() {
        val stateIO = AndroidStateIO(context)
        val content = "Hello"
        val link = LOGIN_LINK_SCREEN_WITH_NO_REQUEST
        stateIO.write(link, content)
        assertEquals(content, stateIO.read(link))
        assertTrue(stateIO.delete(link))
    }
}