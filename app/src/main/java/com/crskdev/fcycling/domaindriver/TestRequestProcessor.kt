package com.crskdev.fcycling.domaindriver

import com.crskdev.fcycling.util.kast
import fcycling.crskdev.com.data.parse.login.LoginModel
import fcycling.crskdev.com.data.parse.manager.ManagerResult
import fcycling.crskdev.com.data.parse.manager.ManagerResultsModel
import fcycling.crskdev.com.data.parse.manager.MinimalManager
import fcycling.crskdev.com.data.parse.menu.MenuModel
import fcycling.crskdev.com.data.parse.profile.*
import fcycling.crskdev.com.data.parse.profile.ProfileStatsLinks.YearLink
import fcycling.crskdev.com.data.parse.race.MinimalResultRaceInfo
import fcycling.crskdev.com.data.parse.race.RaceAbout
import fcycling.crskdev.com.data.parse.race.RaceLink
import fcycling.crskdev.com.data.parse.rider.MinimalInfoRider
import fcycling.crskdev.com.data.parse.rider.MostUsedCaptain
import fcycling.crskdev.com.domain.parse.IRequestProcessor
import fcycling.crskdev.com.domain.screen.*
import fcycling.crskdev.com.domain.screen.Container.MENU_LOGGED_PROFILE_CONTAINER_ID
import fcycling.crskdev.com.domain.screen.Container.ROOT_CONTAINER_ID
import fcycling.crskdev.com.domain.screen.Screen.*
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.*
import java.util.concurrent.TimeUnit

class TestRequestProcessor : IRequestProcessor {

    private val random = Random()

    private val noLink = Link.bare(Screen.NONE, Container.NONE)

    private val profileLink = fcycling.crskdev.com.domain.screen.profileLink(2768)

    private val minimalProfile = MinimalProfile(
            2768,
            "criskey",
            "CRSK",
            "http://firstcycling.com/img/users/t2768.jpg",
            "http://firstcycling.com/img/nat/ROM.png")

    private val screenMinimalProfileModel = ScreenModel.DataModel(
            Link(RouteBehavior(MENU_LOGGED_PROFILE_SCREEN_ID,
                    MENU_LOGGED_PROFILE_CONTAINER_ID,
                    true), screenTag = null), MinimalProfileModel(
            minimalProfile,
            random.nextInt(99) + 1,
            noLink))

    private val screenMenuModel = ScreenModel.DataModel(
            Link(RouteBehavior(
                    MENU_SCREEN_ID,
                    ROOT_CONTAINER_ID
            ), screenTag = null),
            MenuModel(
                    profileLink,
                    noLink,
                    noLink,
                    noLink,
                    MinimalManager(122, null, "Dunkerque", "(08/05) 15:30", noLink),
                    noLink))

    private val fullProfileModel = fun(year: Int, isWomen: Boolean): FullProfileModel {

        val links = if (!isWomen) ProfileStatsLinks(
                CompetitionGender.MEN,
                linkProfileInfoStatsScreen(2768, year, true),
                listOf(
                        YearLink(2018, linkProfileInfoStatsScreen(2768, 2018, false), year == 2018),
                        YearLink(2017, linkProfileInfoStatsScreen(2768, 2017, false), year == 2017)))
        else
            ProfileStatsLinks(
                    CompetitionGender.WOMEN,
                    linkProfileInfoStatsScreen(2768, year, false),
                    listOf(
                            YearLink(2018, linkProfileInfoStatsScreen(2768, 2018, true), year == 2018),
                            YearLink(2017, linkProfileInfoStatsScreen(2768, 2017, true), year == 2017)))


        return FullProfileModel(
                minimalProfile,
                FullProfileModel.Info("Timisoara",
                        "Romania",
                        "http://firstcycling.com/img/nat/ROM.png",
                        "27.04.1985",
                        listOf(
                                FullProfileModel.Info.Badge("http://firstcycling.com/img/riders/87.jpg",
                                        "Zubeldia: had Haimar Zubeldia on the team in his last race, San Sebastian 2017"),
                                FullProfileModel.Info.Badge("http://firstcycling.com/img/riders/27.jpg",
                                        "Scarponi: had Michele Scarponi on the team in his last race, Tour of the Alps 2017"),
                                FullProfileModel.Info.Badge("http://firstcycling.com/img/riders/33.jpg",
                                        "Boonen: had Tom Boonen as captain for his last race, Paris-Roubaix 2017")
                        )),
                links,
                ProfileStatsVictories(
                        listOf(
                                ProfileStatsVictories.Victory(RaceLink(linkRaceManagerScreen(527),
                                        RaceAbout("Cyclassics Hamburg",
                                                "2016",
                                                "$URL_DOMAIN/img/nat/GER.png"))),
                                ProfileStatsVictories.Victory(RaceLink(linkRaceManagerScreen(669),
                                        RaceAbout("GP Kanton Aargau",
                                                "2017",
                                                "$URL_DOMAIN/img/nat/SUI.png"))),
                                ProfileStatsVictories.Victory(RaceLink(linkRaceManagerScreen(638),
                                        RaceAbout("World Championship",
                                                "2017",
                                                "$URL_DOMAIN/img/nat/UCI.png"))),
                                ProfileStatsVictories.Victory(RaceLink(linkRaceManagerScreen(527),
                                        RaceAbout("Cyclassics Hamburg",
                                                "2016",
                                                "$URL_DOMAIN/img/nat/GER.png"))),
                                ProfileStatsVictories.Victory(RaceLink(linkRaceManagerScreen(669),
                                        RaceAbout("GP Kanton Aargau",
                                                "2017",
                                                "$URL_DOMAIN/img/nat/SUI.png"))),
                                ProfileStatsVictories.Victory(RaceLink(linkRaceManagerScreen(638),
                                        RaceAbout("World Championship",
                                                "2017",
                                                "$URL_DOMAIN/img/nat/UCI.png")))

                        ),

                        listOf(
                                ProfileStatsVictories.Victory(
                                        RaceLink(null, RaceAbout("Ronde de l'Isard (U23)", null, "$URL_DOMAIN/img/nat/FRA.png")),
                                        "1x"),
                                ProfileStatsVictories.Victory(
                                        RaceLink(null, RaceAbout("Tour of Sibiu", null, "$URL_DOMAIN/img/nat/FRA.png")),
                                        "2x"),
                                ProfileStatsVictories.Victory(
                                        RaceLink(null, RaceAbout("Tour of Sibiu", null, "$URL_DOMAIN/img/nat/FRA.png")),
                                        "2x"),
                                ProfileStatsVictories.Victory(
                                        RaceLink(null, RaceAbout("Tour of Sibiu", null, "$URL_DOMAIN/img/nat/BEL.png")),
                                        "3x"),
                                ProfileStatsVictories.Victory(
                                        RaceLink(null, RaceAbout("Tour of Sibiu", null, "$URL_DOMAIN/img/nat/BEL.png")),
                                        "3x"),
                                ProfileStatsVictories.Victory(
                                        RaceLink(null, RaceAbout("Tour of Sibiu", null, "$URL_DOMAIN/img/nat/BEL.png")),
                                        "3x"),
                                ProfileStatsVictories.Victory(
                                        RaceLink(null, RaceAbout("Tour of Sibiu", null, "$URL_DOMAIN/img/nat/BEL.png")),
                                        "3x")
                        ))
        )
    }

    private val stats = mapOf(
            CompetitionGender.MEN to mapOf(
                    2018 to ProfileStats(
                            listOf(
                                    SeasonStanding(3, "Season competition"),
                                    SeasonStanding(23, "One-day races"),
                                    SeasonStanding(3, "Stage races"),
                                    SeasonStanding(9, "Spring Classics"),
                                    SeasonStanding(2, "Form (Last 6 Weeks)")
                            ),
                            listOf(
                                    MonthlyStanding("Mai", 10, 166, "1 / 1", "100 %"),
                                    MonthlyStanding("April", 3, 2017, "14 / 14", "100 %"),
                                    MonthlyStanding("March", 16, 1502, "14 / 14", "100 %"),
                                    MonthlyStanding("February", 13, 2194, "15 / 15", "100 %"),
                                    MonthlyStanding("January", 53, 487, "4 / 4", "100 %")
                            ),
                            listOf(
                                    MostUsedCaptain(MinimalInfoRider(38, "A.Valverde", "$URL_DOMAIN/img/nat/ESP.png", linkRiderScreen(38)), 5),
                                    MostUsedCaptain(MinimalInfoRider(22268, "L.Calmejane", "$URL_DOMAIN/img/nat/FRA.png", linkRiderScreen(22268)), 3),
                                    MostUsedCaptain(MinimalInfoRider(784, "P.Sagan", "$URL_DOMAIN/img/nat/SVK.png", linkRiderScreen(784)), 3),
                                    MostUsedCaptain(MinimalInfoRider(38, "A.Valverde", "$URL_DOMAIN/img/nat/ESP.png", linkRiderScreen(38)), 5),
                                    MostUsedCaptain(MinimalInfoRider(22268, "L.Calmejane", "$URL_DOMAIN/img/nat/FRA.png", linkRiderScreen(22268)), 3),
                                    MostUsedCaptain(MinimalInfoRider(784, "P.Sagan", "$URL_DOMAIN/img/nat/SVK.png", linkRiderScreen(784)), 3),
                                    MostUsedCaptain(MinimalInfoRider(38, "A.Valverde", "$URL_DOMAIN/img/nat/ESP.png", linkRiderScreen(38)), 5),
                                    MostUsedCaptain(MinimalInfoRider(22268, "L.Calmejane", "$URL_DOMAIN/img/nat/FRA.png", linkRiderScreen(22268)), 3),
                                    MostUsedCaptain(MinimalInfoRider(784, "P.Sagan", "$URL_DOMAIN/img/nat/SVK.png", linkRiderScreen(784)), 3)
                            ),
                            listOf(
                                    ManagerRace(
                                            MinimalResultRaceInfo(
                                                    RaceLink(linkRaceManagerScreen(123), RaceAbout("Giro d'Italia", "23.05-27.05", "http://firstcycling.com/img/nat/ITA.png")),
                                                    "GT.2",
                                                    null)
                                            , 21, 730, 150, 40, noLink, noLink,
                                            MinimalInfoRider(123, "T.Dumolin", "http://firstcycling.com/img/nat/NED.png", noLink), true),
                                    ManagerRace(
                                            MinimalResultRaceInfo(
                                                    RaceLink(linkRaceManagerScreen(123), RaceAbout("Ronde de l'Isard (U23)", "23.05-27.05", "http://firstcycling.com/img/nat/FRA.png")),
                                                    "2.HC",
                                                    null),
                                            10, 198, 101, -1, noLink, noLink,
                                            MinimalInfoRider(123, "J.Maas", "http://firstcycling.com/img/nat/NED.png", null), true),
                                    ManagerRace(
                                            MinimalResultRaceInfo(
                                                    RaceLink(linkRaceManagerScreen(123), RaceAbout("Belgium Tour", "23.05-27.05", "http://firstcycling.com/img/nat/BEL.png")),
                                                    "2.HC", null),
                                            75, 98, 110, 100, noLink, noLink,
                                            MinimalInfoRider(123, "A.Greipel", "http://firstcycling.com/img/nat/GER.png", null), true),
                                    ManagerRace(
                                            MinimalResultRaceInfo(
                                                    RaceLink(linkRaceManagerScreen(123), RaceAbout("Belgium Tour", "23.05-27.05", "http://firstcycling.com/img/nat/BEL.png")),
                                                    "2.HC", null),
                                            75, 98, 110, 12, noLink, noLink,
                                            MinimalInfoRider(123, "A.Greipel", "http://firstcycling.com/img/nat/GER.png", null), false)

                            )

                    ),
                    2017 to ProfileStats(listOf(
                            SeasonStanding(17, "Season competition"),
                            SeasonStanding(8, "One-day races"),
                            SeasonStanding(22, "Stage races"),
                            SeasonStanding(72, "World Championship"),
                            SeasonStanding(22, "Spring Classics"),
                            SeasonStanding(17, "National Championships"),
                            SeasonStanding(47, "Grand Tour competition"),
                            SeasonStanding(10, "Coppa Italia")
                    ),
                            listOf(
                                    MonthlyStanding("Mai", 10, 166, "1 / 1", "100 %"),
                                    MonthlyStanding("April", 3, 2017, "14 / 14", "100 %"),
                                    MonthlyStanding("March", 16, 1502, "14 / 14", "100 %"),
                                    MonthlyStanding("February", 13, 2194, "15 / 15", "100 %"),
                                    MonthlyStanding("January", 53, 487, "4 / 4", "100 %")
                            ),
                            emptyList(),
                            emptyList()
                    )

            ),
            CompetitionGender.WOMEN to mapOf(
                    2018 to ProfileStats(emptyList(), emptyList(), emptyList(), emptyList()),
                    2017 to ProfileStats(emptyList(), emptyList(), emptyList(), emptyList())
            )
    )

    override fun process(): ObservableTransformer<Link, ScreenModel> =
            ObservableTransformer {
                it.flatMap {
                    if (it == Link.CANCEL) {
                        Observable.just(ScreenModel.Cancel)
                    } else {
                        toScreenModelObservable(it)
                                .simulateWaitTime()
                                .observeOn(AndroidSchedulers.mainThread())
                    }
                }
            }

    @Suppress("UNCHECKED_CAST")
    private fun <T : ScreenModel> Observable<T>.simulateWaitTime(delay: Long = 0L, unit: TimeUnit = TimeUnit.SECONDS) =
            if (delay == 0L)
                this
            else delay(delay, unit)
                    .startWith(ScreenModel.Wait as T)

    private fun toScreenModelObservable(it: Link): Observable<ScreenModel> {
        val screenModel = if (it.request?.skipForNow == true) {
            ScreenModel.DataModel(it, null)
        } else {
            when (it.routeBehavior.screen) {
                MENU_SCREEN_ID -> {
                    processMenuScreen()
                }
                LOGIN_SCREEN_ID -> {
                    processLoginScreen(it)
                }
                FULL_PROFILE_SCREEN_ID -> {
                    processProfileScreen(it)
                }
                PROFILE_INFO_STATS_SCREEN_ID -> {
                    processProfileStatsScreen(it)
                }
                PROFILE_INFO_STATS_MANAGER_RACES_SCREEN_ID -> {
                    processProfileStatsManagerRacesScreen(it)
                }
                RACE_MANAGER_RESULTS_SCREEN_ID -> {
                    processRaceManagerResultsScreen(it)
                }
                else -> ScreenModel.ErrorModel(Error("Mapping for requested link could not be created. Screen ${it.routeBehavior.screen}"))
            }
        }
        return Observable.just(screenModel)
    }

    private fun processRaceManagerResultsScreen(link: Link): ScreenModel {
        val headerToNotCrashApp = screenMenuModel

        val resultsModel = ScreenModel.DataModel(link, ManagerResultsModel(123,
                RaceLink(linkRaceManagerScreen(123), RaceAbout("Tour de Suisse", "2018", "http://firstcycling.com/img/nat/SUI.png")),
                listOf(
                        ManagerResult(
                                "1",
                                minimalProfile,
                                linkRaceManagerSelectionTeamScreen(1, 1),
                                "100",
                                "300",
                                "Cyclopaths",
                                "http://firstcycling.com/img/nat/FC.png",
                                MinimalInfoRider(38, "A.Valverde", "$URL_DOMAIN/img/nat/ESP.png", linkRiderScreen(38))
                        )
                )
        ))
        return ScreenModel.CompositeDataModel(listOf(headerToNotCrashApp, resultsModel))
    }

    private fun processProfileStatsManagerRacesScreen(link: Link): ScreenModel {
        val year = getYear(link)!!
        val gender = getGender(link)
        return ScreenModel.CompositeDataModel(
                listOf(screenMenuModel,
                        ScreenModel.DataModel(
                                link, ManagerRacesModel(
                                screenMinimalProfileModel
                                        .model
                                        ?.kast<MinimalProfileModel>()
                                        ?.minimalProfile!!,
                                year,
                                CompetitionGender.MEN,
                                stats[gender]!![year]!!.managerRaces.filter { !it.isOngoing }))
                ))
    }

    private fun processProfileStatsScreen(link: Link): ScreenModel {
        val year = getYear(link)!!
        val gender = this.getGender(link)
        return ScreenModel.CompositeDataModel(
                listOf(
                        screenMenuModel,
                        ScreenModel.DataModel(profileLink(2768), fullProfileModel(year, gender == CompetitionGender.WOMEN)),
                        ScreenModel.DataModel(link, ProfileStatsModel(minimalProfile,
                                year,
                                gender,
                                stats[gender]!![year])
                        )))
    }

    private fun getYear(link: Link): Int? =
            link.request?.queryParams?.get("aar")?.toInt()

    private fun getGender(link: Link) =
            if (link.request?.queryParams?.get("kj") == "F") CompetitionGender.WOMEN else CompetitionGender.MEN

    private fun processMenuScreen(): ScreenModel.CompositeDataModel {
        return ScreenModel.CompositeDataModel(listOf(
                screenMenuModel,
                screenMinimalProfileModel))
    }


    private var isLoggedIn = false
    private fun processLoginScreen(link: Link): ScreenModel {
        if (link == LOGOUT_LINK_SCREEN) {
            isLoggedIn = false
            return ScreenModel.DataModel(LOGIN_LINK_SCREEN_WITH_NO_REQUEST, null)
        }
        if (isLoggedIn) {
            return processProfileScreen(profileLink(2768))
        }
        val expectedUsername = "a"
        val expectedPassword = "a"

        val loginParams = link.request?.formParams

        return loginParams
                ?.let {
                    val username = loginParams[LOGIN_FORM_USER]
                    val password = loginParams[LOGIN_FORM_PASSWORD]
                    if (expectedUsername == username && expectedPassword == password) {
                        isLoggedIn = true
                        ScreenModel.DataModel(link, LoginModel(0, username, profileLink))
                    } else {
                        ScreenModel.ErrorModel(Error("Username or password are invalid"))
                    }
                }
                ?: ScreenModel.DataModel(link, LoginModel(0, null))
    }

    private fun processProfileScreen(link: Link): ScreenModel.CompositeDataModel {
        val loggedProfileModel = MinimalProfileModel(
                minimalProfile,
                random.nextInt(99) + 1,
                noLink)
        val menuModel = MenuModel(
                profileLink,
                noLink,
                noLink,
                noLink,
                MinimalManager(122, null, "Dunkerque", "(08/05) 15:30", noLink),
                noLink)

        val year = getYear(link) ?: 2018
        val gender = getGender(link)

        return ScreenModel.CompositeDataModel(
                listOf(
                        ScreenModel.DataModel(
                                Link(RouteBehavior(
                                        MENU_SCREEN_ID,
                                        ROOT_CONTAINER_ID
                                ), screenTag = null), menuModel),
                        ScreenModel.DataModel(
                                Link(RouteBehavior(MENU_LOGGED_PROFILE_SCREEN_ID,
                                        MENU_LOGGED_PROFILE_CONTAINER_ID,
                                        true), screenTag = null), loggedProfileModel),
                        ScreenModel.DataModel(
                                profileLink(2768), fullProfileModel(year, gender == CompetitionGender.WOMEN))
                        , ScreenModel.DataModel(
                        linkProfileInfoStatsScreen(2768, year, false),
                        ProfileStatsModel(minimalProfile, year,
                                gender, stats[gender]!![year]!!)
                )
                ))
    }
}