package com.crskdev.fcycling

import android.app.Application
import android.support.multidex.MultiDexApplication
import com.crskdev.fcycling.datadriver.AddAllPersistentCookieJar
import com.crskdev.fcycling.datadriver.AndroidInternetConnectivityCheck
import com.crskdev.fcycling.datadriver.PersistentCookieStoreValueFinder
import com.crskdev.fcycling.datadriver.createOkHttpClient
import com.crskdev.fcycling.domaindriver.TestRequestProcessor
import com.crskdev.fcycling.presentation.screen.*
import com.crskdev.fcycling.presentation.screen.login.LoginController
import com.crskdev.fcycling.presentation.screen.main.MenuController
import com.crskdev.fcycling.presentation.screen.main.MenuLoggedProfileController
import com.crskdev.fcycling.presentation.screen.manager.RaceManagerResultsController
import com.crskdev.fcycling.presentation.screen.manager.RaceManagersOngoingUpcomingController
import com.crskdev.fcycling.presentation.screen.profile.FullProfileController
import com.crskdev.fcycling.presentation.screen.profile.ProfileStatsController
import com.crskdev.fcycling.presentation.screen.profile.ProfileStatsManagerRacesController
import com.crskdev.fcycling.presentation.screen.profile.victories.ProfileStatsVictoriesController
import com.franmontiel.persistentcookiejar.cache.SetCookieCache
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor
import com.squareup.leakcanary.LeakCanary
import fcycling.crskdev.com.data.connection.OkHttpConnection
import fcycling.crskdev.com.data.parse.login.LoginParser
import fcycling.crskdev.com.data.parse.manager.RaceManagerParser
import fcycling.crskdev.com.data.parse.manager.RaceManagersOngoingUpcomingParser
import fcycling.crskdev.com.data.parse.profile.FullProfileParser
import fcycling.crskdev.com.data.parse.profile.ProfileStatsManagerRacesParser
import fcycling.crskdev.com.data.parse.profile.ProfileStatsParser
import fcycling.crskdev.com.data.parse.profile.ProfileStatsVictoriesParser
import fcycling.crskdev.com.data.screen.AuthLinkSupervisor
import fcycling.crskdev.com.domain.parse.RequestProcessor
import fcycling.crskdev.com.domain.schedulers.SchedulersContract
import fcycling.crskdev.com.domain.screen.LinkRefreshHandler
import fcycling.crskdev.com.domain.screen.Screen.*
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import timber.log.Timber
import timber.log.Timber.DebugTree


/**
 * Created by Cristian Pela on 03.05.2018.
 */
class FCyclingApplication : MultiDexApplication() {

    private val persistentCookieJar by lazy {
        AddAllPersistentCookieJar(SetCookieCache(), SharedPrefsCookiePersistor(this))
    }

    internal val okHttpClient by lazy {
        createOkHttpClient(AndroidInternetConnectivityCheck(this), persistentCookieJar, cacheDir)
    }

    internal lateinit var linkDispatcher: LinkDispatcher

    internal lateinit var stateRestorerHandler: StateRestorerHandler

    internal val screenConfig = screenConfig {
        add(LOGIN_SCREEN_ID) {
            controller = { LoginController(it) }
            parser = LoginParser()
            layout = R.layout.login_layout
        }
        add(MENU_SCREEN_ID) {
            controller = { MenuController(it) }
            layout = R.layout.main_layout
        }
        add(MENU_LOGGED_PROFILE_SCREEN_ID) {
            controller = { MenuLoggedProfileController(it) }
            layout = R.layout.menu_logged_profile_layout_nav_header_delegate
        }
        add(FULL_PROFILE_SCREEN_ID) {
            controller = { FullProfileController(it) }
            parser = FullProfileParser()
            layout = R.layout.full_profile_layout
        }
        add(PROFILE_INFO_STATS_SCREEN_ID) {
            controller = { ProfileStatsController(it) }
            parser = ProfileStatsParser()
            layout = R.layout.profile_stats_layout
        }
        add(PROFILE_INFO_STATS_MANAGER_RACES_SCREEN_ID) {
            controller = { ProfileStatsManagerRacesController(it) }
            parser = ProfileStatsManagerRacesParser()
            layout = R.layout.profile_stats_manager_races_layout
        }
        add(PROFILE_INFO_STATS_VICTORIES) {
            controller = { ProfileStatsVictoriesController(it) }
            parser = ProfileStatsVictoriesParser()
            layout = R.layout.profile_stats_victories_layout
        }
        add(RACE_MANAGER_RESULTS_SCREEN_ID){
            controller = { RaceManagerResultsController(it) }
            parser = RaceManagerParser()
            layout = R.layout.race_manager_results_layout
        }
        add(RACE_MANAGERS_ONGOING_UPCOMING_SCREEN_ID){
            controller = { RaceManagersOngoingUpcomingController(it)}
            parser = RaceManagersOngoingUpcomingParser()
            layout = R.layout.race_managers_ongoing_upcoming_layout
        }
    }

    override fun onCreate() {
        super.onCreate()
        //install canary leak
        if (isCanaryLeakInAnalyzerProcess()) return
        installTimber()
        setupDependenciesGraph()
    }

    private fun installTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            //timber for crashlytics?
        }
    }

    @Suppress("UNUSED_VARIABLE")
    private fun setupDependenciesGraph() {
        val requestProcessorTest = TestRequestProcessor()
        val requestProcessor = RequestProcessor(
                OkHttpConnection(okHttpClient),
                screenConfig.parserFactory,
                object : LinkRefreshHandler {},
                AuthLinkSupervisor(PersistentCookieStoreValueFinder(persistentCookieJar)),
                object : SchedulersContract {
                    override fun trampoline(): Scheduler = Schedulers.trampoline()

                    override fun io(): Scheduler = Schedulers.io()

                    override fun ui(): Scheduler = AndroidSchedulers.mainThread()

                    override fun computation(): Scheduler = Schedulers.computation()

                    override fun single(): Scheduler = Schedulers.single()
                })
        linkDispatcher = LinkDispatcher(requestProcessor)
        stateRestorerHandler = StateRestorerHandler
                .install(this, JSONStateRestorer(AndroidStateIO(this)))
    }

    private fun isCanaryLeakInAnalyzerProcess(): Boolean {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return true
        }
        LeakCanary.install(this)
        return false
    }

}

fun Application.getLinkDispatcher() = (this as FCyclingApplication).linkDispatcher

fun Application.getStateRestorer(): StateRestorerHandler = (this as FCyclingApplication).stateRestorerHandler

fun Application.getScreenConfig(): ScreenConfig = (this as FCyclingApplication).screenConfig

@Suppress("unused")
fun Application.getNetworkClient(): OkHttpClient = (this as FCyclingApplication).okHttpClient

