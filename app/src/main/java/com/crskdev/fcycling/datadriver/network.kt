package com.crskdev.fcycling.datadriver

import com.crskdev.fcycling.BuildConfig
import com.franmontiel.persistentcookiejar.PersistentCookieJar
import com.franmontiel.persistentcookiejar.cache.CookieCache
import com.franmontiel.persistentcookiejar.persistence.CookiePersistor
import fcycling.crskdev.com.data.connection.CookieValueFinder
import fcycling.crskdev.com.data.connection.HEADER_AGENT
import fcycling.crskdev.com.data.connection.HEADER_AGENT_VALUE
import fcycling.crskdev.com.data.connection.InternetConnectivityCheck
import fcycling.crskdev.com.data.connection.interceptors.CacheNetworkInterceptor
import fcycling.crskdev.com.data.connection.interceptors.ContentTypeTo_ISO_8859_1_NetworkInterceptor
import fcycling.crskdev.com.data.connection.interceptors.LogoutNetworkInterceptor
import fcycling.crskdev.com.data.connection.interceptors.OfflineCacheInterceptor
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.util.concurrent.TimeUnit


/**
 * Created by Cristian Pela on 30.05.2018.
 */
fun createOkHttpClient(connectivityCheck: InternetConnectivityCheck,
                       cookieJar: CookieJar? = null,
                       cacheDirectory: File? = null): OkHttpClient =
        OkHttpClient.Builder()
                //for these network interceptors, order is important: LIFO (last added will intercept first)
                .addNetworkInterceptor(CacheNetworkInterceptor())
                .addNetworkInterceptor(LogoutNetworkInterceptor())
                .addNetworkInterceptor(ContentTypeTo_ISO_8859_1_NetworkInterceptor())
                //---
                //user agent interceptor
                .addInterceptor {
                    it.proceed(it.request()
                            .newBuilder()
                            .removeHeader(HEADER_AGENT)
                            .header(HEADER_AGENT, HEADER_AGENT_VALUE)
                            .build())
                }
                .addInterceptor(OfflineCacheInterceptor(connectivityCheck))
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .let {
                    if (cookieJar != null)
                        it.cookieJar(cookieJar)
                    else
                        it
                }
                .let {
                    if (cacheDirectory != null) {
                        it.cache(Cache(cacheDirectory, 10L * 1024 * 1024))//10MB cache
                    } else
                        it
                }
                .let {
                    if (BuildConfig.DEBUG) {
                        it.addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.HEADERS })
                    } else
                        it
                }
                .build()

class PersistentCookieStoreValueFinder(private val cookieJar: PersistentCookieJar) : CookieValueFinder {
    override fun findValue(url: String, name: String): String? =
            cookieJar.loadForRequest(HttpUrl.parse(url)
                    ?: throw Error("Could not create HttpUrl for $url"))
                    .firstOrNull { it.name() == name }
                    ?.value()


    override fun exists(url: String, name: String): Boolean =
            cookieJar.loadForRequest(HttpUrl.parse(url)
                    ?: throw Error("Could not create HttpUrl for $url"))
                    .firstOrNull { it.name() == name } != null

}


class AddAllPersistentCookieJar(private val cache: CookieCache, private val persistor: CookiePersistor) :
        PersistentCookieJar(cache, persistor) {
    override fun saveFromResponse(url: HttpUrl?, cookies: MutableList<Cookie>?) {
        cache.addAll(cookies)
        persistor.saveAll(cookies)
    }
}