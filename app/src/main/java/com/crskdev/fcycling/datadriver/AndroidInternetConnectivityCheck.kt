package com.crskdev.fcycling.datadriver

import android.content.Context
import android.net.ConnectivityManager
import androidx.core.content.systemService
import fcycling.crskdev.com.data.connection.InternetConnectivityCheck

/**
 * Created by Cristian Pela on 07.06.2018.
 */
class AndroidInternetConnectivityCheck(private val context: Context): InternetConnectivityCheck {

    override fun hasInternet(): Boolean {
        val connectivityManager = context.systemService<ConnectivityManager>()
        return connectivityManager.activeNetworkInfo?.let {
            it.isConnectedOrConnecting
        } ?: false
    }
}