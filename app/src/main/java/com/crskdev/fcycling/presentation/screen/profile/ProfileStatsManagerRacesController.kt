package com.crskdev.fcycling.presentation.screen.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.crskdev.fcycling.R
import com.crskdev.fcycling.loadCompoundDrawableLeft
import com.crskdev.fcycling.util.BaseGlideController
import com.crskdev.fcycling.util.EqualSpacingDecoration
import com.crskdev.fcycling.util.kast
import fcycling.crskdev.com.data.parse.profile.ManagerRacesModel
import fcycling.crskdev.com.domain.screen.Link
import kotlin.reflect.KClass

/**
 * Created by Cristian Pela on 24.05.2018.
 */
class ProfileStatsManagerRacesController(args: Bundle) : BaseGlideController<ManagerRacesModel>(args),
        ManagerRacesAdapter.OnLinkClicked{

    override fun onManagerRacesAdapterLinkClicked(link: Link) {
        dispatchLink(link)
    }

    override fun modelClass(): KClass<ManagerRacesModel> = ManagerRacesModel::class

    override fun onAttach(view: View, isStateRestored: Boolean) {
        recycler(R.id.recyclerProfileStatsManagerRaces).apply {
            addItemDecoration(EqualSpacingDecoration(2, EqualSpacingDecoration.HORIZONTAL))
            adapter = ManagerRacesAdapter(this@ProfileStatsManagerRacesController,
                    LayoutInflater.from(activity), glide)
        }
        txt(R.id.textHeaderMinProfileTitle).text = activity?.getString(R.string.lbl_profile_stats_manager_races_title)
        imgBtn(R.id.btnHeaderMinProfileBack).setOnClickListener {
            router.popCurrentController()
        }
    }

    override fun onModelAttached(model: ManagerRacesModel) {
        recycler(R.id.recyclerProfileStatsManagerRaces)
                .adapter.kast<ManagerRacesAdapter>()
                .setList(model.races)
        txt(R.id.textHeaderMinProfileTeamname).apply {
            text = model.minimalProfile.managerName
            model.minimalProfile.flagUrl?.let {
                glide.loadCompoundDrawableLeft(this, it)
            }
        }
    }

}