package com.crskdev.fcycling.presentation.screen

import android.content.Context
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.screen.Link
import java.io.File
import java.io.IOException
import java.math.BigInteger
import java.security.MessageDigest
import kotlin.reflect.KClass

interface StateRestorer {
    fun <M : Model> saveState(link: Link, state: M)
    fun <M : Model> loadState(link: Link, klass: KClass<M>): M?
}

class JSONStateRestorer(private val io: StateIO) : StateRestorer {

    private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    override fun <M : Model> saveState(link: Link, state: M) {
        val adapter = moshi.adapter(state.javaClass)
        val jsonModel = adapter.toJson(state)
        try {
            io.write(link, jsonModel)
        } catch (ex: IOException) {
            ex.printStackTrace()
        }

    }

    override fun <M : Model> loadState(link: Link, klass: KClass<M>): M? {
        val adapter = moshi.adapter(klass.java)
        return try {
            adapter.fromJson(io.read(link)).apply {
                io.delete(link) // after successfully read delete the file
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
            null
        }
    }
}

interface StateIO {

    @Throws(IOException::class)
    fun write(link: Link, stringState: String)

    @Throws(IOException::class)
    fun read(link: Link): String

    @Throws(IOException::class)
    fun delete(link: Link): Boolean

    fun fileNameGenerator(link: Link): String = MD5FileNameGenerator.generate(link.toString())
}

object MD5FileNameGenerator {

    fun generate(name: String): String = try {
        val messageDigest = MessageDigest.getInstance("MD5")
        messageDigest.update(name.toByteArray(), 0, name.length)
        BigInteger(1, messageDigest.digest()).toString(16)
                .let { if (it.length < 32) it.padStart(32 - it.length, '0') else it }
                .toUpperCase()
    } catch (ex: Exception) {
        throw  ex
    }
}

class AndroidStateIO(private val context: Context) : StateIO {

    companion object {
        private val FOLDER_NAME = "files" + File.pathSeparator + "state_restore"
    }

    override fun delete(link: Link): Boolean = getFile(link).delete()

    override fun write(link: Link, stringState: String) {
        getFile(link).outputStream().use {
            println("Saving state for link $link $stringState")
            it.write(stringState.toByteArray())
        }
    }

    override fun read(link: Link): String =
            getFile(link).inputStream().bufferedReader().use {
                it.readText().apply {
                    println("Loading state for link $link $this")
                }
            }

    private fun getFile(link: Link): File = context
            .getDir(FOLDER_NAME, Context.MODE_PRIVATE)
            .let { File(it, fileNameGenerator(link)) }

}