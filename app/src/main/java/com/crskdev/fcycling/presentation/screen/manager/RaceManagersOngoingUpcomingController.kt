package com.crskdev.fcycling.presentation.screen.manager

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.crskdev.fcycling.GlideRequests
import com.crskdev.fcycling.R
import com.crskdev.fcycling.loadCompoundDrawableLeft
import com.crskdev.fcycling.util.BaseGlideController
import com.crskdev.fcycling.util.EqualSpacingDecoration
import com.crskdev.fcycling.util.kast
import fcycling.crskdev.com.data.parse.manager.MinimalManager
import fcycling.crskdev.com.data.parse.manager.RaceManagersOngoingUpcomingModel
import fcycling.crskdev.com.domain.screen.Link
import kotlin.reflect.KClass

/**
 * Created by Cristian Pela on 17.06.2018.
 */
class RaceManagersOngoingUpcomingController(args: Bundle) : BaseGlideController<RaceManagersOngoingUpcomingModel>(args),
        RaceManagersOngoingUpcomingAdapter.OnRecyclerItemClick {

    override fun onRecyclerItemClick(link: Link) {
        dispatchLink(link)
    }

    override fun onAttach(view: View, isStateRestored: Boolean) {
        val inflater = LayoutInflater.from(view.context)
        val recyclerFeats: RecyclerView.() -> Unit = {
            addItemDecoration(EqualSpacingDecoration(4, EqualSpacingDecoration.HORIZONTAL))
            isNestedScrollingEnabled = false
            adapter = RaceManagersOngoingUpcomingAdapter(
                    this@RaceManagersOngoingUpcomingController,
                    inflater,
                    glide)
        }
        recycler(R.id.recyclerManagerRacesOnUpOngoing).apply(recyclerFeats)
        recycler(R.id.recyclerManagerRacesOnUpUpcoming).apply(recyclerFeats)
    }

    override fun onModelAttached(model: RaceManagersOngoingUpcomingModel) {
        recycler(R.id.recyclerManagerRacesOnUpOngoing)
                .adapter.kast<RaceManagersOngoingUpcomingAdapter>()
                .setList(model.ongoing)
        recycler(R.id.recyclerManagerRacesOnUpUpcoming)
                .adapter.kast<RaceManagersOngoingUpcomingAdapter>()
                .setList(model.upcoming)
    }

    override fun modelClass(): KClass<RaceManagersOngoingUpcomingModel> = RaceManagersOngoingUpcomingModel::class
}


class RaceManagersOngoingUpcomingAdapter(
        private val onRecyclerItemClick: OnRecyclerItemClick,
        private val inflater: LayoutInflater,
        private val glide: GlideRequests) : RecyclerView.Adapter<RaceManagersOngoingUpcomingVH>() {

    private val items = mutableListOf<MinimalManager>()

    fun setList(newItems: List<MinimalManager>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RaceManagersOngoingUpcomingVH =
            inflater.inflate(R.layout.item_race_managers_ongoing_upcoming_layout, parent, false)
                    .let { RaceManagersOngoingUpcomingVH(it) }


    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RaceManagersOngoingUpcomingVH, position: Int) =
            items[position].let { item ->
                holder.txtName.apply {
                    text = item.name
                    item.flagUrl?.let {
                        glide.loadCompoundDrawableLeft(this, it, 15, 10)
                    }
                }
                holder.txtDate.text = item.date
                holder.itemView.setTag(R.id.recyclingTag, item.link)
                holder.itemView.setOnClickListener {
                    it.getTag(R.id.recyclingTag)?.let {
                        onRecyclerItemClick.onRecyclerItemClick(it as Link)
                    }
                }
                Unit
            }

    interface OnRecyclerItemClick {
        fun onRecyclerItemClick(link: Link)
    }

}


class RaceManagersOngoingUpcomingVH(view: View) : RecyclerView.ViewHolder(view) {
    val txtName = view.findViewById<TextView>(R.id.textManagerRacesOnUpName)!!
    val txtDate = view.findViewById<TextView>(R.id.textManagerRacesOnUpDate)!!
}