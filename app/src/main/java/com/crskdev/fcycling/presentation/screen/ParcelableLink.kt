@file:Suppress("unused")

package com.crskdev.fcycling.presentation.screen

import android.os.Parcelable
import fcycling.crskdev.com.domain.connection.Request
import fcycling.crskdev.com.domain.screen.Container
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.RouteBehavior
import fcycling.crskdev.com.domain.screen.Screen
import kotlinx.android.parcel.Parcelize

/**
 * Created by Cristian Pela on 03.05.2018.
 */
@Parcelize
data class ParcelableLink(val routeBehavior: ParcelableRouteBehavior,
                          val request: ParcelableRequest? = null,
                          val isRefreshable: Boolean = false,
                          val redirectLink: ParcelableLink? = null,
                          private val tag: String? = null,
                          private val containerTag: String? = null,
                          private val screenTag: String? = null) : Parcelable {

    fun tag(): String = tag ?: toString()

    fun containerTag(): String = containerTag
            ?: Container.values()[routeBehavior.containerId].toString()

    fun screenTag(): String = screenTag ?: Screen.values()[routeBehavior.screenId].toString()

    fun skipRequest(skip: Boolean = true): ParcelableLink = this.copy(request = this.request?.copy(skipForNow = skip))
}

/**
 * Created by Cristian Pela on 03.05.2018.
 */
@Parcelize
data class ParcelableRouteBehavior(val screenId: Int,
                                   val containerId: Int,
                                   val showScreenAsChild: Boolean = false,
                                   val hasPop: Boolean = false) : Parcelable {
    override fun toString(): String {
        return "ParcelableRouteBehavior[screenId:${Screen.values()[screenId]}" +
                ",containerId:${Container.values()[containerId]},showScreenAsChild:$showScreenAsChild,hasPop:${hasPop}]"
    }
}

/**
 * Created by Cristian Pela on 03.05.2018.
 */
@Parcelize
data class ParcelableRequest(
        val endPoint: String,
        val queryParams: Map<String, String> = emptyMap(),
        val formParams: Map<String, String> = emptyMap(),
        val skipForNow: Boolean = false,
        val cacheable: Boolean = true
) : Parcelable


class LinkParcelizer {

    fun parcelize(link: Link): ParcelableLink =
            ParcelableLink(
                    ParcelableRouteBehavior(
                            link.routeBehavior.screen.ordinal,
                            link.routeBehavior.container.ordinal,
                            link.routeBehavior.showScreenAsChild,
                            link.routeBehavior.hasPop),
                    link.request?.let {
                        ParcelableRequest(it.endPoint, it.queryParams,
                                it.formParams, it.skipForNow, it.cacheable)
                    },
                    link.isRefreshable,
                    link.redirectLink?.let { parcelize(it) },
                    link.tag(),
                    link.containerTag(),
                    link.screenTag)

    fun deparcelize(link: ParcelableLink): Link =
            Link(
                    RouteBehavior(
                            Screen.values()[link.routeBehavior.screenId],
                            Container.values()[link.routeBehavior.containerId],
                            link.routeBehavior.showScreenAsChild,
                            link.routeBehavior.hasPop),
                    link.request?.let {
                        Request(it.endPoint, it.queryParams, it.formParams,
                                it.skipForNow, it.cacheable)
                    },
                    link.isRefreshable,
                    link.redirectLink?.let { deparcelize(it) },
                    link.tag(),
                    link.containerTag(),
                    link.screenTag()
            )
}

fun Link.parcelize() = LinkParcelizer().parcelize(this)
fun ParcelableLink.deparcelize() = LinkParcelizer().deparcelize(this)