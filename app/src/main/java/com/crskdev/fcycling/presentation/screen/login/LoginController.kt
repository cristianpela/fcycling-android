package com.crskdev.fcycling.presentation.screen.login

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.constraint.Group
import android.support.transition.ChangeBounds
import android.support.transition.TransitionManager
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.view.View
import com.crskdev.fcycling.R
import com.crskdev.fcycling.presentation.screen.BaseController
import com.crskdev.fcycling.util.hideSoftInputKeyboard
import fcycling.crskdev.com.data.parse.login.LoginModel
import fcycling.crskdev.com.domain.screen.loginLinkSubmit
import kotlin.reflect.KClass

/**
 * Created by Cristian Pela on 02.05.2018.
 */
class LoginController(bundle: Bundle) : BaseController<LoginModel>(bundle) {

    override fun modelClass(): KClass<LoginModel> = LoginModel::class

    override fun onAttach(view: View, isStateRestored: Boolean) {
        btn(R.id.btnLogin).setOnClickListener {
            activity?.hideSoftInputKeyboard()
            val username = edit(R.id.editUsername).text.toString()
            val password = edit(R.id.editPassword).text.toString()
            dispatchLink(loginLinkSubmit(username, password))
        }
    }

    override fun onModelAttached(model: LoginModel) {
        model.fullProfileLink?.let { link ->
            v<Group>(R.id.groupLogin).apply { visibility = View.INVISIBLE }
            txt(R.id.textLoginSuccessful).text = activity
                    ?.getString(R.string.lbl_login_successful, model.userName)
            showWelcomeMessage()
            btn(R.id.btnDashboard).setOnClickListener {
                dispatchLink(link)
            }
        } ?: v<Group>(R.id.groupLogin).apply { visibility = View.VISIBLE }
    }

    private fun showWelcomeMessage() {
        val constraintSet = ConstraintSet()
        constraintSet.clone(activity, R.layout.login_successful_layout)
        val transition = ChangeBounds().apply {
            interpolator = FastOutSlowInInterpolator()
            duration = 750
        }
        val constraintLayout = v<ConstraintLayout>(R.id.constraintLogin)
        TransitionManager.beginDelayedTransition(constraintLayout, transition)
        constraintSet.applyTo(constraintLayout)
    }
}



