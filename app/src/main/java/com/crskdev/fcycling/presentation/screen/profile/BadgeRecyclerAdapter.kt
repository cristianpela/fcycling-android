package com.crskdev.fcycling.presentation.screen.profile

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.crskdev.fcycling.GlideRequests
import com.crskdev.fcycling.R
import fcycling.crskdev.com.data.parse.profile.FullProfileModel

/**
 * Created by Cristian Pela on 16.05.2018.
 */
internal class BadgeRecyclerAdapter(private val glide: GlideRequests) : RecyclerView.Adapter<BadgeViewHolder>() {

    private val items = mutableListOf<FullProfileModel.Info.Badge>()

    fun setList(newItems: List<FullProfileModel.Info.Badge>) {
        if (items.isNotEmpty()) {
            items.clear()
        }
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: BadgeViewHolder, position: Int) {
        val item = items[position]
        glide.load(item.url)
                .override(holder.itemView.resources.getDimensionPixelSize(R.dimen.badge_dimen))
                .centerCrop()
                .into(holder.imgBtnBadge)
    }

    override fun onViewRecycled(holder: BadgeViewHolder) {
       // glide.clear(holder.imgBtnBadge)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BadgeViewHolder =
            LayoutInflater.from(parent.context).inflate(R.layout.item_badge_layout, parent, false).let {
                BadgeViewHolder(it)
            }
}


internal class BadgeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val imgBtnBadge = view.findViewById<ImageView>(R.id.btnBadge)!!
}