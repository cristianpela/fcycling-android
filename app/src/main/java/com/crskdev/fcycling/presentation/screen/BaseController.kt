package com.crskdev.fcycling.presentation.screen

import android.app.Application
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.annotation.CallSuper
import android.support.design.widget.AppBarLayout
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.util.SparseArrayCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.bundleOf
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler
import com.crskdev.fcycling.*
import com.crskdev.fcycling.util.*
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.Screen
import fcycling.crskdev.com.domain.screen.ScreenModel
import timber.log.Timber
import java.util.*
import kotlin.reflect.KClass

abstract class BaseController<M : Model>(bundle: Bundle) : Controller(bundle), StateRestoredReceiveCallback {

    companion object {

        @PublishedApi
        internal const val CONTROLLER_LINK_KEY = "CONTROLLER_LINK_KEY"

        @Suppress("MemberVisibilityCanBePrivate")
        inline fun route(router: Router, link: ParcelableLink, model: Model?, controllerCreator: (args: Bundle) -> BaseController<*>) {
            val controllerTag = link.tag()
            router.getControllerWithTag(controllerTag)
                    ?.kast<BaseController<*>>()
                    ?.apply {
                        if (this.router.backstack.last().controller() != this) {
                            this.router.popToTag(controllerTag)
                        }
                        addModel(model)
                    }
                    ?: bundleOf(CONTROLLER_LINK_KEY to link).let {
                        val newController = controllerCreator(it)
                        newController.addLifecycleListener(object : LifecycleListener() {
                            override fun postAttach(controller: Controller, view: View) {
                                (controller as BaseController<*>).addModel(model)
                                controller.removeLifecycleListener(this)
                            }
                        })
                        val transaction = RouterTransaction
                                .with(newController)
                                .tag(controllerTag)
                                .pushChangeHandler(HorizontalChangeHandler())
                                .popChangeHandler(HorizontalChangeHandler())
                        if (!router.hasRootController() || !link.routeBehavior.hasPop) {
                            router.setRoot(transaction)
                        } else {
                            router.pushController(transaction)
                        }
                    }
        }

        fun route(router: Router, link: ParcelableLink, model: Model?, controllerFactory: ControllerFactory) {
            route(router, link, model) {
                controllerFactory.get(Screen.values()[link.routeBehavior.screenId], it)
            }
        }
    }

    @PublishedApi
    internal val cachedViews = WeakHashMap<Int, View>()

    protected val postHandler = Handler(Looper.getMainLooper())

    init {
        addLifecycleListener(object : LifecycleListener() {
            override fun postAttach(controller: Controller, view: View) {
                attemptAttachView(view)
            }

            override fun preDestroyView(controller: Controller, view: View) {
                postHandler.removeCallbacksAndMessages(null)
            }

            override fun postDestroyView(controller: Controller) {
                isViewAttached = false
                if (controller is StateRestoredReceiveCallback) {
                    getStateRestorer()?.evict(controller)
                }
                cachedViews.clear()
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return args.let {
            val screenId: Int = getControllerLink().routeBehavior.screenId
            val layoutId = container
                    .application()
                    .getScreenConfig()
                    .screenToLayoutMapper[Screen.values()[screenId]]!!
            inflater.inflate(layoutId, container, false)
        }.apply { println("Created view ${this@BaseController}") }
    }

    open fun onAttach(view: View, isStateRestored: Boolean) {}

    //######################################LINKS HANDLING##########################################
    protected fun dispatchScreenModel(screenDataModel: ScreenModel.DataModel) =
            activity?.kast<FCyclingConductorActivity>()?.route(screenDataModel)

    @Suppress("MemberVisibilityCanBePrivate")
    protected fun getControllerLink() = (args.getParcelable(CONTROLLER_LINK_KEY) as ParcelableLink)


    override fun link(): Link = getControllerLink().deparcelize()

    @Suppress("MemberVisibilityCanBePrivate")
    protected fun dispatchLink(link: ParcelableLink?) {
        link?.let {
            Timber.d("$this Dispatching: $link")
            activity?.dispatchLink(it)
        }
    }

    protected fun dispatchLink(link: Link?) {
        link?.let {
            Timber.d("$this Dispatching: $link")
            activity?.dispatchLink(it)
        }
    }

    protected open fun autoDispatchControllerLinkEnabled(): Boolean = true

    //######################################MODEL STATE HANDLING####################################
    protected var state: M? = null
        private set
    private var isStateRestoring = false
    private var isViewAttached = false

    @Suppress("UNCHECKED_CAST")
    fun addModel(state: Model?) {
        this.state = state?.let { it as M } ?: this.state
        if (this.state == null && !isStateRestoring) {
            onEmptyModelReceivedFromSkippedRequest()
        } else {
            this.state?.let { m ->
                if (isViewAttached) {
                    onModelAttached(m)
                }
                activity?.notifyModelProcessed()
            }
        }
    }

    private fun getStateRestorer() = applicationContext?.kast<Application>()?.getStateRestorer()

    private fun onRestoreViewStateAsync(klass: KClass<M>) {
        getStateRestorer()?.loadState(this, klass)
    }

    override fun onRestoreViewState(view: View, savedViewState: Bundle) {
        isStateRestoring = true
        state?.let {
            attemptAttachView(view)
            onModelAttached(it)
            isStateRestoring = false
        } ?: onRestoreViewStateAsync(modelClass())

    }

    private fun onSaveViewStateAsync() {
        state?.let {
            getStateRestorer()?.saveState(link(), it)
        }
    }

    @CallSuper
    override fun onSaveViewState(view: View, outState: Bundle) {
        onSaveViewStateAsync()
    }

    @Suppress("UNCHECKED_CAST")
    @CallSuper
    override fun onReceiveRestoredState(state: Model?) {
        isStateRestoring = false
        attemptAttachView(view!!)
        state?.let {
            this.state = it as M
            onModelAttached(it)
        } ?: if (autoDispatchControllerLinkEnabled()) dispatchLink(getControllerLink())
    }

    private fun attemptAttachView(view: View) {
        if (!isViewAttached) {
            onAttach(view, true)
            isViewAttached = true
        }
    }

    override fun onAttach(view: View) {
        attemptAttachView(view)
    }

    abstract fun onModelAttached(model: M)
    /**
     *Called when model sent in addModel is null - usually this happens when the request is skipped
     */
    open fun onEmptyModelReceivedFromSkippedRequest() {}

    abstract fun modelClass(): KClass<M>

    //######################################HELPERS#################################################
    inline fun <reified V : View> v(id: Int, parent: View?): V {
        return cachedViews[id]?.kast<V>()
                ?.let { it }
                ?: parent?.let {
                    val v: View = it.findViewById(id)
                            ?: throw Error("Could not find view for id $id")
                    cachedViews[id] = v
                    v as V
                }
                ?: throw Error("Could not find a view. Parent is null!")
    }

    inline fun <reified V : View> v(id: Int): V = v(id, view)
    fun txt(id: Int, view: View? = this.view) = v<TextView>(id, view)
    fun edit(id: Int, view: View? = this.view) = v<EditText>(id, view)
    fun btn(id: Int, view: View? = this.view) = v<Button>(id, view)
    fun imgBtn(id: Int, view: View? = this.view) = v<ImageButton>(id, view)
    fun img(id: Int, view: View? = this.view) = v<ImageView>(id, view)
    fun toolbar(id: Int, view: View? = this.view) = v<Toolbar>(id, view)
    fun drawer(id: Int, view: View? = this.view) = v<DrawerLayout>(id, view)
    fun nav(id: Int, view: View? = this.view) = v<NavigationView>(id, view)
    fun tabs(id: Int, view: View? = this.view) = v<TabLayout>(id, view)
    fun viewPager(id: Int, view: View? = this.view) = v<ViewPager>(id, view)
    fun appbar(id: Int, view: View? = this.view) = v<AppBarLayout>(id, view)
    fun recycler(id: Int, view: View? = this.view) = v<RecyclerView>(id, view)

    fun Int.toPx(unit: Int): Float = toPx(resources
            ?: throw Error("Resources object is null"), unit)

    fun Int.dpToPx(): Int = dpToPx(resources ?: throw Error("Resources object is null"))
    fun Int.spToPx(): Float = spToPx(resources ?: throw Error("Resources object is null"))
}


abstract class BaseParentController<M : Model>(bundle: Bundle) : BaseController<M>(bundle) {

    companion object {
        private const val KEY_VIEW_CONTAINER_IDS = "KEY_VIEW_CONTAINER_IDS"
    }

    val routers = SparseArrayCompat<MutableList<TaggedRouter>>()

    private val linkParcelizer = LinkParcelizer()

    fun addChild(screenModel: ScreenModel.DataModel, controllerFactory: ControllerFactory, viewContainerID: Int) {
        val link = linkParcelizer.parcelize(screenModel.originalLink)
        val routerTag = link.containerTag()

        if (routers[viewContainerID] == null) {
            val list = mutableListOf<TaggedRouter>()
            val router = getRouterByTag(routerTag)
                    ?: getChildRouter(findViewContainerByID(viewContainerID)
                            ?: throw Error("Could not find inflated container for id $viewContainerID"), routerTag)
            list.add(routerTag to router)
            routers.put(viewContainerID, list)
        }

        routers[viewContainerID]?.let {
            if (it.isEmpty()) {
                val router = getRouterByTag(routerTag)
                        ?: getChildRouter(findViewContainerByID(viewContainerID)
                                ?: throw Error("Could not find inflated container for id $viewContainerID"), routerTag)
                it.add(routerTag to router)
            }
            val taggedRouter = it.firstOrNull { it.first == routerTag }
                    ?: it.first()
                    ?: throw Error("Router with tag $routerTag was not found for view container $viewContainerID")
            BaseController.route(taggedRouter.second, link, screenModel.model, controllerFactory)
        }

    }

    private fun getRouterByTag(routerTag: String): Router? {
        try {
            childRouters.forEach {
                val ctrlHostedRouterClass = Class.forName("com.bluelinelabs.conductor.ControllerHostedRouter")
                        .cast(it).javaClass
                val field = ctrlHostedRouterClass.getDeclaredField("tag")
                field.isAccessible = true
                val foundRouterTag = field.get(it).toString()
                val found = foundRouterTag == routerTag
                field.isAccessible = false
                if (found) return it
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return null
    }

    //TODO recreate routers containers from inside after restore, instead from the outside

//    @CallSuper
//    override fun onSaveViewState(view: View, outState: Bundle) {
//
//        val keys = generateSequence(0) { it + 1 }
//                .take(routers.size())
//                .map { routers.keyAt(it) }
//                .toList()
//                .toIntArray()
//
//        outState.putIntArray(KEY_VIEW_CONTAINER_IDS, keys)
//    }
//
//    @CallSuper
//    override fun onRestoreViewState(view: View, savedViewState: Bundle) {
//        savedViewState.getIntArray(KEY_VIEW_CONTAINER_IDS)
//                .takeIf { it.isNotEmpty() }
//                ?.let {
//                    it.forEach {
//                        val viewContainerID = it
//                        childRouters.forEach { router ->
//                            router.backstack
//                                    .firstOrNull {
//                                        it.controller()
//                                                .view?.parent
//                                                ?.kast<ViewGroup>()
//                                                ?.id == viewContainerID
//                                    }
//                                    ?.let {
//
//                                    }
//                        }
//                    }
//                }
//    }

    protected open fun findViewContainerByID(containerId: Int): ViewGroup? =
            view?.findViewById(containerId)


    abstract fun availableContainers(): IntArray


}

typealias TaggedRouter = Pair<String, Router>

