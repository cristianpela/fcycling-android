package com.crskdev.fcycling.presentation.screen

import android.app.Activity
import android.app.Application
import android.os.*
import android.support.annotation.WorkerThread
import fcycling.crskdev.com.domain.parse.Model
import fcycling.crskdev.com.domain.screen.Link
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.reflect.KClass

/**
 * Created by Cristian Pela on 04.06.2018.
 */
class StateRestorerHandler private constructor(private val stateRestorer: StateRestorer, lifecycleCallbacksDelegate: Application.ActivityLifecycleCallbacks) :
        Application.ActivityLifecycleCallbacks by lifecycleCallbacksDelegate {

    companion object {
        fun install(application: Application, stateRestorer: StateRestorer): StateRestorerHandler {
            return StateRestorerHandler(stateRestorer, SimpleActivityLifecycleCallbacks())
                    .apply {
                        application.registerActivityLifecycleCallbacks(this)
                        bgHandlerThread.start()
                        bgHandler = BackgroundHandler(bgHandlerThread.looper)
                    }

        }

        private const val MSG_STATE_SAVE = 0

        private const val MSG_STATE_LOAD = 1
    }

    private val bgHandlerThread = HandlerThread("StateRestorerHandler-Thread")
    private var bgHandler: Handler? = null
    private val uiHandler = Handler(Looper.getMainLooper())

    private val isActivityAttached = AtomicBoolean(false)

    private var stateRestoredReceiveCallbacks = CopyOnWriteArrayList<StateRestoredReceiveCallback>()

    fun <M : Model> loadState(stateRestoredReceiveCallback: StateRestoredReceiveCallback, modelClass: KClass<M>) {
        if (!stateRestoredReceiveCallbacks.contains(stateRestoredReceiveCallback)) {
            stateRestoredReceiveCallbacks.add(stateRestoredReceiveCallback)
        }
        val message = bgHandler?.obtainMessage()?.apply {
            what = MSG_STATE_LOAD
            obj = stateRestoredReceiveCallback.link() to modelClass
        }
        bgHandler?.sendMessage(message)
    }

    fun <M : Model> saveState(link: Link, model: M) {
        val message = bgHandler?.obtainMessage()?.apply {
            what = MSG_STATE_SAVE
            obj = link to model
        }
        bgHandler?.sendMessage(message)
    }

    fun evict(callback: StateRestoredReceiveCallback) {
        stateRestoredReceiveCallbacks.remove(callback)
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
        isActivityAttached.compareAndSet(false, activity != null)
    }

    override fun onActivityDestroyed(activity: Activity?) {
        isActivityAttached.compareAndSet(true, false)
        stateRestoredReceiveCallbacks.clear()
    }

    private inner class BackgroundHandler(looper: Looper) : Handler(looper) {

        override fun handleMessage(msg: Message) {
            println("BackgroundHandler handle messages on: " + Thread.currentThread() + " The ui thread is: " + Looper.getMainLooper().thread)
            if (Thread.currentThread() == Looper.getMainLooper().thread) {
                throw Exception("Messages must be processed on the background thread")
            }
            when (msg.what) {
                MSG_STATE_SAVE -> {
                    saveState(msg)
                }
                MSG_STATE_LOAD -> {
                    if (isActivityAttached.get().not())
                        return
                    loadState(msg)
                }
            }
        }

        @Suppress("UNCHECKED_CAST")
        @WorkerThread
        private fun loadState(msg: Message) {
            val (link, modelClass) = msg.obj as Pair<Link, KClass<Model>>
            val receiver = stateRestoredReceiveCallbacks.firstOrNull { it.link() == link }
            receiver?.apply {
                stateRestorer.loadState(link, modelClass).apply {
                    uiHandler.post {
                        onReceiveRestoredState(this)
                    }
                }
                stateRestoredReceiveCallbacks.remove(this)
            }
        }

        @Suppress("UNCHECKED_CAST")
        @WorkerThread
        private fun saveState(msg: Message) {
            val (link, model) = msg.obj as Pair<Link, Model>
            stateRestorer.saveState(link, model)
        }
    }
}


interface StateRestoredReceiveCallback {

    fun onReceiveRestoredState(state: Model?)

    fun link(): Link

}

private class SimpleActivityLifecycleCallbacks : Application.ActivityLifecycleCallbacks {
    override fun onActivityPaused(activity: Activity?) = Unit

    override fun onActivityResumed(activity: Activity?) = Unit

    override fun onActivityStarted(activity: Activity?) = Unit

    override fun onActivityDestroyed(activity: Activity?) = Unit

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) = Unit

    override fun onActivityStopped(activity: Activity?) = Unit

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) = Unit
}