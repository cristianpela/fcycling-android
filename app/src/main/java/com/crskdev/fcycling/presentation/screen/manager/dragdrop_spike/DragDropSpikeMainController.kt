package com.crskdev.fcycling.presentation.screen.manager.dragdrop_spike

import android.content.ClipData
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageView
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.ControllerChangeHandler
import com.bluelinelabs.conductor.ControllerChangeType
import com.bluelinelabs.conductor.RouterTransaction
import com.crskdev.fcycling.R
import com.crskdev.fcycling.util.EqualSpacingDecoration
import com.crskdev.fcycling.util.kast


/**
 * Created by Cristian Pela on 14.06.2018.
 */
class DragDropSpikeMainController(args: Bundle) : Controller(args) {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
            inflater.inflate(R.layout.spike_dragdrop_main_layout, container, false)


    override fun onChangeEnded(changeHandler: ControllerChangeHandler, changeType: ControllerChangeType) {
        view?.let {
            if (changeType == ControllerChangeType.PUSH_ENTER) {
                getChildRouter(it.findViewById(R.id.spikeDragDropTopContainer))
                        .setRoot(RouterTransaction.with(DragDropSpikeTopController(Bundle())))

                getChildRouter(it.findViewById(R.id.spikeDragDropBottomContainer))
                        .setRoot(RouterTransaction.with(DragDropSpikeBottomController(Bundle())))
            }
        }
    }

}

class DragDropSpikeTopController(args: Bundle) : Controller(args) {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
            inflater.inflate(R.layout.spike_dragdrop_top_layout, container, false)

    override fun onAttach(view: View) {
        view.kast<RecyclerView>().apply {
            addItemDecoration(EqualSpacingDecoration(4, EqualSpacingDecoration.VERTICAL))
            adapter = DragDropAdapter()
        }
    }


    class DragDropAdapter : RecyclerView.Adapter<DragDropVH>() {

        private val images = mutableListOf(
                R.drawable.ic_no_avatar,
                R.drawable.ic_no_avatar,
                R.drawable.ic_no_avatar,
                R.drawable.ic_no_avatar,
                R.drawable.ic_no_avatar,
                R.drawable.ic_no_avatar,
                R.drawable.ic_no_avatar,
                R.drawable.ic_no_avatar
        )

        private fun replace(index: Int, imageId: Int) {
            images[index] = imageId
            notifyItemChanged(index)
        }

        override fun getItemCount(): Int = images.size

        override fun onBindViewHolder(holder: DragDropVH, position: Int) {
            images[position].let {
                holder.itemView.kast<ImageView>().apply {
                    setImageResource(it)
                    setOnDragListener { _, event ->
                        when (event.action) {
                            DragEvent.ACTION_DROP -> {
                                val imageId = event.clipData.getItemAt(0).text.toString().toInt()
                                replace(holder.adapterPosition, imageId)
                                true
                            }
                            else -> false
                        }
                    }

                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DragDropVH =
                LayoutInflater.from(parent.context).inflate(R.layout.spike_dragdrop_top_layout_item, parent, false)
                        .let {
                            DragDropVH(it)
                        }


    }

    class DragDropVH(view: View) : RecyclerView.ViewHolder(view)
}


class DragDropSpikeBottomController(args: Bundle) : Controller(args) {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
            inflater.inflate(R.layout.spike_dragdrop_bottom_layout, container, false)

    override fun onAttach(view: View) {
        view.findViewById<ImageView>(R.id.imgSpikeDragDropBottom).apply {
            setOnClickListener { it.performClick() }
            setOnTouchListener { v, e ->
                if (e.action == MotionEvent.ACTION_DOWN) {
                    val data = ClipData.newPlainText("DragDropImage", R.drawable.ic_avatar_place_holder.toString())
                    val shadowBuilder = View.DragShadowBuilder(this)
                    ViewCompat.startDragAndDrop(this, data, shadowBuilder, null, 0)
                    true
                } else {
                    false
                }
            }
        }

    }


}
