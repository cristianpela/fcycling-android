package com.crskdev.fcycling.presentation.screen.profile

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.toast
import com.crskdev.fcycling.R
import com.crskdev.fcycling.loadAvatar
import com.crskdev.fcycling.loadCompoundDrawableLeft
import com.crskdev.fcycling.presentation.screen.BaseParentController
import com.crskdev.fcycling.presentation.screen.LinkParcelizer
import com.crskdev.fcycling.util.*
import fcycling.crskdev.com.data.parse.profile.CompetitionGender
import fcycling.crskdev.com.data.parse.profile.FullProfileModel
import fcycling.crskdev.com.data.parse.profile.ProfileStatsLinks
import fcycling.crskdev.com.data.parse.profile.ProfileStatsVictoriesModel
import fcycling.crskdev.com.domain.screen.ScreenModel
import fcycling.crskdev.com.domain.screen.linkProfileInfoStatsScreen
import fcycling.crskdev.com.domain.screen.linkProfileInfoStatsVictories
import kotlin.math.abs
import kotlin.math.min
import kotlin.reflect.KClass

/**
 * Created by Cristian Pela on 08.05.2018.
 */
class FullProfileController(args: Bundle) : BaseParenGlideController<FullProfileModel>(args) {

    private var gender: CompetitionGender? = null

    private var pagerAdapter: PagerAdapter? = null

    private var onOffsetChangedListener: AppBarLayout.OnOffsetChangedListener? = AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
        val tabLayoutTranslationMax = 56.dpToPx()
        val percent = min(abs(verticalOffset), appBarLayout.totalScrollRange) / appBarLayout.totalScrollRange.toFloat()
        tabs(R.id.tabLayoutFullProfile).translationX = percent * tabLayoutTranslationMax
        v<ViewGroup>(R.id.constraintFullProfile).apply {
            alpha = 1 - percent
        }
    }

    override fun onAttach(view: View, isStateRestored: Boolean) {

        val viewPager = v<ViewPager>(R.id.view_pager_profile_info_stats_container_layout).apply {
            offscreenPageLimit = 4
            ViewCompat.setNestedScrollingEnabled(this, true)
            pagerAdapter?.let { adapter = it }
        }

        //if state is marked as restore we don wan't to dispatch link to the view pager
        //because their page controller is/might be restored also
        //we allow only subsequent tab selections to dispatch
        var canSelectTab = !isStateRestored
        tabs(R.id.tabLayoutFullProfile).apply {
            addTabSelectedListener { tab ->
                if (canSelectTab) {
                    state?.let { it ->
                        val id = it.minimalProfile.id
                        val isWomen = it.statsLinks?.raceGender === CompetitionGender.WOMEN
                        val year = tab.text.toString().toInt()
                        dispatchLink(linkProfileInfoStatsScreen(id, year, isWomen).skipRequest())
                    }
                }
                canSelectTab = true
            }
            setupWithViewPager(viewPager)
        }


        appbar(R.id.appBarFullProfile).addOnOffsetChangedListener(onOffsetChangedListener)

        recycler(R.id.recyclerFullProfileBadges).apply {
            adapter = BadgeRecyclerAdapter(glide)
            addItemDecoration(EqualSpacingDecoration(8, EqualSpacingDecoration.VERTICAL))
        }

        imgBtn(R.id.btnFullProfileGender).apply {
            //todo image for gender button
            setOnClickListener {
                state?.statsLinks?.let {
                    dispatchLink(it.nextGenderLink)
                }
            }
        }
    }

    override fun onModelAttached(model: FullProfileModel) {
        model.info.badgesUrls.let {
            recycler(R.id.recyclerFullProfileBadges).adapter.kast<BadgeRecyclerAdapter>()
                    .setList(it)
        }
        img(R.id.imageFullProfileAvatar).apply {
            model.minimalProfile.avatarUrl?.let {
                glide.loadAvatar(resources, it).into(this)
            } ?: setImageResource(R.drawable.ic_no_avatar)
        }


        imgBtn(R.id.btnFullProfileVictories).setOnClickListener {
            model.victories?.let {
                dispatchScreenModel(ScreenModel.DataModel(linkProfileInfoStatsVictories(model.minimalProfile.id),
                        ProfileStatsVictoriesModel(model.minimalProfile, it)))
            } ?: it.context.toast(it.context.getString(R.string.msg_profile_stats_no_victories))
        }

        imgBtn(R.id.btnFullProfileRefresh).setOnClickListener {
            dispatchLink(getControllerLink())
        }

        //btn(R.id.btnFullProfileGender).text = model.statsLinks?.raceGender?.symbol
        txt(R.id.textFullProfileUsername).text = model.minimalProfile.userName
        txt(R.id.textFullProfileTeam).text = resources
                ?.getString(R.string.lbl_full_profile_team_name, model.minimalProfile.managerName)
        txt(R.id.textFullProfileLocation).apply {
            text = resources
                    ?.getString(R.string.lbl_full_profile_location, model.info.location ?: "-")
            glide.loadCompoundDrawableLeft(this, model.info.nationFlagUrl!!)

        }
        txt(R.id.textFullProfileSince).text = resources
                ?.getString(R.string.lbl_full_profile_since, model.info.since)

        //set/change the adapter when gender has changed
        if (model.statsLinks?.raceGender != gender) {
            //setup the page view adapter
            viewPager(R.id.view_pager_profile_info_stats_container_layout).let { vp ->
                vp.adapter =
                        ProfileStatsPagerAdapter(model.statsLinks!!.availableYears,
                                R.id.view_pager_profile_info_stats_container_layout,
                                this)
            }
            model.statsLinks?.availableYears?.first()?.let {
                dispatchLink(it.link)
            }

        }

        gender = model.statsLinks?.raceGender.apply {
            this?.let {
                //todo gender image for this button
                // imgBtn(R.id.btnFullProfileGender).text = it.symbol
            }
        }


    }

    override fun onEmptyModelReceivedFromSkippedRequest() {
        dispatchLink(getControllerLink().skipRequest(false))
    }

    override fun availableContainers(): IntArray = intArrayOf(R.id.view_pager_profile_info_stats_container_layout)

    override fun modelClass(): KClass<FullProfileModel> = FullProfileModel::class

    override fun onDestroyView(view: View) {
        //saving the adapter later for when view is reattached. Is save to-do because doesn't
        //hold references with shorter life span than controller (context, view, actvitity etc...)
        pagerAdapter = viewPager(R.id.view_pager_profile_info_stats_container_layout).adapter
        appbar(R.id.appBarFullProfile).removeOnOffsetChangedListener(onOffsetChangedListener)
        super.onDestroyView(view)
    }

}


internal class ProfileStatsPagerAdapter(private val links: List<ProfileStatsLinks.YearLink>,
                                        containerId: Int,
                                        host: BaseParentController<*>) : RouterPagerAdapter(containerId, host) {

    private val linkParcelizer = LinkParcelizer()

    private val parcelableLinks = links.map { linkParcelizer.parcelize(it.link) }

    override fun getCount(): Int = parcelableLinks.size

    override fun getPageTitle(position: Int): CharSequence? = links[position].year.toString()

    override fun makeRouterName(position: Int): String = parcelableLinks[position].containerTag()


}
