package com.crskdev.fcycling.presentation.screen.main

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crskdev.fcycling.R
import com.crskdev.fcycling.loadAvatar
import com.crskdev.fcycling.loadFlag
import com.crskdev.fcycling.util.BaseGlideController
import com.crskdev.fcycling.util.HasGlideSupport
import fcycling.crskdev.com.data.parse.profile.MinimalProfileModel
import kotlin.reflect.KClass

/**
 * Created by Cristian Pela on 08.05.2018.
 */
class MenuLoggedProfileController(args: Bundle) : BaseGlideController<MinimalProfileModel>(args), HasGlideSupport {

    private var header: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return super.onCreateView(inflater, container).apply {
            header = v<NavigationView>(R.id.nav_view, container.parent as View)
                    .inflateHeaderView(R.layout.menu_logged_profile_header)
        }
    }

    override fun onModelAttached(model: MinimalProfileModel) {
        txt(R.id.textMenuHUsername, header).text = model.minimalProfile.userName
        txt(R.id.textMenuHManagerName, header).text = model.minimalProfile.managerName
        txt(R.id.textMenuHSeasonPosition, header).text = model.seasonPosition.toString()
        img(R.id.imageMenuHAvatar, header).apply {
            glide.loadAvatar(resources, model.minimalProfile.avatarUrl)
                    .into(this)
        }
        img(R.id.imageMenuHFlag, header).apply {
            glide.loadFlag(resources, model.minimalProfile.flagUrl)
                    .into(this)
        }
    }

    override fun onDestroyView(view: View) {
        header = null
        super.onDestroyView(view)
    }

    override fun modelClass(): KClass<MinimalProfileModel> = MinimalProfileModel::class

}