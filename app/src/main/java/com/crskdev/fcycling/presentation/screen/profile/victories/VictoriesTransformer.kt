package com.crskdev.fcycling.presentation.screen.profile.victories

import fcycling.crskdev.com.data.parse.profile.ProfileStatsVictories

/**
 * Created by Cristian Pela on 01.06.2018.
 */
internal object VictoriesTransformer {

    fun transform(raceVictories: List<ProfileStatsVictories.Victory>,
                  stageVictories: List<ProfileStatsVictories.Victory>,
                  raceHeaderTitle: String,
                  stageHeaderTitle: String): List<Item> {

        val items = mutableListOf<Item>(Item.Header(raceHeaderTitle))
        items.addAll(raceVictories.map {
            Item.Race(it.raceLink.about.name, it.raceLink.about.flagUrl, it.raceLink.about.date ?: "")
        })
        items.add(Item.Header(stageHeaderTitle))
        stageVictories.groupBy { it.times }.forEach{
            items.add(Item.StageTimes(it.key!!, it.value.size))
            items.addAll(it.value.map {
                Item.Stage(it.raceLink.about.name, it.raceLink.about.flagUrl)
            })
        }
        return items

    }

}