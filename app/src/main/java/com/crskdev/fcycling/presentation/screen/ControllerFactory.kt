package com.crskdev.fcycling.presentation.screen

import android.os.Bundle
import fcycling.crskdev.com.domain.screen.Screen


class ControllerFactory(private val providers: Map<Screen, ControllerProvider>) {

    fun get(screen: Screen, args: Bundle): BaseController<*> =
            providers[screen]?.create(args)
                    ?: throw Error("Controller for screen id $screen could not be created")

    interface ControllerProvider {
        fun create(args: Bundle): BaseController<*>
    }
}