package com.crskdev.fcycling.presentation.screen.profile.victories

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.arasthel.spannedgridlayoutmanager.SpannedGridLayoutManager
import com.crskdev.fcycling.R
import com.crskdev.fcycling.loadCompoundDrawableLeft
import com.crskdev.fcycling.util.BaseGlideController
import com.crskdev.fcycling.util.EqualSpacingDecoration
import com.crskdev.fcycling.util.kast
import fcycling.crskdev.com.data.parse.profile.ProfileStatsVictoriesModel
import kotlin.reflect.KClass

/**
 * Created by Cristian Pela on 01.06.2018.
 */
class ProfileStatsVictoriesController(args: Bundle) : BaseGlideController<ProfileStatsVictoriesModel>(args) {

    override fun onAttach(view: View, isStateRestored: Boolean) {
        txt(R.id.textHeaderMinProfileTitle).text = activity?.getString(R.string.lbl_profile_stats_victories_title)
        imgBtn(R.id.btnHeaderMinProfileBack).setOnClickListener {
            router.popCurrentController()
        }
        recycler(R.id.recyclerProfileStatsVictories).apply {
            val maxSpans = if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) 12 else 20
            layoutManager = SpannedGridLayoutManager(SpannedGridLayoutManager.Orientation.VERTICAL, maxSpans).apply {
                itemOrderIsStable = true
            }
            addItemDecoration(EqualSpacingDecoration(8, EqualSpacingDecoration.VERTICAL))
            val racesHeaderTitle = resources.getString(R.string.lbl_profile_stats_victories_header_races_title)
            val stagesHeaderTitle = resources.getString(R.string.lbl_profile_stats_victories_header_stages_title)
            adapter = ProfileStatsVictoriesAdapter(LayoutInflater.from(context),
                    racesHeaderTitle,
                    stagesHeaderTitle,
                    glide,
                    maxSpans)
        }
    }

    override fun onModelAttached(model: ProfileStatsVictoriesModel) {
        txt(R.id.textHeaderMinProfileTeamname).apply {
            text = model.minimalProfile.managerName
            model.minimalProfile.flagUrl?.let {
                glide.loadCompoundDrawableLeft(this, it)
            }
        }
        recycler(R.id.recyclerProfileStatsVictories)
                .adapter
                .kast<ProfileStatsVictoriesAdapter>()
                .setList(model.victories.raceVictories, model.victories.stageVictories)
        recycler(R.id.recyclerProfileStatsVictories).layoutManager.scrollToPosition(1)
    }

    override fun modelClass(): KClass<ProfileStatsVictoriesModel> = ProfileStatsVictoriesModel::class
}




