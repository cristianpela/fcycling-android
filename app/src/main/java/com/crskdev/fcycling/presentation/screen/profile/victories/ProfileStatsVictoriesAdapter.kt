package com.crskdev.fcycling.presentation.screen.profile.victories

import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.arasthel.spannedgridlayoutmanager.SpanLayoutParams
import com.arasthel.spannedgridlayoutmanager.SpanSize
import com.bumptech.glide.request.target.ViewTarget
import com.crskdev.fcycling.GlideRequests
import com.crskdev.fcycling.R
import com.crskdev.fcycling.loadCompoundDrawableLeft
import com.crskdev.fcycling.util.kast
import fcycling.crskdev.com.data.parse.profile.ProfileStatsVictories
import kotlin.math.roundToInt

internal class ProfileStatsVictoriesAdapter(private val layoutInflater: LayoutInflater,
                                            private val raceHeaderTitle: String,
                                            private val stageHeaderTitle: String,
                                            private val glide: GlideRequests,
                                            private val maxSpans: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        internal const val TYPE_HEADER = 0
        internal const val TYPE_RACE = 1
        internal const val TYPE_STAGE = 2
        internal const val TYPE_STAGE_TIMES = 3
    }

    private val items = mutableListOf<Item>()

    fun setList(raceVictories: List<ProfileStatsVictories.Victory>, stageVictories: List<ProfileStatsVictories.Victory>) {
        if (items.isEmpty()) {
            items.addAll(VictoriesTransformer.transform(raceVictories, stageVictories, raceHeaderTitle, stageHeaderTitle))
            notifyDataSetChanged()
        }

    }

    override fun getItemViewType(position: Int): Int = items[position].type

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layout = when (viewType) {
            TYPE_HEADER -> R.layout.item_profile_stats_victories_header
            TYPE_RACE -> R.layout.item_profile_stats_victories_race
            TYPE_STAGE -> R.layout.item_profile_stats_victories_stage
            TYPE_STAGE_TIMES -> R.layout.item_profile_stats_victories_stage_times
            else -> throw Error("Can't map layout. Invalid type $viewType")
        }
        val view = layoutInflater.inflate(layout, parent, false)
        return when (viewType) {
            TYPE_HEADER -> HeaderVH(view)
            TYPE_RACE -> RaceVH(view)
            TYPE_STAGE -> StageVH(view)
            TYPE_STAGE_TIMES -> StageTimesVH(view)
            else -> throw Error("Can't map view holder. Invalid type $viewType")
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        val spanWidth = (item.spanInfo.widthProportion * maxSpans).roundToInt()
        holder.itemView.layoutParams = SpanLayoutParams(SpanSize(spanWidth, item.spanInfo.height))
        when (holder) {
            is HeaderVH -> item.kast<Item.Header>().apply {
                holder.textHeader.text = title
            }
            is RaceVH -> item.kast<Item.Race>().apply {
                holder.bind(glide, name, flagUrl, years)
            }
            is StageVH -> item.kast<Item.Stage>().apply {
                holder.bind(glide, name, flagUrl)
            }
            is StageTimesVH -> item.kast<Item.StageTimes>().apply {
                holder.textTimes.text = times
            }
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        clearTargets(holder)
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        clearTargets(holder)
    }

    private fun clearTargets(holder: RecyclerView.ViewHolder) {
        when (holder) {
            is StageVH -> glide.clear(holder.target).let { holder.target = null }
            is RaceVH -> glide.clear(holder.target).let { holder.target = null }
            else -> Unit
        }
    }
}

internal class HeaderVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val textHeader = itemView.findViewById<TextView>(R.id.textProfileStatsVictoriesHeader)!!
}

internal class RaceVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var target: ViewTarget<TextView, Drawable>? = null

    fun bind(glide: GlideRequests, name: String, flagUrl: String, years: String) {
        itemView.findViewById<TextView>(R.id.textProfileStatsVictoriesRace).apply {
            text = name
            //todo weird dimensions not corresponding to what is in dimens.xml
            //val w = itemView.resources.getDimension(R.dimen.flag_dimen_width_small)
            // val h = itemView.resources.getDimension(R.dimen.flag_dimen_height_small)
            target = glide.loadCompoundDrawableLeft(this, flagUrl, 15, 10)
        }
        itemView.findViewById<TextView>(R.id.textProfileStatsVictoriesRaceYears).text = years
    }
}

internal class StageTimesVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val textTimes = itemView.findViewById<TextView>(R.id.textProfileStatsVictoriesStageTimes)!!
}

internal class StageVH(itemView: View) : RecyclerView.ViewHolder(itemView) {


    var target: ViewTarget<TextView, Drawable>? = null

    fun bind(glide: GlideRequests, name: String, flagUrl: String) {
        itemView.findViewById<TextView>(R.id.textProfileStatsVictoriesStage).apply {
            text = name
            //todo weird dimensions not corresponding to what is in dimens.xml
            // val w = itemView.resources.getDimension(R.dimen.flag_dimen_width_small)
            // val h = itemView.resources.getDimension(R.dimen.flag_dimen_height_small)
            target = glide.loadCompoundDrawableLeft(this, flagUrl, 15, 10)
        }
    }
}

sealed class Item(val type: Int, val spanInfo: SpanInfo = SpanInfo(1.0, 1)) {
    data class Header(val title: String) : Item(ProfileStatsVictoriesAdapter.TYPE_HEADER, SpanInfo(1.0))
    data class Race(val name: String, val flagUrl: String, val years: String) : Item(ProfileStatsVictoriesAdapter.TYPE_RACE, SpanInfo(1.0))
    data class StageTimes(val times: String, private val verticalSpan: Int) : Item(ProfileStatsVictoriesAdapter.TYPE_STAGE_TIMES, SpanInfo(.35, verticalSpan))
    data class Stage(val name: String, val flagUrl: String) : Item(ProfileStatsVictoriesAdapter.TYPE_STAGE, SpanInfo(.65))
}

/**
 * Vertical Orientation Spans Info
 */
internal data class SpanInfo(val widthProportion: Double, val height: Int = 1) {
    init {
        assert(widthProportion in 0..1){"Width proportion must be between 0 and 1. Current $widthProportion"}
    }
}