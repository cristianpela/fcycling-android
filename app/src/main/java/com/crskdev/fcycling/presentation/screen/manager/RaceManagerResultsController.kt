package com.crskdev.fcycling.presentation.screen.manager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.crskdev.fcycling.R
import com.crskdev.fcycling.loadCompoundDrawableLeft
import com.crskdev.fcycling.util.BaseGlideController
import com.crskdev.fcycling.util.EqualSpacingDecoration
import com.crskdev.fcycling.util.kast
import fcycling.crskdev.com.data.parse.manager.ManagerResultsModel
import fcycling.crskdev.com.domain.screen.Link
import kotlin.reflect.KClass

/**
 * Created by Cristian Pela on 11.06.2018.
 */
class RaceManagerResultsController(args: Bundle) : BaseGlideController<ManagerResultsModel>(args),
        RaceManagerResultsAdapter.OnLinkClicked {

    override fun onRaceManagerResultsAdapterLinkClicked(link: Link) {
        dispatchLink(link)
    }

    override fun onAttach(view: View, isStateRestored: Boolean) {
        toolbar(R.id.toolbarHeaderTitle).apply {
            inflateMenu(R.menu.screen_title_header_menu)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_close -> router.popController(this@RaceManagerResultsController)
                    else -> true
                }
            }
        }
        recycler(R.id.recyclerRaceManagerResults).apply {
            addItemDecoration(EqualSpacingDecoration(4, EqualSpacingDecoration.HORIZONTAL))
            adapter = RaceManagerResultsAdapter(this@RaceManagerResultsController, LayoutInflater.from(context), glide)
        }
    }

    override fun onModelAttached(model: ManagerResultsModel) {
        txt(R.id.textHeaderTitle).apply {
            val about = model.raceLink.about
            text = about.date + " " + about.name
            glide.loadCompoundDrawableLeft(this, about.flagUrl)
        }
        recycler(R.id.recyclerRaceManagerResults)
                .adapter.kast<RaceManagerResultsAdapter>()
                .setList(model.results)

    }

    override fun modelClass(): KClass<ManagerResultsModel> = ManagerResultsModel::class
}