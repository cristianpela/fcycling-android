package com.crskdev.fcycling.presentation.screen

import android.os.Bundle
import android.support.annotation.LayoutRes
import fcycling.crskdev.com.domain.parse.ParserFactory
import fcycling.crskdev.com.domain.parse.ResponseParser
import fcycling.crskdev.com.domain.screen.Screen

/**
 * Created by Cristian Pela on 08.05.2018.
 */
class ScreenConfig(val controllerFactory: ControllerFactory,
                   val parserFactory: ParserFactory,
                   val screenToLayoutMapper: Map<Screen, Int>)

class ScreenConfigEntry(
        var controller: ((args: Bundle) -> BaseController<*>)? = null,
        var parser: ResponseParser<*>? = null,
        @LayoutRes var layout: Int? = null)

class ScreenConfigBuilder internal constructor() {

    private val entries = mutableMapOf<Screen, ScreenConfigEntry>()

    fun add(screen: Screen, entry: ScreenConfigEntry.() -> Unit) {
        entries[screen] = ScreenConfigEntry().apply(entry).apply {
            assert(controller != null) {
                "For screen id $screen, Controller Provider is not provided"
            }
            assert(layout != null) {
                "For screen id $screen, Layout is not provided"
            }
        }
    }

    internal fun create(): ScreenConfig {
        val controllers = mutableMapOf<Screen, ControllerFactory.ControllerProvider>()
        val parsers = mutableMapOf<Screen, ResponseParser<*>>()
        val layouts = mutableMapOf<Screen, Int>()

        entries.keys.forEach {
            val e = entries[it]!!
            controllers[it] = object : ControllerFactory.ControllerProvider {
                override fun create(args: Bundle): BaseController<*> =
                        e.controller!!.invoke(args)
            }
            e.parser?.let { p -> parsers[it] = p }
            layouts[it] = e.layout!!
        }

        return ScreenConfig(ControllerFactory(controllers), ParserFactory(parsers), layouts)
    }

}


fun screenConfig(builder: ScreenConfigBuilder.() -> Unit): ScreenConfig =
        ScreenConfigBuilder().apply(builder).create()

