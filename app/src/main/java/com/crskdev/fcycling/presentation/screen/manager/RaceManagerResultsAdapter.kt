package com.crskdev.fcycling.presentation.screen.manager

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.request.target.ViewTarget
import com.crskdev.fcycling.GlideRequests
import com.crskdev.fcycling.R
import com.crskdev.fcycling.loadCompoundDrawableBottom
import com.crskdev.fcycling.util.kast
import fcycling.crskdev.com.data.parse.manager.ManagerResult
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.profileLink

/**
 * Created by Cristian Pela on 11.06.2018.
 */
class RaceManagerResultsAdapter(
        private val onLinkClicked: RaceManagerResultsAdapter.OnLinkClicked,
        private val inflater: LayoutInflater,
        private val glide: GlideRequests
) : RecyclerView.Adapter<RaceManagerResultsVH>() {

    private val items = mutableListOf<ManagerResult>()

    fun setList(newItems: List<ManagerResult>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RaceManagerResultsVH =
            inflater.inflate(R.layout.item_race_manager_results, parent, false).let {
                RaceManagerResultsVH(it)
            }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RaceManagerResultsVH, position: Int) {
        val item = items[position]
        with(holder) {
            txtRaceManagerResultsPosition.text = item.position
            txtRaceManagerResultsName.apply {
                val profile = item.profile
                text = profile.managerName
                profile.flagUrl?.let {
                    holder.viewTargets.add(
                            glide.loadCompoundDrawableBottom(this, it, 15, 10))
                }
                //TODO make to click to the team selection not profile
//               // setTag(R.id.recyclingTag, item.teamLink)
                setTag(R.id.recyclingTag, profileLink(profile.id).makePop(true))
                setOnClickListener {
                    it.getTag(R.id.recyclingTag)?.kast<Link>()?.let {
                        onLinkClicked.onRaceManagerResultsAdapterLinkClicked(it)
                    }
                }
            }
            txtRaceManagerResultsRacePoints.text = item.racePoints
            txtRaceManagerResultsSeasonPoints.text = item.seasonCompetitionPoints
            txtRaceManagerResultsFcTeam.apply {
                text = item.fcTeamName
                item.fcTeamNationFlag?.let {
                    holder.viewTargets.add(
                            glide.loadCompoundDrawableBottom(this, it, 15, 10))
                }
            }
            txtRaceManagerResultsCaptain.apply {
                text = item.captain.name
                item.captain.nationFlagUrl?.let {
                    holder.viewTargets.add(
                            glide.loadCompoundDrawableBottom(this, it, 15, 10))
                }
            }
        }
    }

    override fun onViewRecycled(holder: RaceManagerResultsVH) {
        holder.viewTargets.forEach {
            glide.clear(it)
        }
        holder.viewTargets.clear()
        holder.txtRaceManagerResultsName.setCompoundDrawables(null, null, null, null)
        holder.txtRaceManagerResultsFcTeam.setCompoundDrawables(null, null, null, null)
        holder.txtRaceManagerResultsCaptain.setCompoundDrawables(null, null, null, null)
    }

    interface OnLinkClicked {
        fun onRaceManagerResultsAdapterLinkClicked(link: Link)
    }
}


class RaceManagerResultsVH(view: View) : RecyclerView.ViewHolder(view) {
    val viewTargets = mutableListOf<ViewTarget<*, *>>()
    val txtRaceManagerResultsPosition = view.findViewById<TextView>(R.id.txtRaceManagerResultsPosition)!!
    val txtRaceManagerResultsName = view.findViewById<TextView>(R.id.txtRaceManagerResultsName)!!
    val txtRaceManagerResultsSeasonPoints = view.findViewById<TextView>(R.id.txtRaceManagerResultsSeasonPoints)!!
    val txtRaceManagerResultsRacePoints = view.findViewById<TextView>(R.id.txtRaceManagerResultsRacePoints)!!
    val txtRaceManagerResultsFcTeam = view.findViewById<TextView>(R.id.txtRaceManagerResultsFcTeam)!!
    val txtRaceManagerResultsCaptain = view.findViewById<TextView>(R.id.txtRaceManagerResultsCaptain)!!
}