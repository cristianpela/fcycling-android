package com.crskdev.fcycling.presentation.screen.profile

import android.content.res.Resources
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.crskdev.fcycling.GlideRequests
import com.crskdev.fcycling.R
import com.crskdev.fcycling.loadCompoundDrawableLeft
import com.crskdev.fcycling.util.BaseGlideController
import com.crskdev.fcycling.util.EqualSpacingDecoration
import com.crskdev.fcycling.util.kast
import fcycling.crskdev.com.data.parse.profile.*
import fcycling.crskdev.com.data.parse.rider.MostUsedCaptain
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.linkProfileInfoStatsManagerRacesScreen
import kotlin.reflect.KClass

/**
 * Created by Cristian Pela on 17.05.2018.
 */
class ProfileStatsController(args: Bundle) : BaseGlideController<ProfileStatsModel>(args),
        ManagerRacesAdapter.OnLinkClicked {
    override fun modelClass(): KClass<ProfileStatsModel> = ProfileStatsModel::class

    override fun onAttach(view: View, isStateRestored: Boolean) {
        val inflater = LayoutInflater.from(activity)

        recycler(R.id.recyclerProfileStatsStandingsSeason).apply {
            isNestedScrollingEnabled = false
            adapter = SeasonStandingsAdapter(inflater)
        }
        recycler(R.id.recyclerProfileStatsStandingsMonthly).apply {
            isNestedScrollingEnabled = false
            adapter = MonthlyStandingsAdapter(inflater)
        }
        recycler(R.id.recyclerProfileStatsOngoingManagers).apply {
            isNestedScrollingEnabled = false
            adapter = ManagerRacesAdapter(this@ProfileStatsController, inflater, glide)
            addItemDecoration(EqualSpacingDecoration(2, EqualSpacingDecoration.HORIZONTAL))
        }
        recycler(R.id.recyclerProfileStatsMostUsedCaptains).apply {
            isNestedScrollingEnabled = false
            adapter = MostUsedCaptainsAdapter(inflater, glide)
            val spacing = 4
            addItemDecoration(EqualSpacingDecoration(spacing, EqualSpacingDecoration.GRID))
            val screenWidth = Resources.getSystem().displayMetrics.widthPixels
            val captainCardWidth = resources.getDimensionPixelSize(R.dimen.most_used_captain_dimen_width)
            val spacingGrid = 2 * spacing.dpToPx()
            val spanCount = screenWidth / (captainCardWidth + spacingGrid).toFloat()
            layoutManager.kast<GridLayoutManager>().spanCount = Math.round(spanCount)
        }
    }


    override fun onModelAttached(model: ProfileStatsModel) {
        model.stats?.let { stats ->
            recycler(R.id.recyclerProfileStatsStandingsSeason)
                    .adapter
                    .kast<SeasonStandingsAdapter>()
                    .setList(stats.profileSeasonStandings)
            recycler(R.id.recyclerProfileStatsStandingsMonthly)
                    .adapter
                    .kast<MonthlyStandingsAdapter>()
                    .setList(stats.profileMonthlyStandings)
            if (stats.mostUsedCaptains.isNotEmpty()) {
                recycler(R.id.recyclerProfileStatsMostUsedCaptains)
                        .adapter
                        .kast<MostUsedCaptainsAdapter>()
                        .setList(stats.mostUsedCaptains)
                v<View>(R.id.cardProfileStatsMostUsedCaptains).visibility = View.VISIBLE
            } else {
                v<View>(R.id.cardProfileStatsMostUsedCaptains).visibility = View.GONE
            }

            val ongoingManagerRaces = stats.managerRaces.filter { it.isOngoing }
            if (ongoingManagerRaces.isNotEmpty()) {
                v<View>(R.id.cardProfileStatsOngoingManagers).visibility = View.VISIBLE
                recycler(R.id.recyclerProfileStatsOngoingManagers)
                        .adapter
                        .kast<ManagerRacesAdapter>()
                        .setList(ongoingManagerRaces)
            } else {
                v<View>(R.id.cardProfileStatsOngoingManagers).visibility = View.GONE
            }
            imgBtn(R.id.btnProfileStatsFinishedManagers).setOnClickListener {
                dispatchLink(linkProfileInfoStatsManagerRacesScreen(model.minimalProfile.id,
                        model.year,
                        model.gender === CompetitionGender.WOMEN))
            }
        }
    }

    override fun onManagerRacesAdapterLinkClicked(link: Link) {
        dispatchLink(link)
    }

    override fun onEmptyModelReceivedFromSkippedRequest() {
        dispatchLink(getControllerLink().skipRequest(false))
    }
}

class SeasonStandingsAdapter(private val inflater: LayoutInflater)
    : RecyclerView.Adapter<SeasonStandingsAdapter.SeasonStandingsVH>() {

    private var list = mutableListOf<SeasonStanding>()

    fun setList(newList: List<SeasonStanding>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeasonStandingsVH =
            inflater
                    .inflate(R.layout.item_profile_stats_standings_season_layout, parent, false)
                    .let { SeasonStandingsVH(it) }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: SeasonStandingsVH, position: Int) {

        val model = list[position]

        val background = (if (position.rem(2) == 0)
            R.color.colorRowRedOdd else
            R.color.colorWhite).let {
            val context = holder.itemView.context
            ContextCompat.getColor(context, it)
        }

        holder.itemView.setBackgroundColor(background)
        holder.txtPosition.apply {
            text = context.getString(R.string.lbl_item_profile_stats_standings_season_no, model.no)
            // setTextColor(foreground)
        }
        holder.txtType.apply {
            text = model.type
            // setTextColor(foreground)
        }

    }


    class SeasonStandingsVH(view: View) : RecyclerView.ViewHolder(view) {
        val txtPosition = view.findViewById<TextView>(R.id.textItemSeasonStandingPosition)!!
        val txtType = view.findViewById<TextView>(R.id.textItemSeasonStandingType)!!
    }
}


class MonthlyStandingsAdapter(private val inflater: LayoutInflater) : RecyclerView.Adapter<MonthlyStandingsAdapter.MonthlyStandingsVH>() {

    private val items = mutableListOf<MonthlyStanding?>().apply { add(HEADER_ITEM) }

    fun setList(newItems: List<MonthlyStanding>) {
        if (items.size > 1) {
            items.removeAll { it != HEADER_ITEM }
        }
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    companion object {
        private val HEADER_ITEM: MonthlyStanding? = null
        private const val HEADER_TYPE = 0
        private const val ITEM_TYPE = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MonthlyStandingsVH =
            (if (viewType == HEADER_TYPE)
                R.layout.item_profile_stats_standings_monthly_header
            else
                R.layout.item_profile_stats_standings_monthly)
                    .let {
                        val view = inflater.inflate(it, parent, false)
                        MonthlyStandingsVH(view)
                    }


    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: MonthlyStandingsVH, position: Int) {
        if (position > 0) {
            val item = items[position]
            val background = (if ((position + 1).rem(2) == 0)
                R.color.colorRowGreenOdd else
                R.color.colorWhite).let {
                val context = holder.itemView.context
                ContextCompat.getColor(context, it)
            }

            with(holder) {
                textMonth.text = item?.month
                textPosition.text = item?.position?.toString()
                textPoints.text = item?.points?.toString()
                textEntries.text = item?.entries
                textPercent.text = item?.percent
                itemView.setBackgroundColor(background)
            }
        }
    }

    override fun getItemViewType(position: Int): Int = if (position == 0) HEADER_TYPE else ITEM_TYPE

    class MonthlyStandingsVH(view: View) : RecyclerView.ViewHolder(view) {
        val textMonth = view.findViewById<TextView>(R.id.textItemProfileStatsMonthlyMonth)!!
        val textPosition = view.findViewById<TextView>(R.id.textItemProfileStatsMonthlyPosition)!!
        val textPoints = view.findViewById<TextView>(R.id.textItemProfileStatsMonthlyPoints)!!
        val textEntries = view.findViewById<TextView>(R.id.textItemProfileStatsMonthlyEntries)!!
        val textPercent = view.findViewById<TextView>(R.id.textItemProfileStatsMonthlyPercent)!!
    }


}


class ManagerRacesAdapter(private val onLinkClicked: OnLinkClicked,
                          private val inflater: LayoutInflater,
                          private val glide: GlideRequests) : RecyclerView.Adapter<ManagerRacesAdapter.ManagerRacesVH>() {

    private val items = mutableListOf<ManagerRace>()

    interface OnLinkClicked {
        fun onManagerRacesAdapterLinkClicked(link: Link)
    }

    fun setList(newItems: List<ManagerRace>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ManagerRacesVH =
            inflater
                    .inflate(R.layout.item_profile_stats_manager_race, parent, false)
                    .let { ManagerRacesVH(it) }


    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ManagerRacesVH, position: Int) {
        val item = items[position]
        with(holder) {
            val raceInfo = item.resultRaceInfo
            textType.text = raceInfo.classification
            textDate.text = raceInfo.link.about.date
            textName.apply name@{
                text = raceInfo.link.about.name
                //todo weird dimensions not corresponding to what is in dimens.xml
                glide.loadCompoundDrawableLeft(this, raceInfo.link.about.flagUrl, 15, 10)
            }
            btnProfileStatsManagerRaceManager.apply {
                tag = item.managerPage
                setOnClickListener {
                    it.tag?.kast<Link>()?.let {
                        onLinkClicked.onManagerRacesAdapterLinkClicked(it)
                    }
                }
            }
            textCaptain.apply captain@{
                item.captain?.let {
                    text = it.name
                    //todo weird dimensions not corresponding to what is in dimens.xml
                    it.nationFlagUrl?.let { flag ->
                        glide.loadCompoundDrawableLeft(this, flag, 15, 10)
                    }
                }
            }
            textPoints.text = if (item.points != -1) "${item.points}.pts" else "-"
            textPosition.text = if (item.position != -1)
                "${item.position} of ${item.numberOfParticipants}" else "-"
            viewPerformanceIndicator.setBackgroundColor(
                    when {
                        item.performancePercent in 1..25 -> R.color.colorRowRedOdd
                        item.performancePercent in 50..74 -> R.color.colorRowOrangeOdd
                        item.performancePercent in 75..100 -> R.color.colorRowGreenOdd
                        else -> R.color.colorGray50
                    }.let {
                        ContextCompat.getColor(itemView.context, it)
                    }
            )
        }
    }


    class ManagerRacesVH(view: View) : RecyclerView.ViewHolder(view) {
        val textType = view.findViewById<TextView>(R.id.textProfileStatsManagerRaceType)!!
        val textDate = view.findViewById<TextView>(R.id.textProfileStatsManagerRaceDate)!!
        val textName = view.findViewById<TextView>(R.id.textProfileStatsManagerRace)!!
        val textCaptain = view.findViewById<TextView>(R.id.textProfileStatsManagerRaceCaptain)!!
        val textPosition = view.findViewById<TextView>(R.id.textProfileStatsManagerRacePosition)!!
        val textPoints = view.findViewById<TextView>(R.id.textProfileStatsManagerRacePoints)!!
        val viewPerformanceIndicator = view.findViewById<View>(R.id.viewProfileStatsPerformanceIndicator)!!
        val btnProfileStatsManagerRaceManager = view.findViewById<Button>(R.id.btnProfileStatsManagerRaceManager)!!
    }
}


class MostUsedCaptainsAdapter(private val layoutInflater: LayoutInflater, private val glide: GlideRequests) : RecyclerView.Adapter<MostUsedCaptainsAdapter.MostUsedCaptainsVH>() {

    private val items = mutableListOf<MostUsedCaptain>()

    fun setList(newItems: List<MostUsedCaptain>) {
        if (items.isNotEmpty())
            items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MostUsedCaptainsVH =
            layoutInflater.inflate(R.layout.item_profile_stats_most_used_captains_layout, parent, false)
                    .let { MostUsedCaptainsVH(it) }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: MostUsedCaptainsVH, position: Int) {
        items[position].let {
            val name = it.minimalInfoRider.name
            val times = it.times
            holder.textMostUsedCaptain.text = holder.itemView.resources
                    .getString(R.string.lbl_profile_stats_most_used_captains_names_and_times, name, times)
            it.minimalInfoRider.nationFlagUrl?.let { flag ->
                glide.loadCompoundDrawableLeft(holder.textMostUsedCaptain, flag, 15, 10)
            }
        }
    }


    class MostUsedCaptainsVH(view: View) : RecyclerView.ViewHolder(view) {
        val textMostUsedCaptain = view.findViewById<TextView>(R.id.textProfileStatsMostUsedCaptain)!!
    }
}