package com.crskdev.fcycling.presentation.screen.main

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.view.MenuItem
import android.view.View
import com.crskdev.fcycling.R
import com.crskdev.fcycling.presentation.screen.BaseParentController
import com.crskdev.fcycling.util.setAsActionBar
import fcycling.crskdev.com.data.parse.menu.MenuModel
import fcycling.crskdev.com.domain.screen.LOGOUT_LINK_SCREEN
import fcycling.crskdev.com.domain.screen.Link
import kotlin.reflect.KClass


/**
 * Created by Cristian Pela on 03.05.2018.
 */
class MenuController(args: Bundle) : BaseParentController<MenuModel>(args),
        NavigationView.OnNavigationItemSelectedListener {

    init {
        setHasOptionsMenu(true)
    }

    override fun modelClass(): KClass<MenuModel> = MenuModel::class

    override fun onModelAttached(model: MenuModel) {
        model.nextManager?.let {
            txt(R.id.btnMenuNextManager).apply {
                text = resources.getString(R.string.lbl_next_manager_btn, it.name, it.date)
            }
        }
    }

    override fun availableContainers(): IntArray = intArrayOf(
            R.id.main_screen_nav_detail_container,
            R.id.menu_logged_profile_container
    )

    override fun onAttach(view: View, isStateRestored: Boolean) {

        toolbar(R.id.toolbar).setAsActionBar {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
            setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp)
        }

        drawer(R.id.drawer_layout).apply {
            setScrimColor(ContextCompat.getColor(context, R.color.colorNavScrim))

        }

        nav(R.id.nav_view).setNavigationItemSelectedListener(this)

        imgBtn(R.id.btnMenuRefresh).setOnClickListener {
            dispatchLink(Link.REFRESH)
        }

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                android.R.id.home -> {
                    drawer(R.id.drawer_layout).openDrawer(GravityCompat.START)
                    true
                }
                else -> false
            }


    override fun onNavigationItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.action_main_menu_profile -> {
                    dispatchLink(state?.profileLink)
                    true
                }
                R.id.action_main_menu_logout -> {
                    dispatchLink(LOGOUT_LINK_SCREEN)
                    true
                }
                R.id.action_main_menu_exit -> {
                    activity?.finish()
                    true
                }
                R.id.action_main_menu_managers -> {
                    dispatchLink(state?.managersLink)
                    true
                }
                else -> true
            }.apply {
                drawer(R.id.drawer_layout).closeDrawer(GravityCompat.START)
            }


    override fun handleBack(): Boolean {
        return if (drawer(R.id.drawer_layout).isDrawerOpen(GravityCompat.START)) {
            drawer(R.id.drawer_layout).closeDrawer(GravityCompat.START)
            true
        } else
            super.handleBack()
    }
}
