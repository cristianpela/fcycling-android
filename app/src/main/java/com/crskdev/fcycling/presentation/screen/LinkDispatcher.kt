package com.crskdev.fcycling.presentation.screen

import fcycling.crskdev.com.domain.parse.IRequestProcessor
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.ScreenModel
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class LinkDispatcher(requestProcessor: IRequestProcessor) {

    private val linkDispatcher: Subject<Link> = PublishSubject.create()

    private val modelCollector: Subject<ScreenModel> = BehaviorSubject.createDefault(ScreenModel.Init)

    init {
        linkDispatcher.toSerialized()
                .compose(requestProcessor.process())
                .subscribe(modelCollector)
    }

    fun observeScreenModel(): Observable<ScreenModel> = modelCollector

    fun dispatchLink(link: Link) = linkDispatcher.onNext(link)

    fun flush() = modelCollector.onNext(ScreenModel.Idle)

    fun reset() = dispatchLink(Link.INIT)

}