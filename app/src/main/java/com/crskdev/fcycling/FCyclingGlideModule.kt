@file:Suppress("unused")

package com.crskdev.fcycling

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.util.Log
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.target.ViewTarget
import com.bumptech.glide.request.transition.Transition
import com.crskdev.fcycling.util.dpToPx
import com.crskdev.fcycling.util.kast
import java.io.InputStream


/**
 * Created by Cristian Pela on 30.05.2018.
 */
@GlideModule
class FCyclingGlideModule : AppGlideModule() {

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        val factory = OkHttpUrlLoader.Factory(context.applicationContext.kast<FCyclingApplication>().okHttpClient)
        glide.registry.replace(GlideUrl::class.java, InputStream::class.java, factory)
    }

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        if (BuildConfig.DEBUG)
            builder.setLogLevel(Log.INFO)
    }
}

fun GlideRequests.loadAvatar(resources: Resources, url: String?) =
        load(url)
                .override(resources.getDimensionPixelSize(R.dimen.user_avatar_dimen))
                .error(R.drawable.ic_no_avatar)
                .centerCrop()

fun GlideRequests.loadFlag(resources: Resources, url: String?, w: Int? = null, h: Int? = null) =
        asDrawable()
                .load(url)
                .let {
                    val dimens = if (w != null && h != null) {
                        w.dpToPx(resources) to h.dpToPx(resources)
                    } else {
                        resources.getDimensionPixelSize(R.dimen.flag_dimen_width) to resources.getDimensionPixelSize(R.dimen.flag_dimen_height)
                    }
                    it.override(dimens.first, dimens.second)
                }
                .error(R.drawable.ic_nation_flag_placeholder)
                .centerCrop()

private fun GlideRequests.loadCompoundDrawable(where: Int, target: TextView, url: String, w: Int? = null, h: Int? = null): ViewTarget<TextView, Drawable> {
    val resources = target.resources
    val dimens = if (w != null && h != null) {
        w.dpToPx(resources) to h.dpToPx(resources)
    } else {
        resources.getDimensionPixelSize(R.dimen.flag_dimen_width) to resources.getDimensionPixelSize(R.dimen.flag_dimen_height)
    }
    val textViewTarget: ViewTarget<TextView, Drawable> = object : ViewTarget<TextView, Drawable>(target) {
        override fun onLoadFailed(errorDrawable: Drawable?) {
            println("Failed to load")
        }

        override fun onResourceReady(drawable: Drawable, transition: Transition<in Drawable>?) {
            when (where) {
                0 -> view.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
                1 -> view.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null)
                2 -> view.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
                3 -> view.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable)
                else -> throw Error("Could not set compound drawable. Position invalid $where. " +
                        "Must be 0-left, 1-top, 2-right or 3-bottom")
            }
        }

    }.waitForLayout().clearOnDetach()

    return asDrawable()
            .load(url)
            .override(dimens.first, dimens.second)
            .centerCrop()
            .into(textViewTarget)
}


fun GlideRequests.loadCompoundDrawableLeft(target: TextView, url: String, w: Int? = null, h: Int? = null) =
        loadCompoundDrawable(0, target, url, w, h)

fun GlideRequests.loadCompoundDrawableTop(target: TextView, url: String, w: Int? = null, h: Int? = null) =
        loadCompoundDrawable(1, target, url, w, h)

fun GlideRequests.loadCompoundDrawableRight(target: TextView, url: String, w: Int? = null, h: Int? = null) =
        loadCompoundDrawable(2, target, url, w, h)

fun GlideRequests.loadCompoundDrawableBottom(target: TextView, url: String, w: Int? = null, h: Int? = null) =
        loadCompoundDrawable(3, target, url, w, h)


