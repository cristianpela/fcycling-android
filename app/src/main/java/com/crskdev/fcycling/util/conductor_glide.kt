@file:Suppress("unused")

package com.crskdev.fcycling.util

import android.os.Bundle
import android.view.View
import com.bluelinelabs.conductor.Controller
import com.bumptech.glide.RequestManager
import com.bumptech.glide.manager.Lifecycle
import com.bumptech.glide.manager.LifecycleListener
import com.bumptech.glide.manager.RequestManagerTreeNode
import com.bumptech.glide.util.Util
import com.crskdev.fcycling.GlideApp
import com.crskdev.fcycling.GlideRequests
import com.crskdev.fcycling.presentation.screen.BaseController
import com.crskdev.fcycling.presentation.screen.BaseParentController
import fcycling.crskdev.com.domain.parse.Model
import java.util.*

/**
 * Created by Cristian Pela on 31.05.2018.
 *
 * [Based on](https://gist.github.com/sevar83/7b5e151233481b629e3d1fb1be7327dc)
 */


/**
 * Must be implemented by [com.bluelinelabs.conductor.Controller]s to have controller-scoped Glide
 * resource management (image request pausing, cancelation, cleanup, etc.)
 */
interface HasGlideSupport {
    val glideSupport: GlideSupport
}


/**
 * A [com.bumptech.glide.manager.Lifecycle] implementation for tracking and notifying
 * listeners of [com.bluelinelabs.conductor.Controller] lifecycle events.
 */
class ControllerLifecycle : Lifecycle {

    private val lifecycleListeners = Collections.newSetFromMap(WeakHashMap<LifecycleListener, Boolean>())
    private var isStarted: Boolean = false
    private var isDestroyed: Boolean = false

    /**
     * Adds the given listener to the list of listeners to be notified on each lifecycle event.
     *
     *  The latest lifecycle event will be called on the given listener synchronously in this
     * method. If the activity or fragment is stopped, [LifecycleListener.onStop]} will be
     * called, and same for onStart and onDestroy.
     *
     *  Note - [com.bumptech.glide.manager.LifecycleListener]s that are added more than once
     * will have their lifecycle methods called more than once. It is the caller's responsibility to
     * avoid adding listeners multiple times.
     */
    override fun addListener(listener: LifecycleListener) {
        lifecycleListeners.add(listener)

        when {
            isDestroyed -> listener.onDestroy()
            isStarted -> listener.onStart()
            else -> listener.onStop()
        }
    }

    override fun removeListener(listener: LifecycleListener) {
        lifecycleListeners.remove(listener)
    }

    fun onStart() {
        isStarted = true
        for (lifecycleListener in Util.getSnapshot(lifecycleListeners)) {
            lifecycleListener.onStart()
        }
    }

    fun onStop() {
        isStarted = false
        for (lifecycleListener in Util.getSnapshot(lifecycleListeners)) {
            lifecycleListener.onStop()
        }
    }

    fun onDestroy() {
        isDestroyed = true
        for (lifecycleListener in Util.getSnapshot(lifecycleListeners)) {
            lifecycleListener.onDestroy()
        }
    }
}

/**
 * Helper which adapts controller's lifecycle with glide manager's lifecycle.
 * Note: this is not taking in consideration transitions
 */
class GlideSupport(private val controller: Controller) : RequestManagerTreeNode {

    private var lifecycle: ControllerLifecycle? = null

    internal var requestManager: GlideRequests? = null
        private set

    init {
        controller.addLifecycleListener(object : Controller.LifecycleListener() {

            override fun preCreateView(controller: Controller) {
                if (requestManager == null) {
                    controller.activity?.let {
                        lifecycle = ControllerLifecycle()
                        requestManager = GlideRequests(GlideApp.get(it),
                                lifecycle as Lifecycle,
                                this@GlideSupport,
                                it)
                    }
                }
            }

            override fun postAttach(controller: Controller, view: View) {
                lifecycle?.onStart()
            }

            override fun postDetach(controller: Controller, view: View) {
                lifecycle?.onStop()
            }

            override fun postDestroy(controller: Controller) {
                destroyGlide()
            }

            private fun destroyGlide() {
                requestManager?.onDestroy()
                lifecycle?.onDestroy()
                lifecycle = null
                requestManager = null
            }
        })
    }

    override fun getDescendants(): Set<RequestManager> = collectRequestManagers(controller)

    /**
     * Recursively gathers the [RequestManager]s of a given [Controller] and all its child controllers.
     * The [Controller]s in the hierarchy must implement [HasGlideSupport] in order for their
     * request managers to be collected.
     */
    private fun collectRequestManagers(controller: Controller,
                                       collected: MutableSet<RequestManager> = HashSet()
    ): Set<RequestManager> {

        if (!controller.isDestroyed && !controller.isBeingDestroyed) {
            if (controller is HasGlideSupport) {
                controller.glideSupport.requestManager?.let {
                    collected.add(it)
                }
            }

            controller.childRouters
                    .flatMap { childRouter -> childRouter.backstack }
                    .map { routerTransaction -> routerTransaction.controller() }
                    .forEach { collectRequestManagers(it, collected) }
        }

        return collected
    }
}

object GlideConductor {

    /**
     * gets glide's request manager adapted with controller's lifecycle,
     * following glide's "with" idiom
     */
    fun with(glideSupport: GlideSupport): GlideRequests = glideSupport.requestManager
            ?: throw UninitializedPropertyAccessException("requestManager not yet initialized for the given controller")
}


abstract class BaseGlideController<M : Model>(args: Bundle) : BaseController<M>(args), HasGlideSupport {

    protected val glide by lazy {
        GlideConductor.with(glideSupport)
    }

    @Suppress("LeakingThis")
    override val glideSupport: GlideSupport = GlideSupport(this)
}

abstract class BaseParenGlideController<M : Model>(args: Bundle) : BaseParentController<M>(args), HasGlideSupport {

    protected val glide by lazy {
        GlideConductor.with(glideSupport)
    }

    @Suppress("LeakingThis")
    override val glideSupport: GlideSupport = GlideSupport(this)

}