package com.crskdev.fcycling.util

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Resources
import android.support.annotation.DimenRes
import android.support.design.widget.TabLayout
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.systemService
import timber.log.Timber
import kotlin.math.roundToInt


inline fun <reified V> Any.kast() = this as V

fun Activity.hideSoftInputKeyboard(focusingView: View? = null) {
    val imm = systemService<InputMethodManager>()
    val focusedView = focusingView ?: this.currentFocus
    focusedView?.let {
        imm.hideSoftInputFromWindow(it.windowToken, 0)
    } ?: Timber.w("Can't hide the soft-keyboard, because there is no view holding the focus")
}

@PublishedApi
internal fun Context.scanForActivity(): AppCompatActivity? {
    fun scanForActivityRec(context: Context): AppCompatActivity? {
        return when (context) {
            is AppCompatActivity -> return context
            is ContextWrapper -> return scanForActivityRec(context.baseContext)
            else -> null
        }
    }
    return scanForActivityRec(this)
}


fun View.application(): Application = context.applicationContext as Application


fun Int.toPx(resources: Resources, unit: Int): Float =
        TypedValue.applyDimension(unit, this.toFloat(), resources.displayMetrics)

fun Int.dpToPx(resources: Resources): Int = toPx(resources, TypedValue.COMPLEX_UNIT_DIP).roundToInt()
fun Int.spToPx(resources: Resources): Float = toPx(resources, TypedValue.COMPLEX_UNIT_SP)


inline fun TabLayout.addTabSelectedListener(crossinline tabSelector: (tab: TabLayout.Tab) -> Unit) {
    addOnTabSelectedListener(object : SimpleOnTabSelectedListener() {
        override fun onTabSelected(tab: TabLayout.Tab) {
            tabSelector(tab)
        }
    })
}

inline fun Toolbar.setAsActionBar(activity: Activity? = null, block: ActionBar.() -> Unit = {}) {
    (activity?.kast<AppCompatActivity>() ?: context.scanForActivity())?.let {
        it.setSupportActionBar(this)
        it.supportActionBar?.apply(block)
    } ?: throw Error("Toolbar activity was not found")
}

abstract class SimpleOnTabSelectedListener : TabLayout.OnTabSelectedListener {
    override fun onTabReselected(tab: TabLayout.Tab?) {}
    override fun onTabUnselected(tab: TabLayout.Tab?) {}
    override fun onTabSelected(tab: TabLayout.Tab) {}
}

/**
 * Return the dimension as they are in dimensions.xml. ignoring the screen density
 */
fun Resources.getDimensionRaw(@DimenRes id: Int): Int =
        (getDimension(id) / Resources.getSystem().displayMetrics.density).toInt()

/**
 * Return the dimension as they are in dimensions.xml. ignoring the screen density
 */
fun Resources.getDimensionRawPixelsSize(@DimenRes id: Int): Int =
        (getDimensionPixelSize(id) / Resources.getSystem().displayMetrics.density).toInt()
