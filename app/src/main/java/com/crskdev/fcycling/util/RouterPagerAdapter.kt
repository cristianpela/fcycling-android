@file:Suppress("unused")

package com.crskdev.fcycling.util

import android.os.Bundle
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Router
import com.crskdev.fcycling.presentation.screen.BaseParentController
import com.crskdev.fcycling.presentation.screen.TaggedRouter
import java.util.*

/**
 * Created by Cristian Pela on 17.05.2018.
 */
abstract class RouterPagerAdapter(private val containerId: Int, private val host: BaseParentController<*>) : PagerAdapter() {

    companion object {
        private const val KEY_SAVED_PAGES = "RouterPagerAdapter.savedStates"
        private const val KEY_MAX_PAGES_TO_STATE_SAVE = "RouterPagerAdapter.maxPagesToStateSave"
        private const val KEY_SAVE_PAGE_HISTORY = "RouterPagerAdapter.savedPageHistory"
    }


    private var maxPagesToStateSave = Integer.MAX_VALUE
    private var savedPages: SparseArray<Bundle>? = SparseArray()
    private val visibleRouters = SparseArray<TaggedRouter>()
    private var savedPageHistory: ArrayList<Int>? = ArrayList()


    /**
     * Sets the maximum number of pages that will have their states saved. When this number is exceeded,
     * the page that was state saved least recently will have its state removed from the save data.
     */
    fun setMaxPagesToStateSave(maxPagesToStateSave: Int) {
        if (maxPagesToStateSave < 0) {
            throw IllegalArgumentException("Only positive integers may be passed for maxPagesToStateSave.")
        }

        this.maxPagesToStateSave = maxPagesToStateSave

        ensurePagesSaved()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val tagRouter = makeRouterName(position)

        val router = obtainChildRouter(container, position, tagRouter)
        if (!router.hasRootController()) {
            val routerSavedState = savedPages!!.get(position)

            if (routerSavedState != null) {
                router.restoreInstanceState(routerSavedState)
                savedPages!!.remove(position)
            }
        }

        router.rebindIfNeeded()
        return router
    }


    private fun obtainChildRouter(container: ViewGroup, position: Int, tagRouter: String): Router =
            host.routers[containerId]
                    ?.find { it.first == tagRouter }
                    ?.apply { visibleRouters.put(position, this) }
                    ?.second
                    ?: host.getChildRouter(container, tagRouter)
                            .apply {
                                val taggedRouter = TaggedRouter(tagRouter, this)
                                visibleRouters.put(position, taggedRouter)
                                val hostList = host.routers[containerId]
                                        ?: mutableListOf<TaggedRouter>().apply {
                                            host.routers.put(containerId, this)
                                        }
                                hostList.add(taggedRouter)
                            }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val router = `object` as Router

        val savedState = Bundle()
        router.saveInstanceState(savedState)
        savedPages!!.put(position, savedState)

        savedPageHistory!!.remove(position)
        savedPageHistory!!.add(position)

        ensurePagesSaved()

        host.removeChildRouter(router)

        visibleRouters[position]?.let { tr ->
            visibleRouters.removeAt(position)
            host.routers[containerId]?.first { it.first == tr.first }?.let {
                host.routers[containerId].remove(it)
            }
        }

    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        val router = `object` as Router
        val backstack = router.backstack
        for (transaction in backstack) {
            if (transaction.controller().view === view) {
                return true
            }
        }
        return false
    }

    override fun saveState(): Parcelable? {
        val bundle = Bundle()
        bundle.putSparseParcelableArray(KEY_SAVED_PAGES, savedPages)
        bundle.putInt(KEY_MAX_PAGES_TO_STATE_SAVE, maxPagesToStateSave)
        bundle.putIntegerArrayList(KEY_SAVE_PAGE_HISTORY, savedPageHistory)
        return bundle
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {
        val bundle = state as Bundle?
        if (state != null) {
            savedPages = bundle!!.getSparseParcelableArray(KEY_SAVED_PAGES)
            maxPagesToStateSave = bundle.getInt(KEY_MAX_PAGES_TO_STATE_SAVE)
            savedPageHistory = bundle.getIntegerArrayList(KEY_SAVE_PAGE_HISTORY)
        }
    }

    /**
     * Returns the already instantiated Router in the specified position or `null` if there
     * is no router associated with this position.
     */
    fun getRouter(position: Int): Router? {
        return visibleRouters[position].second
    }

    fun getItemId(position: Int): Long {
        return position.toLong()
    }

    internal fun getSavedPages(): SparseArray<Bundle>? {
        return savedPages
    }

    private fun ensurePagesSaved() {
        while (savedPages!!.size() > maxPagesToStateSave) {
            val positionToRemove = savedPageHistory!!.removeAt(0)
            savedPages!!.remove(positionToRemove)
        }
    }

    abstract fun makeRouterName(position: Int): String

}