package com.crskdev.fcycling.util


import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.RestoreViewOnCreateController
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.SimpleSwapChangeHandler

/**
 * [Source from forked lib](https://github.com/dmdevgo/Conductor/blob/develop/conductor/src/main/java/com/bluelinelabs/conductor/DialogController.java)<br></br>
 * * A controller that displays a dialog window, floating on top of its activity's window.
 * This is a wrapper over [Dialog] object like [android.app.DialogFragment].
 *
 *
 *
 * Implementations should override this class and implement [.onCreateDialog] to create a custom dialog, such as an [android.app.AlertDialog]
 * <br></br>
 * Created by Cristian Pela on 31.03.2018.
 */
abstract class DialogController(args: Bundle = Bundle()) : RestoreViewOnCreateController(args) {

    private lateinit var dialog: Dialog
    internal lateinit var controllerTag: String

    private var showing: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedViewState: Bundle?): View {
        dialog = onCreateDialog(savedViewState)
                .apply {
                    ownerActivity = activity
                    setOnDismissListener { dismiss() }
                    savedViewState?.getBundle(SAVED_DIALOG_STATE_TAG)?.let { this.onRestoreInstanceState(it) }
                }
        return View(activity)//stub view
    }

    override fun onRestoreViewState(view: View, savedViewState: Bundle) {
        with(savedViewState) {
            showing = getBoolean(SHOWING_DIALOG_STATE_TAG, false)
            controllerTag = getString(CONTROLLER_TAG)
        }
    }

    override fun onSaveViewState(view: View, outState: Bundle) {
        super.onSaveViewState(view, outState)
        with(outState) {
            putBundle(SAVED_DIALOG_STATE_TAG, dialog.onSaveInstanceState())
            putBoolean(SHOWING_DIALOG_STATE_TAG, showing)
            putString(CONTROLLER_TAG, controllerTag)
        }

    }

    override fun onAttach(view: View) {
        if (showing) {
            dialog.show()
        }
    }

    override fun onDetach(view: View) {
        dialog.setOnDismissListener(null)
        dialog.dismiss()
    }

    /**
     * Display the dialog, create a transaction and pushing the controller.
     *
     * @param router The router on which the transaction will be applied
     */
    fun show(router: Router) {
        if (!showing) {
            router.pushController(RouterTransaction.with(this)
                    .pushChangeHandler(SimpleSwapChangeHandler(false))
                    .popChangeHandler(SimpleSwapChangeHandler(false))
                    .tag(controllerTag))
            dialog.show()
            showing = true
        }
    }

    /**
     * Dismiss the dialog and pop this controller
     */
    fun dismiss() {
        if (showing) {
            router.popController(this)
            dialog.dismiss()
            showing = false
        }
    }

    override fun handleBack(): Boolean {
        dismiss()
        return true
    }

    /**
     * Build your own custom Dialog container such as an [android.app.AlertDialog]
     *
     * @param savedViewState A bundle for the view's state, which would have been created in [.onSaveViewState] or `null` if no saved state exists.
     * @return Return a new Dialog instance to be displayed by the Controller
     */
    protected abstract fun onCreateDialog(savedViewState: Bundle?): Dialog

    companion object {

        private const val SAVED_DIALOG_STATE_TAG = "android:savedDialogState"

        private const val SHOWING_DIALOG_STATE_TAG = "android:showingDialogState"

        private const val CONTROLLER_TAG = "conductor:controllerDialogTag"
    }
}

@Suppress("UNCHECKED_CAST")
class DialogControllerManager<D : DialogController>(
        private val router: Router,
        private val tag: String,
        private val factory: (args: Bundle) -> D) : IDialogControllerManager<D> {

    override fun show() {
        router.getControllerWithTag(tag)
                ?.let { it as D }
                ?.dismiss()
                ?: factory(Bundle())
                        .apply { controllerTag = tag }
                        .show(router)

    }

    override fun dismiss() {
        router.getControllerWithTag(tag)
                ?.let { it as D }
                ?.dismiss()
    }

}

class ContentLoadingDialogControllerManager<D : DialogController>(private val base: DialogControllerManager<D>) :
        IDialogControllerManager<D> by base {

    companion object {
        private const val MIN_SHOW_TIME = 500 // ms
        private const val MIN_DELAY = 500 // ms
    }

    private var mStartTime: Long = -1

    private var mPostedHide = false

    private var mPostedShow = false

    private var mDismissed = false

    private val handler = Handler(Looper.getMainLooper())

    private val mDelayedHide = Runnable {
        mPostedHide = false
        mStartTime = -1
        base.dismiss()
    }

    private val mDelayedShow = Runnable {
        mPostedShow = false
        if (!mDismissed) {
            mStartTime = System.currentTimeMillis()
            base.show()
        }
    }

    //TODO make use of this method, possible leak?
    fun removeCallbacks() {
        handler.removeCallbacks(mDelayedHide)
        handler.removeCallbacks(mDelayedShow)
    }

    override fun dismiss() {
        mDismissed = true
        handler.removeCallbacks(mDelayedShow)
        mPostedShow = false
        val diff = System.currentTimeMillis() - mStartTime
        if (diff >= MIN_SHOW_TIME || mStartTime == -1L) {
            // The progress spinner has been shown long enough
            // OR was not shown yet. If it wasn't shown yet,
            // it will just never be shown.
            base.dismiss()
        } else {
            // The progress spinner is shown, but not long enough,
            // so put a delayed message in to hide it when its been
            // shown long enough.
            if (!mPostedHide) {
                handler.postDelayed(mDelayedHide, MIN_SHOW_TIME - diff)
                mPostedHide = true
            }
        }
    }

    override fun show() {
        mStartTime = -1
        mDismissed = false
        handler.removeCallbacks(mDelayedHide)
        mPostedHide = false
        if (!mPostedShow) {
            handler.postDelayed(mDelayedShow, MIN_DELAY.toLong())
            mPostedShow = true
        }
    }
}

interface IDialogControllerManager<D : DialogController> {

    fun show()

    fun dismiss()
}
