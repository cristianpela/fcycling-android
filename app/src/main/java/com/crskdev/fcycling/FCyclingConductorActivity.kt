@file:Suppress("DEPRECATION")

package com.crskdev.fcycling

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import androidx.core.widget.toast
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.crskdev.fcycling.presentation.screen.BaseController
import com.crskdev.fcycling.presentation.screen.BaseParentController
import com.crskdev.fcycling.presentation.screen.LinkParcelizer
import com.crskdev.fcycling.presentation.screen.ParcelableLink
import com.crskdev.fcycling.util.ContentLoadingDialogControllerManager
import com.crskdev.fcycling.util.DialogController
import com.crskdev.fcycling.util.DialogControllerManager
import com.crskdev.fcycling.util.IDialogControllerManager
import fcycling.crskdev.com.domain.screen.Container.*
import fcycling.crskdev.com.domain.screen.LOGIN_LINK_SCREEN_WITH_NO_REQUEST
import fcycling.crskdev.com.domain.screen.Link
import fcycling.crskdev.com.domain.screen.ScreenModel
import io.reactivex.disposables.Disposable


class FCyclingConductorActivity : AppCompatActivity() {

    private val containersToLayoutMapper = mapOf(
            ROOT_CONTAINER_ID to R.id.root_container,
            MAIN_SCREEN_NAV_DETAIL_CONTAINER_ID to R.id.main_screen_nav_detail_container,
            MENU_LOGGED_PROFILE_CONTAINER_ID to R.id.menu_logged_profile_container,
            PROFILE_INFO_STATS_CONTAINER_ID to R.id.view_pager_profile_info_stats_container_layout
    )
    private lateinit var router: Router

    internal val linkParcelizer = LinkParcelizer()

    private lateinit var linkDispatcherDisposable: Disposable

    private lateinit var waitDialogManager: IDialogControllerManager<WaitDialogController>

    private var isRecreated = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fcycling_conductor)
        val container = findViewById<ViewGroup>(containersToLayoutMapper[ROOT_CONTAINER_ID]!!)
        router = Conductor.attachRouter(this, container, savedInstanceState)
        waitDialogManager = ContentLoadingDialogControllerManager(DialogControllerManager(router, "wait-dialog-controller-tag") {
            WaitDialogController(it)
        })
        isRecreated = savedInstanceState != null

//        linkDispatcherDisposable = Disposables.empty()
//        if(!router.hasRootController())
//            router.setRoot(RouterTransaction.with(DragDropSpikeMainController(Bundle())))


    }

    override fun onStart() {
        super.onStart()
        val linkDispatcher = application.getLinkDispatcher()
        linkDispatcherDisposable = linkDispatcher.observeScreenModel().subscribe {
            when (it) {
                is ScreenModel.Init -> dispatchLink(LOGIN_LINK_SCREEN_WITH_NO_REQUEST)
                is ScreenModel.Wait -> showWaitDialog()
                is ScreenModel.DataModel -> route(it)
                is ScreenModel.CompositeDataModel -> routeComposite(it)
                is ScreenModel.ErrorModel -> showError(it)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        dismissWaitDialog()
        linkDispatcherDisposable.dispose()
    }

    private fun showWaitDialog() {
        waitDialogManager.show()
    }

    private fun dismissWaitDialog() {
        waitDialogManager.dismiss()
    }

    private fun showError(errorModel: ScreenModel.ErrorModel) {
        dismissWaitDialog()
        //TODO better ui for error + plus allow to "retry"
        toast(errorModel.error.javaClass.simpleName + ": " + errorModel.error.message.toString())
    }


    fun route(screenModel: ScreenModel.DataModel) {
        dismissWaitDialog()
        val controllerFactory = application.getScreenConfig().controllerFactory
        val isChild = screenModel.originalLink.routeBehavior.showScreenAsChild
        if (isChild) {
            val containerId = containersToLayoutMapper[screenModel.originalLink.routeBehavior.container]
                    ?: throw Error("Container id not found")
            val controller = findParentControllerByViewContainer(screenModel.originalLink.containerTag())
            controller?.addChild(screenModel, controllerFactory, containerId)
                    ?: throw Error("Parent controller not found")
        } else {
            val link = linkParcelizer.parcelize(screenModel.originalLink)
            val controller = findControllerByViewContainer(screenModel.originalLink.screenTag())
            val foundRouter = controller?.router ?: router
            BaseController.route(foundRouter, link, screenModel.model, controllerFactory)
        }
    }

    private fun routeComposite(screenModel: ScreenModel.CompositeDataModel) {
        dismissWaitDialog()
        screenModel.models.forEach {
            route(it)
        }
    }

    override fun onBackPressed() {
        if (!router.handleBack()) {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        if (!isChangingConfigurations) {
            application.getLinkDispatcher().reset()
        }
        super.onDestroy()
    }

    private fun findControllerByViewContainer(screenTag: String): BaseController<*>? {
        router.backstack.forEach {
            val controller = it.controller() as BaseController<*>
            if (controller.link().screenTag() == screenTag) {
                return controller
            }
        }
        return null
    }


    private fun findParentControllerByViewContainer(containerTag: String): BaseParentController<*>? {
        if (!router.hasRootController()) {
            return null
        }
//TODO make it recursive
        router.backstack.forEach {
            val parent = it.controller()
                    .takeIf { c ->
                        if (c is BaseParentController<*>) {
                            val parentScreenTagFromChild = containerTag
                                    .takeIf { containerTag.contains(":") }
                                    ?.let { containerTag.split(":")[0] }
                            val parentScreenTag = c.link().screenTag()
                            (parentScreenTagFromChild == null || parentScreenTag == parentScreenTagFromChild)
                        } else
                            false
                    }
            if (parent != null) {
                return parent as BaseParentController<*>
            } else {
                it.controller().childRouters.forEach {
                    it.backstack.forEach {
                        val parent2 = it.controller()
                                .takeIf { c->
                                    if (c is BaseParentController<*>) {
                                        val parentScreenTagFromChild = containerTag
                                                .takeIf { containerTag.contains(":") }
                                                ?.let { containerTag.split(":")[0] }
                                        val parentScreenTag = c.link().screenTag()
                                        (parentScreenTagFromChild == null || parentScreenTag == parentScreenTagFromChild)
                                    } else
                                        false
                                }
                        if (parent2 != null) {
                            return parent2 as BaseParentController<*>
                        }
                    }
                }
            }
        }
        return null
    }


}

fun Activity.dispatchLink(link: ParcelableLink) {
    (this as FCyclingConductorActivity).let {
        application.getLinkDispatcher().dispatchLink(linkParcelizer.deparcelize(link))
    }
}


fun Activity.dispatchLink(link: Link) {
    (this as FCyclingConductorActivity).let {
        application.getLinkDispatcher().dispatchLink(link)
    }
}

fun Activity.notifyModelProcessed(){
    (this as FCyclingConductorActivity).let {
        application.getLinkDispatcher().flush()
    }
}


private class WaitDialogController(args: Bundle = Bundle()) : DialogController(args) {
    override fun onCreateDialog(savedViewState: Bundle?): Dialog =
            ProgressDialog(activity).apply {
                setOnCancelListener {
                    activity?.application?.getLinkDispatcher()?.dispatchLink(Link.CANCEL)
                }
                setMessage("Please Wait!")
            }

}